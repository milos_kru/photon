#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform samplerCube skyboxSampler;

layout(location = 0) in vec3 fragPosition;

layout(location = 0) out vec4 outColor;

vec3 Uncharted2Tonemap(vec3 color) // TODO: Doent use this one.
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	float W = 11.2;
	return ((color*(A*color+C*B)+D*E)/(color*(A*color+B)+D*F))-E/F;
}

void main()
{
    vec3 color = texture(skyboxSampler, fragPosition).rgb;
	
	// Tone mapping
	color = Uncharted2Tonemap(color * 4.5);
	color = color * (1.0f / Uncharted2Tonemap(vec3(11.2f)));	
	// Gamma correction
	color = pow(color, vec3(1.0f / 2.2));
	
	outColor = vec4(color, 1.0);
}
