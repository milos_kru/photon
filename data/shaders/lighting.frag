#version 450

layout (location = 0) in vec2 inUV;

layout (binding = 0) uniform UBO { // TODO: Don't need all of these!!! Use only one constant buffer.
	mat4 projection;
	mat4 ProjectionViewInverse;
	mat4 model;
	mat4 view;
	mat4 depthMVP;
	vec3 camPos;
} ubo;

layout (binding = 1) uniform UBOParams {
	vec4 lights[4];
	float exposure;
	float gamma;
} uboParams;

layout (binding = 2) uniform samplerCube samplerIrradiance;
layout (binding = 3) uniform sampler2D samplerBRDFLUT;
layout (binding = 4) uniform samplerCube prefilteredMap;

layout (binding = 5) uniform sampler2D albedoMap;
layout (binding = 6) uniform sampler2D normalMap;
layout (binding = 7) uniform sampler2D armMap;
layout (binding = 8) uniform sampler2D depthMap;
layout (binding = 9) uniform sampler2D shadowMap;

layout (location = 0) out vec4 outColor;

#define PI 3.1415926535897932384626433832795

// From http://filmicgames.com/archives/75
vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

// Normal Distribution function --------------------------------------
float D_GGX(float dotNH, float roughness)
{
	float alpha = roughness * roughness;
	float alpha2 = alpha * alpha;
	float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
	return (alpha2)/(PI * denom*denom); 
}

// Geometric Shadowing function --------------------------------------
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;
	float GL = dotNL / (dotNL * (1.0 - k) + k);
	float GV = dotNV / (dotNV * (1.0 - k) + k);
	return GL * GV;
}

// Fresnel function ----------------------------------------------------
vec3 F_Schlick(float cosTheta, vec3 F0)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}
vec3 F_SchlickR(float cosTheta, vec3 F0, float roughness)
{
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 prefilteredReflection(vec3 R, float roughness)
{
	const float MAX_REFLECTION_LOD = 9.0; // todo: param/const
	float lod = roughness * MAX_REFLECTION_LOD;
	float lodf = floor(lod);
	float lodc = ceil(lod);
	vec3 a = textureLod(prefilteredMap, R, lodf).rgb;
	vec3 b = textureLod(prefilteredMap, R, lodc).rgb;
	return mix(a, b, lod - lodf);
}

vec3 specularContribution(vec3 L, vec3 V, vec3 N, vec3 F0, float metallic, float roughness)
{
	// Precalculate vectors and dot products	
	vec3 H = normalize (V + L);
	float dotNH = clamp(dot(N, H), 0.0, 1.0);
	float dotNV = clamp(dot(N, V), 0.0, 1.0);
	float dotNL = clamp(dot(N, L), 0.0, 1.0);

	// Light color fixed
	vec3 lightColor = vec3(1.0);

	vec3 color = vec3(0.0);

	if (dotNL > 0.0) {
		vec3 ALBEDO = texture(albedoMap, inUV).rgb; // TODO: Second albedo sample.
		// D = Normal distribution (Distribution of the microfacets)
		float D = D_GGX(dotNH, roughness); 
		// G = Geometric shadowing term (Microfacets shadowing)
		float G = G_SchlicksmithGGX(dotNL, dotNV, roughness);
		// F = Fresnel factor (Reflectance depending on angle of incidence)
		vec3 F = F_Schlick(dotNV, F0);		
		vec3 spec = D * F * G / (4.0 * dotNL * dotNV + 0.001);		
		vec3 kD = (vec3(1.0) - F) * (1.0 - metallic);			
		color += (kD * ALBEDO / PI + spec) * dotNL;
	}

	return color;
}

vec3 worldPosFromDepth(float depth)
{
	vec4 clipSpacePosition = vec4(inUV * 2 - 1, depth, 1.0);
    vec4 worldSpacePosition = ubo.ProjectionViewInverse * clipSpacePosition;
	worldSpacePosition /= worldSpacePosition.w;
    return worldSpacePosition.xyz;
}

float sampleShadow(vec4 shadowCoord)
{
	float shadow = 1.0;
	if (shadowCoord.z > -1.0 && shadowCoord.z < 1.0) 
	{
		float dist = texture(shadowMap, shadowCoord.xy).r;
		if (shadowCoord.w > 0.0 && dist < shadowCoord.z) 
		{
			shadow = 0.1;
		}
	}

	return shadow;
}

float textureProj(vec4 shadowCoord, vec2 off)
{
	float shadow = 1.0;
	if ( shadowCoord.z > -1.0 && shadowCoord.z < 1.0 ) 
	{
		float dist = texture( shadowMap, shadowCoord.st + off ).r;
		if ( shadowCoord.w > 0.0 && dist < shadowCoord.z ) 
		{
			shadow = 0.1;
		}
	}
	return shadow;
}

float filterPCF(vec4 sc)
{
	ivec2 texDim = textureSize(shadowMap, 0);
	float scale = 1.5;
	float dx = scale * 1.0 / float(texDim.x);
	float dy = scale * 1.0 / float(texDim.y);

	float shadowFactor = 0.0;
	int count = 0;
	int range = 1;
	
	for (int x = -range; x <= range; x++)
	{
		for (int y = -range; y <= range; y++)
		{
			shadowFactor += textureProj(sc, vec2(dx*x, dy*y));
			count++;
		}
	
	}
	return shadowFactor / count;
}

const mat4 biasMat = mat4( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 );

const mat4 depthMVP = mat4( 
	-1.70711, -0.9856, 0.58343, 0.57735,
	0.00, 1.9712, 0.58343, 0.57735,
	1.70711, -0.9856, 0.58343, 0.57735,
	0.00, 0.00, 4.24032, 5.19615);

void main()
{
	float DEPTH = texture(depthMap, inUV).r;
	vec3 W = worldPosFromDepth(DEPTH);
	
	vec4 shadowCoord = (biasMat * depthMVP) * vec4(W, 1.0);
	float shadowMask = filterPCF(shadowCoord / shadowCoord.w);
	
	vec3 ARM = texture(armMap, inUV).rgb;
	vec3 N = texture(normalMap, inUV).rgb;

	vec3 V = normalize(ubo.camPos - W);
	vec3 R = reflect(-V, N); 

	float metallic = ARM.b;
	float roughness = ARM.g;
	
	vec3 ALBEDO = texture(albedoMap, inUV).rgb;

	vec3 F0 = vec3(0.04); 
	F0 = mix(F0, ALBEDO, metallic);
	
	vec3 Lo = vec3(0.0);
	vec3 L = normalize(vec3(-3.0, -3.0, -3.0)); // TODO: Pass light vector by cbuffer.
	Lo += specularContribution(L, V, N, F0, metallic, roughness);
	
	vec2 brdf = texture(samplerBRDFLUT, vec2(max(dot(N, V), 0.0), roughness)).rg;
	vec3 reflection = prefilteredReflection(R, roughness).rgb;	
	vec3 irradiance = texture(samplerIrradiance, N).rgb;

	// Diffuse based on irradiance
	vec3 diffuse = irradiance * ALBEDO;
	
	vec3 F = F_SchlickR(max(dot(N, V), 0.0), F0, roughness);

	// Specular reflectance
	vec3 specular = reflection * (F * brdf.x + brdf.y);
	
	// Ambient part
	vec3 kD = 1.0 - F;
	kD *= 1.0 - metallic;

	float shadow = ARM.r * shadowMask;

	vec3 ambient = (kD * diffuse + specular);
	
	vec3 color = ambient + Lo;
	
	color *= shadow;

	// Tone mapping
	color = Uncharted2Tonemap(color * uboParams.exposure);
	color = color * (1.0f / Uncharted2Tonemap(vec3(11.2f)));

	// Gamma correction
	color = pow(color, vec3(1.0f / uboParams.gamma));
	
	outColor = vec4(color, 1.0);
}
