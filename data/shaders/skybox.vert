#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject
{
    mat4 proj;
    mat4 model;
} ubo;

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTexCoord;
layout (location = 3) in vec4 inTangent;

layout(location = 0) out vec3 fragPosition;

void main()
{
    fragPosition = inPosition;
    gl_Position = ubo.proj * ubo.model * vec4(inPosition * 10.0, 1.0); // TODO: * 10.0 Handle this somehow better.
}
