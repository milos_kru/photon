#include "include/cameracontroller.h"
#include "include/camera.h"
#include "include/inputmanager.h"

PHOTON_BEGIN_NAMESPACE

#define ROTATION_SPEED_MUL 100.0f
#define MOVEMENT_SPEED_MUL 2.0f
#define FAST_MOVEMENT_SPEED_MUL 5.0f

void CameraController::Initialize(Camera& camera)
{
	m_Camera = &camera;
}

void CameraController::Update(float deltaTime)
{
	UpdateRotation(deltaTime);
	UpdatePosition(deltaTime);
}

void CameraController::UpdatePosition(float deltaTime)
{
	glm::vec3 cameraRotation = m_Camera->GetRotation();

	glm::vec3 cameraFront;
	cameraFront.x = -cos(glm::radians(cameraRotation.x)) * sin(glm::radians(cameraRotation.y));
	cameraFront.y = sin(glm::radians(cameraRotation.x));
	cameraFront.z = cos(glm::radians(cameraRotation.x)) * cos(glm::radians(cameraRotation.y));
	cameraFront = glm::normalize(cameraFront);

	float movementSpeed;
	if (InputManager::Get().IsKeyPressed(KeyCode::SHIFT))
	{
		movementSpeed = deltaTime * FAST_MOVEMENT_SPEED_MUL;
	}
	else
	{
		movementSpeed = deltaTime * MOVEMENT_SPEED_MUL;
	}

	glm::vec3 cameraPosition = m_Camera->GetPosition();

	if (InputManager::Get().IsKeyPressed(KeyCode::W))
	{
		cameraPosition += cameraFront * movementSpeed;
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::S))
	{
		cameraPosition -= cameraFront * movementSpeed;
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::A))
	{
		cameraPosition -= glm::normalize(glm::cross(cameraFront, glm::vec3(0.0f, 1.0f, 0.0f))) * movementSpeed;
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::D))
	{
		cameraPosition += glm::normalize(glm::cross(cameraFront, glm::vec3(0.0f, 1.0f, 0.0f))) * movementSpeed;
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::E))
	{
		cameraPosition += glm::vec3(0.0f, 1.0f, 0.0f) * movementSpeed;
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::Q))
	{
		cameraPosition -= glm::vec3(0.0f, 1.0f, 0.0f) * movementSpeed;
	}

	m_Camera->SetPosition(cameraPosition);
}

void CameraController::UpdateRotation(float deltaTime)
{
	glm::vec3 cameraRotation = m_Camera->GetRotation();

	float rotationSpeed = deltaTime * ROTATION_SPEED_MUL;
	if (InputManager::Get().IsKeyPressed(KeyCode::UP_ARROW))
	{
		cameraRotation += glm::vec3(rotationSpeed, 0.0f, 0.0f);
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::DOWN_ARROW))
	{
		cameraRotation += glm::vec3(-rotationSpeed, 0.0f, 0.0f);
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::LEFT_ARROW))
	{
		cameraRotation += glm::vec3(0.0f, -rotationSpeed, 0.0f);
	}

	if (InputManager::Get().IsKeyPressed(KeyCode::RIGHT_ARROW))
	{
		cameraRotation += glm::vec3(0.0f, rotationSpeed, 0.0f);
	}

	m_Camera->SetRotation(cameraRotation);
}

PHOTON_END_NAMESPACE
