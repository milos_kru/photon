#include "include\camera.h"

PHOTON_BEGIN_NAMESPACE

void Camera::SetPosition(const glm::vec3 position)
{
	m_Position = position;
	UpdateViewMatrix();
}

void Camera::SetRotation(const glm::vec3 rotation)
{
	m_Rotation = rotation;
	UpdateViewMatrix();
}

void Camera::SetPerspective(float fov, float aspectRatio, float nearPlane, float farPlane)
{
	m_PerspectiveData.Fov = fov;
	m_PerspectiveData.AspectRatio = aspectRatio;
	m_PerspectiveData.NearPlane = nearPlane;
	m_PerspectiveData.FarPlane = farPlane;

	m_Matrices.Perspective = glm::perspective(glm::radians(fov), aspectRatio, nearPlane, farPlane);
}

void Camera::SetOrthographic(int left, int right, int top, int bottom, int nearPlane, int farPlane)
{
	m_OrthographicData.Left = left;
	m_OrthographicData.Right = right;
	m_OrthographicData.Top = top;
	m_OrthographicData.Bottom = bottom;
	m_OrthographicData.NearPlane = nearPlane;
	m_OrthographicData.FarPlane = farPlane;

	m_Matrices.Orthographic = glm::ortho(left, right, top, bottom, nearPlane, farPlane);
}

void Camera::UpdateViewMatrix()
{
	glm::mat4 rotationMatrix = glm::mat4(1.0f);
	glm::mat4 translationMatrix;

	rotationMatrix = glm::rotate(rotationMatrix, glm::radians(m_Rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
	rotationMatrix = glm::rotate(rotationMatrix, glm::radians(m_Rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
	rotationMatrix = glm::rotate(rotationMatrix, glm::radians(m_Rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));

	glm::vec3 translation = m_Position;
	translationMatrix = glm::translate(glm::mat4(1.0f), translation);

	m_Matrices.View = rotationMatrix * translationMatrix;
	m_ViewPosition = glm::vec4(m_Position, 0.0f) * glm::vec4(-1.0f, 1.0f, -1.0f, 1.0f);
}

PHOTON_END_NAMESPACE
