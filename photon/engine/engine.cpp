#include "include/engine.h"
#include "include/window.h"
#include "include/camera.h"

#include "visual/include/renderer.h"

PHOTON_BEGIN_NAMESPACE

Engine::~Engine()
{
	delete(&Renderer::Get());
}

void Engine::Initialize()
{
	InitializeCamera();
	InitializeCameraController();
}

void Engine::Update()
{
	if (IsIconic(Window::Get().GetHandle()))
	{
		return;
	}

	UpdateDeltaTime();

	Renderer::Get().DrawFrame();
	m_CameraController.Update(m_DeltaTime);
}

void Engine::InitializeCamera()
{
	m_Camera.SetPerspective(60.0f, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.05f, 256.0f);
	m_Camera.SetPosition({ 0.7f, -0.1f, 0.7f });
	m_Camera.SetRotation({ 0.0f, 135.0f, 0.0f });
}

void Engine::InitializeCameraController()
{
	m_CameraController.Initialize(m_Camera);
}

void Engine::UpdateDeltaTime()
{
	auto currentTime = std::chrono::high_resolution_clock::now();
	m_DeltaTime = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - m_PreviousTime).count();
	m_PreviousTime = currentTime;
}

PHOTON_END_NAMESPACE
