#include "include/inputmanager.h"

PHOTON_BEGIN_NAMESPACE

#define TO_KEY_CODE(wParam) static_cast<KeyCode>(wParam)

void InputManager::HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CLOSE:
		DestroyWindow(hWnd);
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		SetKeyDown(TO_KEY_CODE(wParam));
		break;

	case WM_KEYUP:
		SetKeyUp(TO_KEY_CODE(wParam));
		break;
	}
}

void InputManager::SetKeyDown(KeyCode keyCode)
{
	m_KeyValues.insert(keyCode);
}

void InputManager::SetKeyUp(KeyCode keyCode)
{
	m_KeyValues.erase(keyCode);
}

PHOTON_END_NAMESPACE
