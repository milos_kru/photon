#pragma once

#include "photon.h"
#include "singleton.h"

PHOTON_BEGIN_NAMESPACE

class Window : public Singleton<Window>
{
public:
	Window();

private:
	HWND InitializeWindow(HINSTANCE hinstance, WNDPROC wndproc, bool fullscreen = false);

public:
	HWND GetHandle() const { return m_Handle; }
	HINSTANCE GetInstance() const { return m_Instance; }

private:
	HWND m_Handle;
	HINSTANCE m_Instance;
};

PHOTON_END_NAMESPACE
