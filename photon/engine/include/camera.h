#pragma once

#include "photon.h"

PHOTON_BEGIN_NAMESPACE

class Camera
{
public:
	void SetPosition(glm::vec3 position);
	void SetRotation(glm::vec3 rotation);
	void SetPerspective(float fov, float aspectRatio, float nearPlane, float farPlane);
	void SetOrthographic(int left, int right, int top, int bottom, int nearPlane, int farPlane);

public:
	inline glm::mat4 GetPerspectiveMatrix() const { return m_Matrices.Perspective; }
	inline glm::mat4 GetOrthographicMatrix() const { return m_Matrices.Orthographic; }
	inline glm::mat4 GetViewMatrix() const { return m_Matrices.View; }
	inline glm::vec3 GetPosition() const { return m_Position; }
	inline glm::vec3 GetRotation() const { return m_Rotation; }

private:
	void UpdateViewMatrix();

private:
	struct
	{
		glm::mat4 Perspective;
		glm::mat4 Orthographic;
		glm::mat4 View;
	} m_Matrices;

	struct
	{
		float Fov;
		float AspectRatio;
		float NearPlane;
		float FarPlane;
	} m_PerspectiveData;

	struct
	{
		int Left;
		int Right;
		int Top;
		int Bottom;
		int NearPlane;
		int FarPlane;
	} m_OrthographicData;

	glm::vec3 m_Rotation = glm::vec3();
	glm::vec3 m_Position = glm::vec3();
	glm::vec4 m_ViewPosition = glm::vec4();
};

PHOTON_END_NAMESPACE
