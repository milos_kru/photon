#pragma once

#include "photon.h"
#include "singleton.h"
#include "camera.h"
#include "cameracontroller.h"

#include <chrono>

PHOTON_BEGIN_NAMESPACE

class Engine : public Singleton<Engine>
{
public:
	Engine() = default;
	~Engine();

public:
	void Initialize();
	void Update();

private:
	void InitializeCamera();
	void InitializeCameraController();
	void UpdateDeltaTime();

public:
	inline const Camera& GetCamera() const { return m_Camera; }

private:
	Camera m_Camera;
	CameraController m_CameraController;

	std::chrono::steady_clock::time_point m_PreviousTime = std::chrono::high_resolution_clock::now();
	float m_DeltaTime;
};

PHOTON_END_NAMESPACE
