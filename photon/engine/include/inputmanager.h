#pragma once

#include "photon.h"
#include "singleton.h"

#include <unordered_set>
#include <optional>

PHOTON_BEGIN_NAMESPACE

enum class KeyCode : uint8_t
{
	W = 0x57,
	S = 0x53,
	A = 0x41,
	D = 0x44,
	Q = 0x51,
	E = 0x45,
	SHIFT = 0x10,
	UP_ARROW = 0x26,
	DOWN_ARROW = 0x28,
	LEFT_ARROW = 0x25,
	RIGHT_ARROW = 0x27
};

class InputManager : public Singleton<InputManager>
{
public:
	InputManager() = default;

public:
	void HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
	void SetKeyDown(KeyCode keyCode);
	void SetKeyUp(KeyCode keyCode);
	
private:
public:
	inline bool IsKeyPressed(KeyCode keyCode) const
	{
		return m_KeyValues.find(keyCode) != m_KeyValues.end();
	}

private:
	std::unordered_set<KeyCode> m_KeyValues;
};

PHOTON_END_NAMESPACE
