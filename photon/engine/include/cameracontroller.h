#pragma once

#include "photon.h"

PHOTON_BEGIN_NAMESPACE

class Camera;

class CameraController
{
public:
	void Initialize(Camera& camera);
	void Update(float deltaTime);

private:
	void UpdatePosition(float deltaTime);
	void UpdateRotation(float deltaTime);

private:
	Camera* m_Camera;
};

PHOTON_END_NAMESPACE
