#pragma once

PHOTON_BEGIN_NAMESPACE

template <typename T>
class Singleton
{
public:
	static T& Get()
	{
		static T* instance = new T();
		return *instance;
	}

public:
	Singleton() = default;

public:
	Singleton(Singleton const&) = delete;
	Singleton& operator=(Singleton const&) = delete;
};

PHOTON_END_NAMESPACE
