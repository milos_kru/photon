#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/hash.hpp>

#ifndef NDEBUG
	#define PHOTON_DEBUG
#endif // !NDEBUG

#ifdef PHOTON_DEBUG
	#define PHOTON_ASSERT(condition) if(!(condition)) __debugbreak()
	
	#define PHOTON_ASSERT_WITH_MESSAGE(condition, message) \
			if(!(condition)) \
			{ \
				std::cerr << message << std::endl; \
				__debugbreak(); \
			}
	
	#define PHOTON_ASSERT_ALWAYS(message) PHOTON_ASSERT_WITH_MESSAGE(0, message)
#else
	#define PHOTON_ASSERT(condition)
	#define PHOTON_ASSERT_WITH_MESSAGE(condition, message)
	#define PHOTON_ASSERT_ALWAYS(message)
#endif // GFX_DEBUG

#define PHOTON_FLAG_TYPE_SETUP(flagType) \
    inline flagType operator&(flagType a, flagType b) \
    { \
        return static_cast<flagType>(static_cast<std::underlying_type<flagType>::type>(a) & \
			static_cast<std::underlying_type<flagType>::type>(b) ); \
    } \
	\
    inline flagType operator|(flagType a, flagType b) \
    { \
        return static_cast<flagType>( static_cast<std::underlying_type<flagType>::type>(a) | \
			static_cast<std::underlying_type<flagType>::type>(b)); \
    } \
	\
    inline bool IsFlagSet(flagType x) \
    { \
        return (static_cast<uint32_t>(x) != 0); \
    }

#define PHOTON_BEGIN_NAMESPACE namespace Photon \
	{

#define PHOTON_END_NAMESPACE }

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

// TODO: Move this to project settings and add it automatically with CMake.
#define DATA_PATH std::string("D:/Dev/Vulkan/Photon Engine/photon/data/")