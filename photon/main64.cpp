#include <SDL.h>
#include <SDL_syswm.h>

#include "engine/include/engine.h"

int main(int argc, char* args[])
{
	Photon::Engine::Get().Initialize();

	MSG msg;
	bool quitMessageReceived = false;
	while (!quitMessageReceived)
	{
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				quitMessageReceived = true;
				break;
			}
		}

		Photon::Engine::Get().Update();
	}

	delete(&Photon::Engine::Get());

	return EXIT_SUCCESS;
}
