#include "include/uniformbuffer.h"
#include "include/device.h"
#include "include/devicestate.h"
#include "include/swapchain.h"
#include "include/buffer.h"

PHOTON_BEGIN_NAMESPACE

UniformBuffer::UniformBuffer(Device* device, const uint32_t size, const std::string name)
	: m_Device(device)
{
	m_Buffers.resize(2);
	for (uint32_t i = 0; i < m_Buffers.size(); ++i)
	{
		m_Buffers[i] = new Buffer(m_Device, Fluent::BufferUsageFlags::UniformBuffer,
			Fluent::MemoryAccess::Host, size, name);
	}
}

UniformBuffer::~UniformBuffer()
{
	for (uint32_t i = 0; i < m_Buffers.size(); ++i)
	{
		delete(m_Buffers[i]);
	}
	m_Buffers.clear();
}

void* UniformBuffer::Map() const
{
	auto currentFrameInFlightIndex = Swapchain::Get().GetCurrentFrameInFlightIndex();
	return m_Buffers[currentFrameInFlightIndex]->Map();
}

void UniformBuffer::Unmap() const
{
	auto currentFrameInFlightIndex = Swapchain::Get().GetCurrentFrameInFlightIndex();
	m_Buffers[currentFrameInFlightIndex]->Unmap();
}

Buffer* UniformBuffer::GetBuffer() const
{
	auto currentFrameInFlightIndex = Swapchain::Get().GetCurrentFrameInFlightIndex();
	return m_Buffers[currentFrameInFlightIndex];
}

PHOTON_END_NAMESPACE
