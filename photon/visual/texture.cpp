#include "include/texture.h"
#include "include/buffer.h"
#include "include/device.h"

#include <ktx.h>

PHOTON_BEGIN_NAMESPACE

void Texture::Initialize()
{
	PHOTON_ASSERT(m_Device);

	if (IsFlagSet(m_Flags & TextureFlags::Swapchain))
	{
		PHOTON_ASSERT(m_Resource);
		Fluent::Extent3D resourceExtent = m_Resource->GetInfo().Extent;
		m_Width = resourceExtent.Width;
		m_Height = resourceExtent.Height;
	}
	else
	{
		InitializeResource();
	}

	InitializeView();
	InitializeSampler();
}

TextureBuilder Texture::Create()
{
	return TextureBuilder();
}

Texture::~Texture()
{
	delete(m_Sampler);
	delete(m_View);

	if (!IsFlagSet(m_Flags & TextureFlags::Swapchain))
	{
		delete(m_Resource);
	}
}

void Texture::InitializeResource()
{
	ktxTexture* internalTexture = nullptr;
	Buffer* stagingBuffer = nullptr;
	if (m_FilePath.has_value())
	{
		PHOTON_ASSERT(ktxTexture_CreateFromNamedFile(m_FilePath.value().c_str(),
			KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &internalTexture) == KTX_SUCCESS);

		m_Width = internalTexture->baseWidth;
		m_Height = internalTexture->baseHeight;
		m_MipCount = internalTexture->numLevels;

		ktx_uint8_t* ktxTextureData = ktxTexture_GetData(internalTexture);
		ktx_size_t ktxTextureSize = ktxTexture_GetSize(internalTexture);

		stagingBuffer = new Buffer(m_Device, Fluent::BufferUsageFlags::TransferSrc,
			Fluent::MemoryAccess::Host, static_cast<uint32_t>(ktxTextureSize), "Staging Buffer");

		void* data = stagingBuffer->Map();
		memcpy(data, ktxTextureData, ktxTextureSize);
		stagingBuffer->Unmap();
	}
	else if (m_Data.has_value())
	{
		// TODO: Slightly repeated code.
		TextureData textureData = m_Data.value();

		stagingBuffer = new Buffer(m_Device, Fluent::BufferUsageFlags::TransferSrc,
			Fluent::MemoryAccess::Host, static_cast<uint32_t>(textureData.Size), "Staging Buffer");

		void* data = stagingBuffer->Map();
		memcpy(data, textureData.Data, textureData.Size);
		stagingBuffer->Unmap();
	}
	else
	{
		PHOTON_ASSERT(m_Width && m_Height);
	}

	bool hasInitialData = stagingBuffer != nullptr;
	bool isCubeMap = IsFlagSet(m_Flags & TextureFlags::CubeMap);
	bool isDepthStencil = IsFlagSet(m_Flags & TextureFlags::DepthStencil);
	bool isRenderTarget = IsFlagSet(m_Flags & TextureFlags::RenderTarget);
	bool isResource = IsFlagSet(m_Flags & TextureFlags::Read);

	Fluent::ImageResourceInfo imageResourceInfo;
	imageResourceInfo.Flags = isCubeMap ? Fluent::ImageFlags::CubeMap : Fluent::ImageFlags::None;
	imageResourceInfo.UsageFlags =
		(hasInitialData ? Fluent::ImageUsageFlags::TransferDst : Fluent::ImageUsageFlags::None) |
		(isResource ? Fluent::ImageUsageFlags::Resource : Fluent::ImageUsageFlags::None) |
		(isDepthStencil ? Fluent::ImageUsageFlags::DepthStencil :
			(isRenderTarget ? Fluent::ImageUsageFlags::RenderTarget : Fluent::ImageUsageFlags::None));
	imageResourceInfo.MemoryAccess = Fluent::MemoryAccess::Device;
	imageResourceInfo.Format = m_Format;
	imageResourceInfo.Extent.Width = m_Width;
	imageResourceInfo.Extent.Height = m_Height;
	imageResourceInfo.Extent.Depth = 1;
	imageResourceInfo.ArraySize = 1;
	imageResourceInfo.MipCount = m_MipCount;
	imageResourceInfo.MultisampleType = Fluent::MultisampleType::Sample_1;

	m_Resource = new Fluent::ImageResource(m_Device->GetDevice(), imageResourceInfo, m_Name.c_str());

	Fluent::ResourceState stateAfter = Fluent::ResourceState::None;
	if (isDepthStencil && isRenderTarget)
	{
		stateAfter = Fluent::ResourceState::DepthStencilWrite;
	}
	else if(isDepthStencil && isResource)
	{
		stateAfter = Fluent::ResourceState::DepthStencilRead;
	}
	else if (isRenderTarget)
	{
		stateAfter = Fluent::ResourceState::RenderTarget;
	}
	else if (isResource)
	{
		stateAfter = Fluent::ResourceState::GenericRead;
	}

	m_Device->BeginTransferCommands();

	if (hasInitialData)
	{
		m_Device->TransitionLayout(this, Fluent::ResourceState::None, Fluent::ResourceState::TransferDst);

		for (uint32_t i = 0; i < m_MipCount; ++i)
		{
			ktx_size_t offset = 0;

			if (!m_Data.has_value())
				PHOTON_ASSERT(ktxTexture_GetImageOffset(internalTexture, i, 0, 0, &offset) == KTX_SUCCESS);

			Fluent::BufferRegion bufferRegion{};
			bufferRegion.Offset = static_cast<uint32_t>(offset);
			bufferRegion.Size = static_cast<uint32_t>((m_Width >> i) * (m_Height >> i) * 6);

			Fluent::ImageRegion imageRegion{};
			imageRegion.Extent.Width = m_Width >> i;
			imageRegion.Extent.Height = m_Height >> i;
			imageRegion.Extent.Depth = 1;
			imageRegion.Offset.X = 0;
			imageRegion.Offset.Y = 0;
			imageRegion.Offset.Z = 0;
			imageRegion.Subresource.MipSlice = i;
			imageRegion.Subresource.MipSize = 1;
			imageRegion.Subresource.ArraySlice = 0;
			imageRegion.Subresource.ArraySize = isCubeMap ? 6 : 1;

			m_Device->CopyBufferToTexture(stagingBuffer, bufferRegion, this, imageRegion);
		}

		m_Device->TransitionLayout(this, Fluent::ResourceState::TransferDst, stateAfter);
	}
	else
	{
		m_Device->TransitionLayout(this, Fluent::ResourceState::None, stateAfter);
	}

	m_Device->EndTransferCommands();

	if (internalTexture)
	{
		ktxTexture_Destroy(internalTexture);
	}

	if (stagingBuffer)
	{
		delete(stagingBuffer);
	}
}

void Texture::InitializeView()
{
	Fluent::ClearValue clearValue;
	if (IsFlagSet(m_Flags & TextureFlags::DepthStencil))
	{
		clearValue.DepthStencil.Depth = 1.0f;
		clearValue.DepthStencil.Stencil = 0;
	}
	else
	{
		clearValue.Color.value[0] = 0.09804f;
		clearValue.Color.value[1] = 0.09804f;
		clearValue.Color.value[2] = 0.09804f;
		clearValue.Color.value[3] = 1.0f;
	}

	Fluent::ImageViewInfo imageViewInfo;
	imageViewInfo.Resource = m_Resource;
	imageViewInfo.Format = Fluent::Format::UNDEFINED;
	imageViewInfo.Flags = Fluent::ImageViewFlags::Color;
	imageViewInfo.Subresource.MipSlice = 0;
	imageViewInfo.Subresource.MipSize = m_MipCount;
	imageViewInfo.Subresource.ArraySlice = 0;
	imageViewInfo.Subresource.ArraySize = 1;
	imageViewInfo.ClearValue = clearValue;

	m_View = new Fluent::ImageView(m_Device->GetDevice(), imageViewInfo, m_Name.c_str());
}

void Texture::InitializeSampler() // TODO: Generate samplers automatically.
{
	Fluent::ImageSamplerInfo imageSamplerInfo;
	imageSamplerInfo.AddressModeU = m_AddressMode;
	imageSamplerInfo.AddressModeV = m_AddressMode;
	imageSamplerInfo.AddressModeW = m_AddressMode;
	imageSamplerInfo.MagFilterType = m_FilterType;
	imageSamplerInfo.MinFilterType = m_FilterType;
	imageSamplerInfo.MipFilterType = m_FilterType;
	imageSamplerInfo.CompareOperation = Fluent::CompareOperation::Never;
	imageSamplerInfo.MaxLevelOfAnisotropy = 16;
	imageSamplerInfo.BorderColor.value[0] = 1.0f;
	imageSamplerInfo.BorderColor.value[1] = 1.0f;
	imageSamplerInfo.BorderColor.value[2] = 1.0f;
	imageSamplerInfo.BorderColor.value[3] = 1.0f;
	imageSamplerInfo.MipLODBias = 0.0f;
	imageSamplerInfo.MinLOD = 0.0f;
	imageSamplerInfo.MaxLOD = static_cast<float>(m_MipCount);

	m_Sampler = new Fluent::ImageSampler(m_Device->GetDevice(), imageSamplerInfo, m_Name.c_str());
}

TextureBuilder& TextureBuilder::SetDevice(Device* device)
{
	m_Texture->m_Device = device;
	return *this;
}

TextureBuilder& TextureBuilder::SetFilePath(std::string filePath)
{
	m_Texture->m_FilePath = std::make_optional(filePath);
	return *this;
}

TextureBuilder& TextureBuilder::SetData(TextureData data)
{
    m_Texture->m_Data = std::make_optional(data);
	return *this;
}

TextureBuilder& TextureBuilder::SetWidth(uint32_t width)
{
	m_Texture->m_Width = width;
	return *this;
}

TextureBuilder& TextureBuilder::SetHeight(uint32_t height)
{
	m_Texture->m_Height = height;
	return *this;
}

TextureBuilder& TextureBuilder::SetMipCount(uint32_t mipCount)
{
	m_Texture->m_MipCount = mipCount;
	return *this;
}

TextureBuilder& TextureBuilder::SetFormat(Fluent::Format format)
{
	m_Texture->m_Format = format;
	return *this;
}

TextureBuilder& TextureBuilder::SetFlags(TextureFlags flags)
{
	m_Texture->m_Flags = flags;
	return *this;
}

TextureBuilder& TextureBuilder::SetAddressMode(Fluent::AddressMode addressMode)
{
	m_Texture->m_AddressMode = addressMode;
	return *this;
}

TextureBuilder& TextureBuilder::SetFilterType(Fluent::FilterType filterType)
{
	m_Texture->m_FilterType = filterType;
	return *this;
}

TextureBuilder& TextureBuilder::SetResource(Fluent::ImageResource* resource)
{
	m_Texture->m_Resource = resource;
	return *this;
}

TextureBuilder& TextureBuilder::SetName(std::string name)
{
	m_Texture->m_Name = name;
	return *this;
}

TexturePtr TextureBuilder::Build()
{
	m_Texture->Initialize();
	return m_Texture;
}

PHOTON_END_NAMESPACE
