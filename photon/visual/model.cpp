#include "include\model.h"
#include "include\device.h"
#include "include\buffer.h"
#include "include\texture.h"

#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define TINYGLTF_NO_STB_IMAGE_WRITE
#include "tiny_gltf.h"

PHOTON_BEGIN_NAMESPACE

Model::Model(Device* device, const std::string filePath, const std::string name, bool loadImages)
	: m_Device(device)
	, m_FilePath(filePath)
	, m_Name(name)
{
	LoadFromFile(FileLoadingFlags::FlipY | FileLoadingFlags::PreTransformVertices | (loadImages ? FileLoadingFlags::None : FileLoadingFlags::DontLoadImages));
}

Model::~Model()
{
	m_Materials.clear();

	for (auto* node : m_LinearNodes)
	{
		delete(node);
	}
	m_LinearNodes.clear();
}

bool loadImageDataFunc(tinygltf::Image* image, const int imageIndex, std::string* error,
	std::string* warning, int req_width, int req_height, const unsigned char* bytes, int size, void* userData)
{
	if (image->uri.find_last_of(".") != std::string::npos) {
		if (image->uri.substr(image->uri.find_last_of(".") + 1) == "ktx") {
			return true;
		}
	}

	return tinygltf::LoadImageData(image, imageIndex, error, warning, req_width, req_height, bytes, size, userData);
}

bool loadImageDataFuncEmpty(tinygltf::Image* image, const int imageIndex, std::string* error,
	std::string* warning, int req_width, int req_height, const unsigned char* bytes, int size, void* userData)
{
	// This function will be used for samples that don't require images to be loaded
	return true;
}

void LoadNode(Device* m_Device, std::vector<Node*>& m_Nodes, std::vector<Node*>& m_LinearNodes,
	Node* parent, const tinygltf::Node& node, uint32_t nodeIndex, const tinygltf::Model& model, bool loadImages)
{
	Node* newNode = new Node{};
	newNode->index = nodeIndex;
	newNode->parent = parent;
	newNode->name = node.name;
	newNode->matrix = glm::mat4(1.0f);

	// Generate local node matrix
	glm::vec3 translation = glm::vec3(0.0f);
	if (node.translation.size() == 3) {
		translation = glm::make_vec3(node.translation.data());
		newNode->translation = translation;
	}
	glm::mat4 rotation = glm::mat4(1.0f);
	if (node.rotation.size() == 4) {
		glm::quat q = glm::make_quat(node.rotation.data());
		newNode->rotation = glm::mat4(q);
	}
	glm::vec3 scale = glm::vec3(1.0f);
	if (node.scale.size() == 3) {
		scale = glm::make_vec3(node.scale.data());
		newNode->scale = scale;
	}
	if (node.matrix.size() == 16) {
		newNode->matrix = glm::make_mat4x4(node.matrix.data());
	};

	// Node with children
	if (node.children.size() > 0) {
		for (auto i = 0; i < node.children.size(); i++) {
			LoadNode(m_Device, m_Nodes, m_LinearNodes, newNode, model.nodes[node.children[i]], node.children[i], model, loadImages);
		}
	}

	// Node contains mesh data
	if (node.mesh > -1) {
		const tinygltf::Mesh mesh = model.meshes[node.mesh];
		Mesh* newMesh = new Mesh();
		newMesh->name = mesh.name;
		for (size_t j = 0; j < mesh.primitives.size(); j++) {
			const tinygltf::Primitive& primitive = mesh.primitives[j];
			if (primitive.indices < 0) {
				continue;
			}

			std::vector<Vertex> vertices2;
			std::vector<uint32_t> indices2;

			glm::vec3 posMin{};
			glm::vec3 posMax{};
			bool hasSkin = false;
			// Vertices
			{
				const float* bufferPos = nullptr;
				const float* bufferNormals = nullptr;
				const float* bufferTexCoords = nullptr;
				const float* bufferColors = nullptr;
				const float* bufferTangents = nullptr;
				uint32_t numColorComponents;

				// Position attribute is required
				assert(primitive.attributes.find("POSITION") != primitive.attributes.end());

				const tinygltf::Accessor& posAccessor = model.accessors[primitive.attributes.find("POSITION")->second];
				const tinygltf::BufferView& posView = model.bufferViews[posAccessor.bufferView];
				bufferPos = reinterpret_cast<const float*>(&(model.buffers[posView.buffer].data[posAccessor.byteOffset + posView.byteOffset]));
				posMin = glm::vec3(posAccessor.minValues[0], posAccessor.minValues[1], posAccessor.minValues[2]);
				posMax = glm::vec3(posAccessor.maxValues[0], posAccessor.maxValues[1], posAccessor.maxValues[2]);

				if (primitive.attributes.find("NORMAL") != primitive.attributes.end()) {
					const tinygltf::Accessor& normAccessor = model.accessors[primitive.attributes.find("NORMAL")->second];
					const tinygltf::BufferView& normView = model.bufferViews[normAccessor.bufferView];
					bufferNormals = reinterpret_cast<const float*>(&(model.buffers[normView.buffer].data[normAccessor.byteOffset + normView.byteOffset]));
				}

				if (primitive.attributes.find("TEXCOORD_0") != primitive.attributes.end()) {
					const tinygltf::Accessor& uvAccessor = model.accessors[primitive.attributes.find("TEXCOORD_0")->second];
					const tinygltf::BufferView& uvView = model.bufferViews[uvAccessor.bufferView];
					bufferTexCoords = reinterpret_cast<const float*>(&(model.buffers[uvView.buffer].data[uvAccessor.byteOffset + uvView.byteOffset]));
				}

				if (primitive.attributes.find("COLOR_0") != primitive.attributes.end())
				{
					const tinygltf::Accessor& colorAccessor = model.accessors[primitive.attributes.find("COLOR_0")->second];
					const tinygltf::BufferView& colorView = model.bufferViews[colorAccessor.bufferView];
					// Color buffer are either of type vec3 or vec4
					numColorComponents = colorAccessor.type == TINYGLTF_PARAMETER_TYPE_FLOAT_VEC3 ? 3 : 4;
					bufferColors = reinterpret_cast<const float*>(&(model.buffers[colorView.buffer].data[colorAccessor.byteOffset + colorView.byteOffset]));
				}

				if (primitive.attributes.find("TANGENT") != primitive.attributes.end())
				{
					const tinygltf::Accessor& tangentAccessor = model.accessors[primitive.attributes.find("TANGENT")->second];
					const tinygltf::BufferView& tangentView = model.bufferViews[tangentAccessor.bufferView];
					bufferTangents = reinterpret_cast<const float*>(&(model.buffers[tangentView.buffer].data[tangentAccessor.byteOffset + tangentView.byteOffset]));
				}

				for (size_t v = 0; v < posAccessor.count; v++) {
					Vertex vertex{};
					vertex.Position = glm::vec4(glm::make_vec3(&bufferPos[v * 3]), 1.0f);
					vertex.Normal = glm::normalize(glm::vec3(bufferNormals ? glm::make_vec3(&bufferNormals[v * 3]) : glm::vec3(0.0f)));
					vertex.Uv = bufferTexCoords ? glm::make_vec2(&bufferTexCoords[v * 2]) : glm::vec3(0.0f);
					vertex.Tangent = bufferTangents ? glm::vec4(glm::make_vec4(&bufferTangents[v * 4])) : glm::vec4(0.0f);

					// TODO: For each vertex calculate a distance to center of the model/primitive?, which is going ot be used for culling later.

					const glm::mat4 localMatrix = newNode->getMatrix();
					vertex.Position = glm::vec3(localMatrix * glm::vec4(vertex.Position, 1.0f));
					vertex.Normal = glm::normalize(glm::mat3(localMatrix) * vertex.Normal);
					vertex.Position.y *= -1.0f;
					vertex.Normal.y *= -1.0f;

					vertices2.push_back(vertex);
				}
			}
			// Indices
			{
				const tinygltf::Accessor& accessor = model.accessors[primitive.indices];
				const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];
				const tinygltf::Buffer& buffer = model.buffers[bufferView.buffer];

				switch (accessor.componentType) {
				case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT: {
					uint32_t* buf = new uint32_t[accessor.count];
					memcpy(buf, &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(uint32_t));
					for (size_t index = 0; index < accessor.count; index++) {
						indices2.push_back(buf[index]);
					}
					break;
				}
				case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT: {
					uint16_t* buf = new uint16_t[accessor.count];
					memcpy(buf, &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(uint16_t));
					for (size_t index = 0; index < accessor.count; index++) {
						indices2.push_back(buf[index]);
					}
					break;
				}
				case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE: {
					uint8_t* buf = new uint8_t[accessor.count];
					memcpy(buf, &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(uint8_t));
					for (size_t index = 0; index < accessor.count; index++) {
						indices2.push_back(buf[index]);
					}
					break;
				}
				default:
					std::cerr << "Index component type " << accessor.componentType << " not supported!" << std::endl;
					return;
				}
			}

			Primitive* newPrimitive = new Primitive();

			newPrimitive->MaterialIndex = loadImages ? std::make_optional(primitive.material) : std::nullopt;

			{
				newPrimitive->m_Indices = indices2;

				size_t bufferSize = sizeof(indices2[0]) * indices2.size();

				Buffer stagingBuffer(m_Device, Fluent::BufferUsageFlags::TransferSrc,
					Fluent::MemoryAccess::Host, (uint32_t)bufferSize, "Staging Buffer");

				void* data = stagingBuffer.Map();
				memcpy(data, indices2.data(), bufferSize);
				stagingBuffer.Unmap();

				newPrimitive->m_IndexBuffer = new Buffer(m_Device, Fluent::BufferUsageFlags::TransferDst | Fluent::BufferUsageFlags::IndexBuffer,
					Fluent::MemoryAccess::Device, (uint32_t)bufferSize, "Index Buffer");

				m_Device->BeginTransferCommands();
				m_Device->CopyBufferToBuffer(&stagingBuffer, newPrimitive->m_IndexBuffer);
				m_Device->EndTransferCommands();
			}

			{
				newPrimitive->m_Vertices = vertices2;

				size_t bufferSize = sizeof(vertices2[0]) * vertices2.size();

				Buffer stagingBuffer(m_Device, Fluent::BufferUsageFlags::TransferSrc,
					Fluent::MemoryAccess::Host, (uint32_t)bufferSize, "Staging Buffer");

				void* data = stagingBuffer.Map();
				memcpy(data, vertices2.data(), bufferSize);
				stagingBuffer.Unmap();

				newPrimitive->m_VertexBuffer = new Buffer(m_Device, Fluent::BufferUsageFlags::TransferDst | Fluent::BufferUsageFlags::VertexBuffer,
					Fluent::MemoryAccess::Device, (uint32_t)bufferSize, "Vertex Buffer");

				m_Device->BeginTransferCommands();
				m_Device->CopyBufferToBuffer(&stagingBuffer, newPrimitive->m_VertexBuffer);
				m_Device->EndTransferCommands();
			}

			newPrimitive->SetDimensions(posMin, posMax);
			newMesh->primitives.push_back(newPrimitive);
		}
		newNode->mesh = newMesh;
	}

	if (parent)
	{
		parent->children.push_back(newNode);
	}
	else
	{
		m_Nodes.push_back(newNode);
	}

	m_LinearNodes.push_back(newNode);
}

void LoadImages(Device* m_Device, std::vector<TexturePtr>& textures, tinygltf::Model& gltfModel)
{
	textures.resize(gltfModel.images.size());
	for (uint32_t i = 0; i < gltfModel.images.size(); ++i)
	{
		tinygltf::Image& glTFImage = gltfModel.images[i];
		PHOTON_ASSERT(glTFImage.component == 4);

		TextureData textureData;
		textureData.Data = &glTFImage.image[0];
		textureData.Size = glTFImage.image.size();

		bool isLinearColorSpace = true;

		for (uint32_t j = 0; j < gltfModel.materials.size(); j++) {
			tinygltf::Material glTFMaterial = gltfModel.materials[j];

			if (glTFMaterial.pbrMetallicRoughness.baseColorTexture.index == i)
			{
				isLinearColorSpace = false;
			}
		}

		textures[i] = Texture::Create()
			.SetDevice(m_Device)
			.SetData(textureData)
			.SetWidth(glTFImage.width)
			.SetHeight(glTFImage.height)
			.SetFormat(isLinearColorSpace ? Fluent::Format::RGBA8_UNORM : Fluent::Format::RGBA8_UNORM_SRGB)
			.SetFlags(TextureFlags::Color | TextureFlags::Read)
			.SetAddressMode(Fluent::AddressMode::Repeat)
			.SetName(glTFImage.uri)
			.Build();
	}
}

void LoadMaterials(Device* m_Device, std::vector<Material>& materials, tinygltf::Model& gltfModel)
{
	materials.resize(gltfModel.materials.size());
	for (uint32_t i = 0; i < gltfModel.materials.size(); i++) {
		tinygltf::Material glTFMaterial = gltfModel.materials[i];

		materials[i].baseColorTextureIndex = glTFMaterial.pbrMetallicRoughness.baseColorTexture.index;
		materials[i].normalTextureIndex = glTFMaterial.normalTexture.index;
		materials[i].armTextureIndex = glTFMaterial.occlusionTexture.index;
	}
}

void Model::LoadFromFile(uint32_t fileLoadingFlags)
{
	tinygltf::Model gltfModel;
	tinygltf::TinyGLTF gltfContext;

	if (fileLoadingFlags & FileLoadingFlags::DontLoadImages)
	{
		gltfContext.SetImageLoader(loadImageDataFuncEmpty, nullptr);
	}
	else
	{
		gltfContext.SetImageLoader(loadImageDataFunc, nullptr);
	}

	std::string error, warning;
	PHOTON_ASSERT(gltfContext.LoadASCIIFromFile(&gltfModel, &error, &warning, m_FilePath));

	if (!(fileLoadingFlags & FileLoadingFlags::DontLoadImages))
	{
		LoadImages(m_Device, m_Textures, gltfModel);
	}

	LoadMaterials(m_Device, m_Materials, gltfModel);

	const tinygltf::Scene& scene = gltfModel.scenes[gltfModel.defaultScene > -1 ? gltfModel.defaultScene : 0];
	for (size_t i = 0; i < scene.nodes.size(); ++i)
	{
		const tinygltf::Node node = gltfModel.nodes[scene.nodes[i]];
		LoadNode(m_Device, m_Nodes, m_LinearNodes, nullptr, node, scene.nodes[i], gltfModel, !(fileLoadingFlags & FileLoadingFlags::DontLoadImages));
	}
}

Primitive::~Primitive()
{
	delete(m_VertexBuffer);
	delete(m_IndexBuffer);

	m_Vertices.clear();
	m_Indices.clear();
}

PHOTON_END_NAMESPACE
