#include "include/buffer.h"
#include "include/device.h"

PHOTON_BEGIN_NAMESPACE

Buffer::Buffer(Device* device, const Fluent::BufferUsageFlags usageFlags,
	const Fluent::MemoryAccess memoryAccessFlags, const uint32_t size, const std::string name)
	: m_UsageFlags(usageFlags)
	, m_MemoryAccessFlags(memoryAccessFlags)
	, m_Size(size)
	, m_Name(name)
{
	InitializeResource(device);
}

Buffer::~Buffer()
{
	delete(m_Resource);
}

void Buffer::InitializeResource(Device* device)
{
	Fluent::BufferResourceInfo bufferInfo;
	bufferInfo.UsageFlags = m_UsageFlags;
	bufferInfo.MemoryAccess = m_MemoryAccessFlags;
	bufferInfo.Size = m_Size;
	m_Resource = new Fluent::BufferResource(device->GetDevice(), bufferInfo, m_Name.c_str());
}

void* Buffer::Map() const
{
	return m_Resource->Map();
}

void Buffer::Unmap() const
{
	m_Resource->Unmap();
}

PHOTON_END_NAMESPACE
