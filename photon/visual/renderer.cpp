#include "include/renderer.h"
#include "include/device.h"
#include "include/swapchain.h"
#include "include/geometrypass.h"
#include "include/shadowpass.h"
#include "include/lightingpass.h"
#include "include/skyboxpass.h"
#include "include/renderset.h"
#include "include/framebuffer.h"
#include "include/texture.h"
#include "include/model.h"

PHOTON_BEGIN_NAMESPACE

#define SHADOW_MAP_WIDTH 2048
#define SHADOW_MAP_HEIGHT 2048

static const std::string MAIN_MODEL_PATH = DATA_PATH + "models/FlightHelmet/FlightHelmet.gltf";

Renderer::Renderer()
{
	InitializeScene();
	InitializeDepthStencil();
	InitializeLightingRenderTarget();
	InitializeGeometryPass();
	InitializeShadowPass();
	InitializeLightingPass();
	InitializeSkyboxPass();
}

Renderer::~Renderer()
{
	Device::Get().GetDevice()->WaitIdle();

	m_LightingRenderTarget = nullptr;
	m_DepthStencil = nullptr;

	delete(m_SkyboxFramebuffer);
	delete(m_SkyboxPass);
	delete(m_SkyboxRenderSet);

	delete(m_LightingFramebuffer);
	delete(m_LightingPass);
	delete(m_LightingRenderSet);

	delete(m_ShadowFramebuffer);
	delete(m_ShadowPass);
	delete(m_ShadowRenderSet);

	delete(m_GeometryFramebuffer);
	delete(m_GeometryPass);
	delete(m_GeometryRenderSet);

	delete(m_MainModel);

	delete(&Swapchain::Get());
	delete(&Device::Get());
}

void Renderer::InitializeDepthStencil()
{
	m_DepthStencil = Texture::Create()
		.SetDevice(&Device::Get())
		.SetWidth(SCREEN_WIDTH)
		.SetHeight(SCREEN_HEIGHT)
		.SetFormat(Fluent::Format::D32_SFLOAT)
		.SetFlags(TextureFlags::Read | TextureFlags::DepthStencil | TextureFlags::RenderTarget)
		.SetFilterType(Fluent::FilterType::Point)
		.SetName("Depth Stencil")
		.Build();
}

void Renderer::InitializeLightingRenderTarget()
{
	m_LightingRenderTarget = Texture::Create()
		.SetDevice(&Device::Get())
		.SetWidth(SCREEN_WIDTH)
		.SetHeight(SCREEN_HEIGHT)
		.SetFormat(Fluent::Format::RGBA16_FLOAT)
		.SetFlags(TextureFlags::Read | TextureFlags::Color | TextureFlags::RenderTarget)
		.SetName("Lighting Target")
		.Build();
}

void Renderer::InitializeGeometryPass()
{
	m_GeometryRenderSet = new RenderSet(SCREEN_WIDTH, SCREEN_HEIGHT);
	m_GeometryRenderSet->SetDepthStencil(m_DepthStencil);

	m_GeometryRenderSet->SetRenderTarget(Texture::Create()
		.SetDevice(&Device::Get())
		.SetWidth(SCREEN_WIDTH)
		.SetHeight(SCREEN_HEIGHT)
		.SetFormat(Fluent::Format::RGBA8_UNORM_SRGB)
		.SetFlags(TextureFlags::Read | TextureFlags::Color | TextureFlags::RenderTarget)
		.SetFilterType(Fluent::FilterType::Point)
		.SetName("Albedo Target")
		.Build());

	m_GeometryRenderSet->SetRenderTarget(Texture::Create()
		.SetDevice(&Device::Get())
		.SetWidth(SCREEN_WIDTH)
		.SetHeight(SCREEN_HEIGHT)
		.SetFormat(Fluent::Format::RGBA16_FLOAT)
		.SetFlags(TextureFlags::Read | TextureFlags::Color | TextureFlags::RenderTarget)
		.SetFilterType(Fluent::FilterType::Point)
		.SetName("Normal Target")
		.Build());

	m_GeometryRenderSet->SetRenderTarget(Texture::Create()
		.SetDevice(&Device::Get())
		.SetWidth(SCREEN_WIDTH)
		.SetHeight(SCREEN_HEIGHT)
		.SetFlags(TextureFlags::Read | TextureFlags::Color | TextureFlags::RenderTarget)
		.SetFilterType(Fluent::FilterType::Point)
		.SetName("AMR Target")
		.Build());

	m_GeometryPass = new GeometryPass(&Device::Get(), m_GeometryRenderSet, m_MainModel);
	m_GeometryFramebuffer = new Framebuffer(&Device::Get(), m_GeometryRenderSet, m_GeometryPass->GetRenderPass(), "Geometry");
}

void Renderer::InitializeShadowPass()
{
	m_ShadowRenderSet = new RenderSet(SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);

	m_ShadowRenderSet->SetDepthStencil(Texture::Create()
		.SetDevice(&Device::Get())
		.SetWidth(SHADOW_MAP_WIDTH)
		.SetHeight(SHADOW_MAP_HEIGHT)
		.SetFormat(Fluent::Format::D32_SFLOAT)
		.SetFlags(TextureFlags::Read | TextureFlags::DepthStencil | TextureFlags::RenderTarget)
		.SetFilterType(Fluent::FilterType::Linear)
		.SetName("Shadow Map")
		.Build());

	m_ShadowPass = new ShadowPass(&Device::Get(), m_ShadowRenderSet, m_MainModel);
	m_ShadowFramebuffer = new Framebuffer(&Device::Get(), m_ShadowRenderSet, m_ShadowPass->GetRenderPass(), "Shadow");
}

void Renderer::InitializeLightingPass()
{
	m_LightingRenderSet = new RenderSet(SCREEN_WIDTH, SCREEN_HEIGHT);
	m_LightingRenderSet->SetRenderTarget(m_LightingRenderTarget);

	m_LightingPass = new LightingPass(&Device::Get(), m_GeometryRenderSet, m_LightingRenderSet, m_ShadowRenderSet);
	m_LightingFramebuffer = new Framebuffer(&Device::Get(), m_LightingRenderSet, m_LightingPass->GetRenderPass(), "Lighting");
}

void Renderer::InitializeSkyboxPass()
{
	m_SkyboxRenderSet = new RenderSet(SCREEN_WIDTH, SCREEN_HEIGHT);
	m_SkyboxRenderSet->SetDepthStencil(m_DepthStencil);
	m_SkyboxRenderSet->SetRenderTarget(m_LightingRenderTarget);

	m_SkyboxPass = new SkyboxPass(&Device::Get(), m_GeometryRenderSet, m_SkyboxRenderSet);
	m_SkyboxFramebuffer = new Framebuffer(&Device::Get(), m_SkyboxRenderSet, m_SkyboxPass->GetRenderPass(), "Skybox");
}

void Renderer::InitializeScene()
{
	m_MainModel = new Model(&Device::Get(), MAIN_MODEL_PATH, "Scene");
}

void Renderer::DrawFrame()
{
	BeginFrame();

	m_GeometryPass->Draw(m_GeometryFramebuffer);
	m_ShadowPass->Draw(m_ShadowFramebuffer);
	m_LightingPass->Draw(m_LightingFramebuffer);
	m_SkyboxPass->Draw(m_SkyboxFramebuffer);
	Swapchain::Get().Present();

	EndFrame();
}

void Renderer::BeginFrame()
{
	m_GeometryPass->Setup();
	m_ShadowPass->Setup();
	m_LightingPass->Setup();
	m_SkyboxPass->Setup();

	Swapchain::Get().BeginFrame();
	Device::Get().BeginGraphicsCommands();
}

void Renderer::EndFrame()
{
	Device::Get().EndGraphicsCommands();
	Swapchain::Get().EndFrame();
}

PHOTON_END_NAMESPACE
