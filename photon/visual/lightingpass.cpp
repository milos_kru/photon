#include "include/lightingpass.h"
#include "include/device.h"
#include "include/renderset.h"
#include "include/framebuffer.h"
#include "include/buffer.h"
#include "include/texture.h"
#include "include/model.h"
#include "include/shader.h"
#include "include/uniformbuffer.h"

#include "engine/include/engine.h"
#include "engine/include/camera.h"

#include <chrono>

PHOTON_BEGIN_NAMESPACE

static const std::string BRDF_TEXTURE_PATH = DATA_PATH + "textures/brdf.ktx";
static const std::string LIGHTING_VERTEX_SHADER_PATH = DATA_PATH + "shaders/fullscreen.vert.spv";
static const std::string LIGHTING_FRAGMENT_SHADER_PATH = DATA_PATH + "shaders/lighting.frag.spv";
static const std::string DIFUSE_IRRADIANCE_TEXTURE_PATH = DATA_PATH + "textures/hdri/pisa_difuse_irradiance.ktx";
static const std::string PREFILTERED_TEXTURE_PATH = DATA_PATH + "textures/hdri/pisa_prefilter.ktx";

struct LightingData
{
	glm::mat4 Projection;
	glm::mat4 ProjectionViewInverse;
	glm::mat4 Model;
	glm::mat4 View;
	glm::mat4 DepthMVP;
	glm::vec3 CameraPosition;
};

struct LightsData // TODO: Delete!
{
	glm::vec4 lights[4];
	float exposure = 4.5f;
	float gamma = 2.2f;
};

LightingPass::LightingPass(Device* device, const RenderSet* geometryRenderSet,
	const RenderSet* lightingRenderSet, const RenderSet* shadowRenderSet)
	: RenderPass(device, lightingRenderSet, true, Fluent::ResourceState::None, Fluent::ResourceState::RenderTarget,
	Fluent::ResourceState::GenericRead, Fluent::ResourceState::DepthStencilWrite, "Lighting")
	, m_Device(device)
	, m_GeometryRenderSet(geometryRenderSet)
	, m_ShadowRenderSet(shadowRenderSet)
	, m_Camera(Photon::Engine::Get().GetCamera())
{
	m_DiffuseIrradiance = Texture::Create()
		.SetDevice(m_Device)
		.SetFilePath(DIFUSE_IRRADIANCE_TEXTURE_PATH)
		.SetFormat(Fluent::Format::RGBA32_FLOAT)
		.SetFlags(TextureFlags::Color | TextureFlags::Read | TextureFlags::CubeMap)
		.SetFilterType(Fluent::FilterType::Linear)
		.SetName("Diffuse Irradiance")
		.Build();

	m_Brdf = Texture::Create()
		.SetDevice(m_Device)
		.SetFilePath(BRDF_TEXTURE_PATH)
		.SetFormat(Fluent::Format::RG16_FLOAT)
		.SetFlags(TextureFlags::Color | TextureFlags::Read)
		.SetFilterType(Fluent::FilterType::Linear)
		.SetName("BRDF")
		.Build();

	m_Prefiltered = Texture::Create()
		.SetDevice(m_Device)
		.SetFilePath(PREFILTERED_TEXTURE_PATH)
		.SetFormat(Fluent::Format::RGBA16_FLOAT)
		.SetFlags(TextureFlags::Color | TextureFlags::Read | TextureFlags::CubeMap)
		.SetFilterType(Fluent::FilterType::Linear)
		.SetName("Prefiltered Skybox")
		.Build();

	m_LightingData = new UniformBuffer(m_Device, static_cast<uint32_t>(sizeof(LightingData)), "Lighting Data");
	m_LightsData = new UniformBuffer(m_Device, static_cast<uint32_t>(sizeof(LightsData)), "Lights Data");

	m_LightingVertexShader = new Shader(m_Device, LIGHTING_VERTEX_SHADER_PATH, "Lighting");
	m_LightingFragmentShader = new Shader(m_Device, LIGHTING_FRAGMENT_SHADER_PATH, "Lighting");
}

LightingPass::~LightingPass()
{
	delete(m_LightingData);
	delete(m_LightsData);

	delete(m_LightingVertexShader);
	delete(m_LightingFragmentShader);
}

void LightingPass::Setup()
{
	// TODO: Merge cbuffers?
	{
		static LightingData lightingData{};
		lightingData.Model = glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		lightingData.Projection = m_Camera.GetPerspectiveMatrix();
		lightingData.View = m_Camera.GetViewMatrix();
		lightingData.CameraPosition = m_Camera.GetPosition() * -1.0f;
		lightingData.ProjectionViewInverse = glm::inverse(lightingData.View) * glm::inverse(lightingData.Projection);

		glm::mat4 depthProjectionMatrix = glm::perspective(glm::radians(45.0f), 1.0f, 1.0f, 96.0f);
		glm::vec3 lightPosition = glm::vec3(-3.0f, -3.0f, -3.0f);
		glm::mat4 depthViewMatrix = glm::lookAt(lightPosition, glm::vec3(0.0f), glm::vec3(0, 1, 0));
		glm::mat4 depthModelMatrix = glm::mat4(1.0f);
		lightingData.DepthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

		void* data = m_LightingData->Map();
		memcpy(data, &lightingData, sizeof(lightingData));
		m_LightingData->Unmap();
	}

	{
		static LightsData lightsData{};

		const float p = 15.0f;
		lightsData.lights[0] = glm::vec4(-p, -p * 0.5f, -p, 1.0f);
		lightsData.lights[1] = glm::vec4(-p, -p * 0.5f, p, 1.0f);
		lightsData.lights[2] = glm::vec4(p, -p * 0.5f, p, 1.0f);
		lightsData.lights[3] = glm::vec4(p, -p * 0.5f, -p, 1.0f);
		lightsData.exposure = 4.5f;
		lightsData.gamma = 2.2f;

		void* data = m_LightsData->Map();
		memcpy(data, &lightsData, sizeof(lightsData));
		m_LightsData->Unmap();
	}
}

void LightingPass::Draw(const Framebuffer* framebuffer)
{
	m_Device->BeginDebugMarker("Lighting Pass", Fluent::DefaultColor::Yellow);
	m_Device->BeginRenderPass(m_RenderPass, framebuffer->GetFramebuffer());

	m_Device->SetShader(Fluent::ShaderType::Vertex, m_LightingVertexShader);
	m_Device->SetShader(Fluent::ShaderType::Fragment, m_LightingFragmentShader);
	m_Device->SetCullMode(Fluent::CullMode::Front);
	m_Device->SetWindingOrder(Fluent::WindingOrder::CounterClockwise);
	m_Device->SetDepthTestEnabled(false);
	m_Device->SetDepthWriteEnabled(false);
	m_Device->SetDepthCompare(Fluent::CompareOperation::Less);
	m_Device->SetColorWriteMask(0, Fluent::ColorWriteMaskFlags::RGB);

	m_Device->SetUniformBuffer(0, m_LightingData);
	m_Device->SetUniformBuffer(1, m_LightsData);

	m_Device->SetTexture(2, m_DiffuseIrradiance);
	m_Device->SetTexture(3, m_Brdf);
	m_Device->SetTexture(4, m_Prefiltered);

	m_Device->SetTexture(5, m_GeometryRenderSet->GetRenderTarget(0));
	m_Device->SetTexture(6, m_GeometryRenderSet->GetRenderTarget(1));
	m_Device->SetTexture(7, m_GeometryRenderSet->GetRenderTarget(2));
	
	m_Device->SetTexture(8, m_GeometryRenderSet->GetDepthStencil());
	m_Device->SetTexture(9, m_ShadowRenderSet->GetDepthStencil());

	m_Device->Draw(3);

	m_Device->EndRenderPass();
	m_Device->EndDebugMarker();
}

PHOTON_END_NAMESPACE
