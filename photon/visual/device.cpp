#include "include/device.h"
#include "include/devicestate.h"
#include "include/swapchain.h"
#include "include/shader.h"
#include "include/uniformbuffer.h"

PHOTON_BEGIN_NAMESPACE

Device::Device()
	: m_Name("Main")
{
	InitializeInstance();
	InitializeDevice();
	InitializeQueues();
	m_State = new DeviceState(this);
}

Device::~Device()
{
	delete(m_State);

	delete(m_TransferQueue);
	delete(m_ComputeQueue);
	delete(m_PresentQueue);
	delete(m_GraphicsQueue);

	delete(m_Device);
	delete(m_Instance);
}

void Device::InitializeInstance()
{
	Fluent::InstanceInfo instanceInfo;
	instanceInfo.ApplicationName = "Renderer"; // TODO: Hardcoded. Add defines.
	instanceInfo.EngineName = "Photon"; // TODO: Hardcoded.
	instanceInfo.EnableRenderDoc = true; // TODO: Whaaa.

#ifdef PHOTON_DEBUG
	instanceInfo.EnableValidations = true;
#else
	instanceInfo.EnableValidations = false;
#endif // GFX_DEBUG

	m_Instance = new Fluent::Instance(instanceInfo);
}

void Device::InitializeDevice()
{
	Fluent::DeviceInfo deviceInfo;

#ifdef PHOTON_DEBUG
	deviceInfo.EnableDebugMarkers = true;
#else
	deviceInfo.EnableDebugMarkers = false;
#endif // GFX_DEBUG

	m_Device = new Fluent::Device(m_Instance, deviceInfo, m_Name.c_str());
}

void Device::InitializeQueues()
{
	Fluent::QueueInfo graphicsQueueInfo{};
	graphicsQueueInfo.Type = Fluent::QueueType::Graphics;
	graphicsQueueInfo.Index = 0;
	m_GraphicsQueue = new Fluent::Queue(m_Device, graphicsQueueInfo, std::string(m_Name + " Graphics").c_str());

	Fluent::QueueInfo presentQueueInfo{};
	presentQueueInfo.Type = Fluent::QueueType::Present;
	presentQueueInfo.Index = 0;
	m_PresentQueue = new Fluent::Queue(m_Device, presentQueueInfo, std::string(m_Name + " Present").c_str());

	Fluent::QueueInfo computeQueueInfo{};
	computeQueueInfo.Type = Fluent::QueueType::Compute;
	computeQueueInfo.Index = 0;
	m_ComputeQueue = new Fluent::Queue(m_Device, computeQueueInfo, std::string(m_Name + " Compute").c_str());

	Fluent::QueueInfo transferQueueInfo{};
	transferQueueInfo.Type = Fluent::QueueType::Transfer;
	transferQueueInfo.Index = 0;
	m_TransferQueue = new Fluent::Queue(m_Device, transferQueueInfo, std::string(m_Name + " Transfer").c_str());
}

void Device::SetShader(const Fluent::ShaderType type, const Shader* shader)
{
	m_State->SetShader(type, shader);
}

void Device::SetTopology(const Fluent::Topology topology)
{
	m_State->SetTopology(topology);
}

void Device::SetMultisampleType(const Fluent::MultisampleType multisampleType)
{
	m_State->SetMultisampleType(multisampleType);
}

void Device::SetDepthWriteEnabled(const bool enabled)
{
	m_State->SetDepthWriteEnabled(enabled);
}

void Device::SetDepthTestEnabled(const bool enabled)
{
	m_State->SetDepthTestEnabled(enabled);
}

void Device::SetDepthCompare(const Fluent::CompareOperation compareFunction)
{
	m_State->SetDepthCompare(compareFunction);
}

void Device::SetDepthBias(const float depthBiasSlope, const float depthBias, const float depthBiasClamp)
{
	m_State->SetDepthBias(depthBiasSlope, depthBias, depthBiasClamp);
}

void Device::SetStencilEnable(const bool enabled)
{
	m_State->SetStencilEnable(enabled);
}

void Device::SetStencilOperation(const Fluent::StencilOperation stencilFail, const Fluent::StencilOperation depthFail, const Fluent::StencilOperation stencilPass)
{
	m_State->SetStencilOperation(stencilFail, depthFail, stencilPass);
}

void Device::SetStencilFunction(const Fluent::CompareOperation compareOperation, const uint8_t compareMask)
{
	m_State->SetStencilFunction(compareOperation, compareMask);
}

void Device::SetStencilWriteMask(const uint8_t mask)
{
	m_State->SetStencilWriteMask(mask);
}

void Device::SetWireFrameEnabled(const bool enabled)
{
	m_State->SetWireFrameEnabled(enabled);
}

void Device::SetCullMode(const Fluent::CullMode mode)
{
	m_State->SetCullMode(mode);
}

void Device::SetWindingOrder(const Fluent::WindingOrder winding)
{
	m_State->SetWindingOrder(winding);
}

void Device::SetColorWriteMask(const uint32_t mrtIndex, const Fluent::ColorWriteMaskFlags mask)
{
	m_State->SetColorWriteMask(mrtIndex, mask);
}

void Device::SetColorWriteMask(const uint32_t mrtIndex, const uint32_t mrtCount, const Fluent::ColorWriteMaskFlags masks[])
{
	m_State->SetColorWriteMask(mrtIndex, mrtCount, masks);
}

void Device::SetAlphaBlendEnabled(const uint32_t mrtIndex, const bool enabled)
{
	m_State->SetAlphaBlendEnabled(mrtIndex, enabled);
}

void Device::SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcBlend, const Fluent::BlendValue destBlend)
{
	m_State->SetAlphaBlendFunction(mrtIndex, srcBlend, destBlend);
}

void Device::SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcColorBlend,
	const Fluent::BlendValue destColorBlend, const Fluent::BlendValue srcAlphaBlend, const Fluent::BlendValue dstAlphablend)
{
	m_State->SetAlphaBlendFunction(mrtIndex, srcColorBlend, destColorBlend, srcAlphaBlend, dstAlphablend);
}

void Device::SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation)
{
	m_State->SetAlphaBlendOperation(mrtIndex, colorOperation);
}

void Device::SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation, const Fluent::BlendOperation alphaOperation)
{
	m_State->SetAlphaBlendOperation(mrtIndex, colorOperation, alphaOperation);
}

void Device::SetTexture(const uint32_t binding, const TexturePtr texture)
{
	m_State->SetTexture(binding, texture);
}

void Device::SetUniformBuffer(const uint32_t binding, UniformBuffer* uniformBuffer)
{
	m_State->SetUniformBuffer(binding, uniformBuffer);
}

void Device::BeginGraphicsCommands()
{
	m_State->BeginGraphicsCommands();
}

void Device::EndGraphicsCommands()
{
	m_State->EndGraphicsCommands();
}

// TODO: Pass Photon::RenderPass and Photon::Framebuffer instead.
void Device::BeginRenderPass(Fluent::RenderPass* renderPass, const Fluent::Framebuffer* framebuffer)
{
	m_State->BeginRenderPass(renderPass, framebuffer);
}

void Device::EndRenderPass()
{
	m_State->EndRenderPass();
}

void Device::BeginDebugMarker(const char* name, const Fluent::Color color)
{
	m_State->BeginDebugMarker(name, color);
}

void Device::EndDebugMarker()
{
	m_State->EndDebugMarker();
}

void Device::SetModel(Model* model)
{
	m_State->SetModel(model);
}

void Device::SetViewport(const Fluent::Viewport viewport)
{
	m_State->SetViewport(viewport);
}

void Device::SetScissorRect(const Fluent::Rect scissorRect)
{
	m_State->SetScissorRect(scissorRect);
}

void Device::Draw(const uint32_t vertexCount)
{
	m_State->Draw(vertexCount);
}

void Device::DrawIndexed(bool bindTextures)
{
	m_State->DrawIndexed(bindTextures);
}

void Device::BeginTransferCommands()
{
	m_State->BeginTransferCommands();
}

void Device::EndTransferCommands()
{
	m_State->EndTransferCommands();
}

void Device::CopyBufferToBuffer(const Buffer* srcBuffer, const Buffer* dstBuffer)
{
	m_State->CopyBufferToBuffer(srcBuffer, dstBuffer);
}

void Device::CopyBufferToTexture(const Buffer* srcBuffer, const Fluent::BufferRegion srcRegion,
	const Texture* dstTexture, const Fluent::ImageRegion dstRegion)
{
	m_State->CopyBufferToTexture(srcBuffer, srcRegion, dstTexture, dstRegion);
}

void Device::TransitionLayout(const Texture* texture, const Fluent::ResourceState stateBefore, const Fluent::ResourceState stateAfter)
{
	m_State->TransitionLayout(texture, stateBefore, stateAfter);
}

PHOTON_END_NAMESPACE
