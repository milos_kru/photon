#include "include/shader.h"
#include "include/device.h"

#include <fstream>

PHOTON_BEGIN_NAMESPACE

Shader::Shader(const Device* device, const std::string fileName, const std::string name)
	: m_FileName(fileName)
	, m_Name(name)
{
	InitializeShader(device);
}

Shader::~Shader()
{
	delete(m_Shader);
}

std::vector<char> LoadFileData(const std::string& filename)
{
	std::ifstream file(filename, std::ios::ate | std::ios::binary);
	PHOTON_ASSERT(file.is_open());

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);
	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();

	return buffer;
}

void Shader::InitializeShader(const Device* device)
{
	std::vector<char> byteCode = LoadFileData(m_FileName);
	
	Fluent::ShaderType type;
	std::string suffix;
	if (m_FileName.find(".vert") != std::string::npos)
	{
		type = Fluent::ShaderType::Vertex;
		suffix = " Vertex";
	}
	else if (m_FileName.find(".frag") != std::string::npos)
	{
		type = Fluent::ShaderType::Fragment;
		suffix = " Fragment";
	}
	else
	{
		PHOTON_ASSERT_ALWAYS("Unsupported shader type.");
	}

	Fluent::ShaderInfo shaderInfo;
	shaderInfo.Type = type;
	shaderInfo.CodeEntry = "main";
	m_Shader = new Fluent::Shader(device->GetDevice(), byteCode.size(), byteCode.data(), shaderInfo, (m_Name + suffix).c_str());
}

PHOTON_END_NAMESPACE
