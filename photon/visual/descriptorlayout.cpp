#include "include/descriptorlayout.h"
#include "include/device.h"
#include "include/shader.h"

#include <Crc32.h>

PHOTON_BEGIN_NAMESPACE

uint32_t DescriptorLayout::CalculateHash(const std::vector<Fluent::Shader*> shaders)
{
	uint32_t hash = 0;
	for (const auto* shader : shaders)
	{
		uint32_t shaderHash = shader->GetHash();
		hash = crc32_16bytes(&shaderHash, sizeof(shaderHash), hash);
	}
	return hash;
}

DescriptorLayout::DescriptorLayout(Device* device, const std::vector<Fluent::Shader*> shaders,
	const Fluent::DescriptorPool* descriptorPool, const std::string name)
{
	m_DescriptorSetLayout = new Fluent::DescriptorSetLayout(device->GetDevice(), shaders, name.c_str());

	m_DescriptorSets.resize(BACK_BUFFER_COUNT);
	for (uint32_t i = 0; i < m_DescriptorSets.size(); ++i)
	{
		m_DescriptorSets[i] = new Fluent::DescriptorSet(device->GetDevice(), descriptorPool,
			m_DescriptorSetLayout, name.c_str());
	}

	m_Hash = CalculateHash(shaders);
}

DescriptorLayout::~DescriptorLayout()
{
	delete(m_DescriptorSetLayout);

	for (uint32_t i = 0; i < m_DescriptorSets.size(); ++i)
	{
		delete(m_DescriptorSets[i]);
	}
	m_DescriptorSets.clear();
}

void DescriptorLayout::UpdateDescriptorSet(const uint32_t backBufferIndex,
	std::vector<Fluent::Descriptor> descriptors) const
{
	m_DescriptorSets[backBufferIndex]->Update(descriptors);
}

PHOTON_END_NAMESPACE
