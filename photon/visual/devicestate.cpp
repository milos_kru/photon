#include "include/devicestate.h"
#include "include/device.h"
#include "include/swapchain.h"
#include "include/buffer.h"
#include "include/texture.h"
#include "include/model.h"
#include "include/shader.h"
#include "include/descriptorlayout.h"
#include "include/uniformbuffer.h"

PHOTON_BEGIN_NAMESPACE

#define MAX_DESCRIPTOR_COUNT 256

DeviceState::DeviceState(Device* device)
	: m_Device(device)
	, m_CurrentCommandBuffer(std::nullopt)
	, m_CurrentModel(std::nullopt)
	, m_CurrentRenderPass(std::nullopt)
	, m_DescriptorDirty(false)
{
	InitializeCommandPools();
	InitializeCommandBuffers();
	InitializeDescriptorPool();
	ResetShaders();
}

DeviceState::~DeviceState()
{
	delete(m_DescriptorPool);

	for (uint32_t i = 0; i < m_GraphicsCommandBuffers.size(); ++i)
	{
		delete(m_GraphicsCommandBuffers[i]);
	}

	delete(m_TransferCommandBuffer);

	delete(m_TransferCommandPool);
	delete(m_GraphicsCommandPool);

	m_Descriptors.clear();

	for (auto const& entry : m_GraphicsPipelineMap)
	{
		delete(entry.second);
	}

	for (auto const& entry : m_DescriptorLayoutMap)
	{
		delete(entry.second);
	}
}

void DeviceState::InitializeCommandPools()
{
	m_GraphicsCommandPool = new Fluent::CommandPool(m_Device->GetDevice(), m_Device->GetGraphicsQueue(), "Graphics");
	m_TransferCommandPool = new Fluent::CommandPool(m_Device->GetDevice(), m_Device->GetTransferQueue(), "Transfer");
}

void DeviceState::InitializeCommandBuffers()
{
	m_GraphicsCommandBuffers.resize(BACK_BUFFER_COUNT);

	for (uint32_t i = 0; i < m_GraphicsCommandBuffers.size(); ++i)
	{
		m_GraphicsCommandBuffers[i] = new Fluent::GraphicsCommandBuffer(m_Device->GetDevice(),
			m_GraphicsCommandPool, "Main Graphics");
	}

	m_TransferCommandBuffer = new Fluent::TransferCommandBuffer(m_Device->GetDevice(), m_TransferCommandPool, "Main Transfer");
}

void DeviceState::InitializeGraphicsPipelineDescriptor()
{
	// TODO: Move this to model class as a default, with a potential to change it.
	std::vector<Fluent::VertexElement*> vertexElements;
	Fluent::VertexElementInfo vertexElementInfo;
	vertexElementInfo.Binding = 0;

	vertexElementInfo.Location = 0;
	vertexElementInfo.Format = Fluent::Format::RGB32_FLOAT;
	vertexElementInfo.Offset = offsetof(Vertex, Position);
	vertexElements.push_back(new Fluent::VertexElement(vertexElementInfo));

	vertexElementInfo.Location = 1;
	vertexElementInfo.Format = Fluent::Format::RGB32_FLOAT;
	vertexElementInfo.Offset = offsetof(Vertex, Normal);
	vertexElements.push_back(new Fluent::VertexElement(vertexElementInfo));

	vertexElementInfo.Location = 2;
	vertexElementInfo.Format = Fluent::Format::RG32_FLOAT;
	vertexElementInfo.Offset = offsetof(Vertex, Uv);
	vertexElements.push_back(new Fluent::VertexElement(vertexElementInfo));

	vertexElementInfo.Location = 3;
	vertexElementInfo.Format = Fluent::Format::RGBA32_FLOAT;
	vertexElementInfo.Offset = offsetof(Vertex, Tangent);
	vertexElements.push_back(new Fluent::VertexElement(vertexElementInfo));

	Fluent::VertexBindingInfo vertexBindingInfo;
	vertexBindingInfo.Binding = 0;
	vertexBindingInfo.Stride = sizeof(Vertex);
	vertexBindingInfo.InputRate = Fluent::VertexInputRate::PerVertex;
	Fluent::VertexBinding* vertexBinding = new Fluent::VertexBinding(vertexElements, vertexBindingInfo);

	m_GraphicsPipelineDescription.SetVertexBinding(vertexBinding);
}

void DeviceState::InitializeDescriptorPool()
{
	Fluent::DescriptorPoolInfo descriptorPoolInfo;
	descriptorPoolInfo.MaxDescriptorSets = static_cast<uint32_t>(BACK_BUFFER_COUNT) * 50;
	Fluent::DescriptorPoolSize uniformBufferPoolSize = { Fluent::DescriptorType::UniformBuffer, MAX_DESCRIPTOR_COUNT };
	Fluent::DescriptorPoolSize combinedImageSamplerPoolSize = { Fluent::DescriptorType::CombinedSampler, MAX_DESCRIPTOR_COUNT };
	descriptorPoolInfo.DescriptorSizes = { uniformBufferPoolSize, combinedImageSamplerPoolSize };
	m_DescriptorPool = new Fluent::DescriptorPool(m_Device->GetDevice(), descriptorPoolInfo, "TEMP");
}

void DeviceState::SetTopology(const Fluent::Topology topology)
{
	m_GraphicsPipelineDescription.SetTopology(topology);
}

void DeviceState::SetMultisampleType(const Fluent::MultisampleType multisampleType)
{
	m_GraphicsPipelineDescription.SetMultisampleType(multisampleType);
}

void DeviceState::SetDepthWriteEnabled(const bool enabled)
{
	m_GraphicsPipelineDescription.SetDepthWriteEnabled(enabled);
}

void DeviceState::SetDepthTestEnabled(const bool enabled)
{
	m_GraphicsPipelineDescription.SetDepthTestEnabled(enabled);
}

void DeviceState::SetDepthCompare(const Fluent::CompareOperation compareFunction)
{
	m_GraphicsPipelineDescription.SetDepthCompare(compareFunction);
}

void DeviceState::SetDepthBias(const float depthBiasSlope, const float depthBias, const float depthBiasClamp)
{
	m_GraphicsPipelineDescription.SetDepthBias(depthBiasSlope, depthBias, depthBiasClamp);
}

void DeviceState::SetStencilEnable(const bool enabled)
{
	m_GraphicsPipelineDescription.SetStencilEnable(enabled);
}

void DeviceState::SetStencilOperation(const Fluent::StencilOperation stencilFail,
	const Fluent::StencilOperation depthFail, const Fluent::StencilOperation stencilPass)
{
	m_GraphicsPipelineDescription.SetStencilOperation(stencilFail, depthFail, stencilPass);
}

void DeviceState::SetStencilFunction(const Fluent::CompareOperation compareOperation, const uint8_t compareMask)
{
	m_GraphicsPipelineDescription.SetStencilFunction(compareOperation, compareMask);
}

void DeviceState::SetStencilWriteMask(const uint8_t mask)
{
	m_GraphicsPipelineDescription.SetStencilWriteMask(mask);
}

void DeviceState::SetWireFrameEnabled(const bool enabled)
{
	m_GraphicsPipelineDescription.SetWireFrameEnabled(enabled);
}

void DeviceState::SetCullMode(const Fluent::CullMode mode)
{
	m_GraphicsPipelineDescription.SetCullMode(mode);
}

void DeviceState::SetWindingOrder(const Fluent::WindingOrder winding)
{
	m_GraphicsPipelineDescription.SetWindingOrder(winding);
}

void DeviceState::SetColorWriteMask(const uint32_t mrtIndex, const Fluent::ColorWriteMaskFlags mask)
{
	m_GraphicsPipelineDescription.SetColorWriteMask(mrtIndex, mask);
}

void DeviceState::SetColorWriteMask(const uint32_t mrtIndex, const uint32_t mrtCount, const Fluent::ColorWriteMaskFlags masks[])
{
	m_GraphicsPipelineDescription.SetColorWriteMask(mrtIndex, mrtCount, masks);
}

void DeviceState::SetAlphaBlendEnabled(const uint32_t mrtIndex, const bool enabled)
{
	m_GraphicsPipelineDescription.SetAlphaBlendEnabled(mrtIndex, enabled);
}

void DeviceState::SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcBlend, const Fluent::BlendValue dstBlend)
{
	m_GraphicsPipelineDescription.SetAlphaBlendFunction(mrtIndex, srcBlend, dstBlend);
}

void DeviceState::SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcColorBlend,
	const Fluent::BlendValue dstColorBlend, const Fluent::BlendValue srcAlphaBlend, const Fluent::BlendValue dstAlphablend)
{
	m_GraphicsPipelineDescription.SetAlphaBlendFunction(mrtIndex, srcColorBlend, dstColorBlend, srcAlphaBlend, dstAlphablend);
}

void DeviceState::SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation)
{
	m_GraphicsPipelineDescription.SetAlphaBlendOperation(mrtIndex, colorOperation);
}

void DeviceState::SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation,
	const Fluent::BlendOperation alphaOperation)
{
	m_GraphicsPipelineDescription.SetAlphaBlendOperation(mrtIndex, colorOperation, alphaOperation);
}

void DeviceState::SetTexture(const uint32_t binding, const TexturePtr texture) // TODO: Make m_Descriptors a fixed size and place textures to Binding slot.
{
	Fluent::CombinedImageSampler combindedImageSampler;
	combindedImageSampler.ImageView = texture->GetView();
	combindedImageSampler.ImageSampler = texture->GetSampler();

	Fluent::DescriptorInfo combindedImageSamplerDescriptorInfo;
	combindedImageSamplerDescriptorInfo.Type = Fluent::DescriptorType::CombinedSampler;
	combindedImageSamplerDescriptorInfo.Resource.CombinedImageSampler = &combindedImageSampler;
	combindedImageSamplerDescriptorInfo.Binding = binding;

	m_Descriptors.emplace_back(combindedImageSamplerDescriptorInfo);

	m_DescriptorDirty = true;
}

void DeviceState::SetUniformBuffer(const uint32_t binding, UniformBuffer* uniformBuffer)
{
	Fluent::DescriptorInfo uniformBufferDescriptorInfo;
	uniformBufferDescriptorInfo.Type = Fluent::DescriptorType::UniformBuffer;
	uniformBufferDescriptorInfo.Resource.UniformBuffer = uniformBuffer->GetBuffer()->GetResource();
	uniformBufferDescriptorInfo.Binding = binding;
	m_Descriptors.emplace_back(uniformBufferDescriptorInfo);

	m_DescriptorDirty = true;
}

void DeviceState::BeginTransferCommands()
{
	m_TransferCommandBuffer->Begin();
	m_CurrentCommandBuffer = std::make_optional(m_TransferCommandBuffer);
}

void DeviceState::EndTransferCommands()
{
	m_TransferCommandBuffer->End();
	m_Device->GetTransferQueue()->Submit({ m_TransferCommandBuffer });
	m_Device->GetTransferQueue()->WaitIdle();
	m_CurrentCommandBuffer = std::nullopt;
}

void DeviceState::CopyBufferToBuffer(const Buffer* srcBuffer, const Buffer* dstBuffer)
{
	Fluent::BufferResource* srcResource = srcBuffer->GetResource();
	Fluent::BufferResource* dstResource = dstBuffer->GetResource();
	m_TransferCommandBuffer->CopyResource(srcResource, dstResource);
}

void DeviceState::CopyBufferToTexture(const Buffer* srcBuffer, const Fluent::BufferRegion srcRegion,
	const Texture* dstTexture, const Fluent::ImageRegion dstRegion)
{
	Fluent::BufferResource* srcResource = srcBuffer->GetResource();
	Fluent::ImageResource* dstResource = dstTexture->GetResource();
	m_TransferCommandBuffer->CopyResourceRegion(srcResource, srcRegion, dstResource, dstRegion);
}

void DeviceState::BeginGraphicsCommands()
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->Begin();
	// TODO: Use m_CurrentCommandBuffer instead of getting it manually every time.
	m_CurrentCommandBuffer = std::make_optional(m_GraphicsCommandBuffers[Swapchain::Get().GetCurrentBackBufferIndex()]);
}

void DeviceState::EndGraphicsCommands()
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->End();
	m_CurrentCommandBuffer = std::nullopt;
}

void DeviceState::BeginRenderPass(Fluent::RenderPass* renderPass, const Fluent::Framebuffer* framebuffer)
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->BeginRenderPass(renderPass, framebuffer);

	m_CurrentRenderPass = std::make_optional(renderPass);
	m_GraphicsPipelineDescription.SetAttachmentCount(framebuffer->GetRenderTargetCount());

	Fluent::Viewport viewport;
	viewport.X = 0.0f;
	viewport.Y = 0.0f;
	viewport.Width = static_cast<float>(framebuffer->GetInfo().Width);
	viewport.Height = static_cast<float>(framebuffer->GetInfo().Height);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	SetViewport(viewport);

	Fluent::Rect scissorRect;
	scissorRect.Offset = { 0, 0 };
	scissorRect.Extent = { framebuffer->GetInfo().Width, framebuffer->GetInfo().Height };
	SetScissorRect(scissorRect);
}

void DeviceState::EndRenderPass()
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	PHOTON_ASSERT(m_CurrentRenderPass.has_value());
	m_GraphicsCommandBuffers[currentBackBufferIndex]->EndRenderPass(m_CurrentRenderPass.value());
	m_CurrentRenderPass = std::nullopt;
}

void DeviceState::BeginDebugMarker(const char* name, const Fluent::Color color)
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->BeginDebugMarker(name, color);
}

void DeviceState::EndDebugMarker()
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->EndDebugMarker();
}

void DeviceState::SetShader(const Fluent::ShaderType type, const Shader* shader)
{
	PHOTON_ASSERT(type == shader->GetShader()->GetInfo().Type);
	m_Shaders[static_cast<uint32_t>(type)] = shader;
}

void DeviceState::SetModel(Model* model)
{
	// TODO:
	//auto currentBackBufferIndex = m_Device->GetSwapchain()->GetCurrentBackBufferIndex();
	//m_GraphicsCommandBuffers[currentBackBufferIndex]->SetVertexBuffer(model->GetVertexBuffer()->GetResource(), 0);
	//m_GraphicsCommandBuffers[currentBackBufferIndex]->SetIndexBuffer(model->GetIndexBuffer()->GetResource(), 0);
	m_CurrentModel = std::make_optional(model);
}

void DeviceState::SetViewport(const Fluent::Viewport viewport)
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->SetViewport(viewport);
}

void DeviceState::SetScissorRect(const Fluent::Rect scissorRect)
{
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->SetScissorRect(scissorRect);
}

void DeviceState::BindGraphicsPipeline()
{
	if (!DEFAULT_TEX.has_value())
		DEFAULT_TEX = std::make_optional(Texture::Create()
			.SetDevice(m_Device)
			.SetWidth(32)
			.SetHeight(32)
			.SetFormat(Fluent::Format::RGBA8_UNORM_SRGB)
			.SetFlags(TextureFlags::Color | TextureFlags::Read)
			.SetAddressMode(Fluent::AddressMode::Repeat)
			.SetName("DEFAULT")
			.Build());


	std::vector<Fluent::Shader*> shaders;
	for (const auto* shader : m_Shaders)
	{
		if (shader)
		{
			shaders.push_back(shader->GetShader());
		}
	}

	m_GraphicsPipelineDescription.SetShaders(shaders);

	m_DescriptorLayout = m_DescriptorLayoutMap[DescriptorLayout::CalculateHash(shaders)];
	if (m_DescriptorLayout == nullptr)
	{
		m_DescriptorLayout = new DescriptorLayout(m_Device, shaders, m_DescriptorPool, "TEMP");
		m_DescriptorLayoutMap[m_DescriptorLayout->GetHash()] = m_DescriptorLayout;
	}

	uint32_t pipelineHash = m_GraphicsPipelineDescription.GetHash();
	Fluent::GraphicsPipeline* graphicsPipeline = m_GraphicsPipelineMap[pipelineHash];
	if (graphicsPipeline == nullptr)
	{
		if (m_CurrentModel.has_value())
		{
			InitializeGraphicsPipelineDescriptor();
		}

		PHOTON_ASSERT(m_CurrentRenderPass.has_value());
		 graphicsPipeline = new Fluent::GraphicsPipeline(m_Device->GetDevice(), m_CurrentRenderPass.value(),
			 m_DescriptorLayout->GetDescriptorSetLayout(), &m_GraphicsPipelineDescription, "TEMP");

		m_GraphicsPipelineMap[pipelineHash] = graphicsPipeline;
	}

	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	m_GraphicsCommandBuffers[currentBackBufferIndex]->SetGraphicsPipeline(graphicsPipeline);

	if (!m_CurrentModel.has_value())
	{
		if (m_DescriptorDirty)
		{
			m_DescriptorLayout->UpdateDescriptorSet(currentBackBufferIndex, m_Descriptors);
		}

		m_GraphicsCommandBuffers[currentBackBufferIndex]->SetDescriptorSet(0,
			m_DescriptorLayout->GetDescriptorSet(currentBackBufferIndex));
	}
}

void DeviceState::ResetShaders()
{
	for (uint32_t i = 0; i < static_cast<uint32_t>(Fluent::ShaderType::Count); ++i)
	{
		m_Shaders[i] = nullptr;
	}
}

void DeviceState::UnbindResources()
{
	m_CurrentModel = std::nullopt;
	ResetShaders();
	m_Descriptors.clear(); // TODO: Leak???
	// TODO: Unbind stuff here?

	m_DescriptorDirty = false;

	std::vector<Fluent::VertexElement*> vertexElements;
	Fluent::VertexElementInfo vertexElementInfo;
	vertexElementInfo.Binding = 0;

	Fluent::VertexBindingInfo vertexBindingInfo;
	vertexBindingInfo.Binding = 0;
	vertexBindingInfo.Stride = sizeof(Vertex);
	vertexBindingInfo.InputRate = Fluent::VertexInputRate::PerVertex;
	Fluent::VertexBinding* vertexBinding = new Fluent::VertexBinding(vertexElements, vertexBindingInfo);

	m_GraphicsPipelineDescription.SetVertexBinding(vertexBinding);
}

void DeviceState::Draw(const uint32_t vertexCount)
{
	BindGraphicsPipeline();

	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
	if (m_DescriptorDirty)
	{
		m_DescriptorLayout->UpdateDescriptorSet(currentBackBufferIndex, m_Descriptors);
	}

	PHOTON_ASSERT(!m_CurrentModel.has_value());
	m_GraphicsCommandBuffers[currentBackBufferIndex]->SetDescriptorSet(0,
		m_DescriptorLayout->GetDescriptorSet(currentBackBufferIndex));

	m_GraphicsCommandBuffers[currentBackBufferIndex]->Draw(vertexCount, 1, 0, 0);

	UnbindResources();
}

void DeviceState::DrawNode(Node* node, bool isShadow)
{
	// TODO: HACK!
	if (node->mesh)
	{
		PHOTON_ASSERT(m_CurrentModel.has_value());
		Model* model = m_CurrentModel.value();

		auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();
		auto commandBuffer = m_GraphicsCommandBuffers[currentBackBufferIndex];

		for (uint32_t j = 0; j < node->mesh->primitives.size(); ++j)
		{
			Primitive* primitive = node->mesh->primitives[j];
			commandBuffer->SetVertexBuffer(primitive->m_VertexBuffer->GetResource(), 0);
			commandBuffer->SetIndexBuffer(primitive->m_IndexBuffer->GetResource(), 0);

			if (primitive->MaterialIndex.has_value())
			{
				Material& material = model->m_Materials[primitive->MaterialIndex.value()];

				if (isShadow)
				{

				}
				else if (model->m_Textures.size() == 0)
				{
					SetTexture(1, DEFAULT_TEX.value());
					SetTexture(2, DEFAULT_TEX.value());
					SetTexture(3, DEFAULT_TEX.value());
				}
				else
				{
					TexturePtr albedo = model->m_Textures[material.baseColorTextureIndex];
					SetTexture(1, albedo);

					TexturePtr normal = model->m_Textures[material.normalTextureIndex];
					SetTexture(2, normal);

					TexturePtr arm = model->m_Textures[material.armTextureIndex];
					SetTexture(3, arm);
				}

				if (isShadow)
				{
					if (!material.shadowDescriptorsInitilaized)
					{
						material.InitializeShadow(m_Device->GetDevice(), m_DescriptorPool, m_DescriptorLayout->GetDescriptorSetLayout(), model->m_Name);
						material.shadowDescriptorsInitilaized = true;
					}

					//if (m_DescriptorDirty)
					{
						material.UpdateShadowDescriptorSet(currentBackBufferIndex, m_Descriptors);
					}

					m_GraphicsCommandBuffers[currentBackBufferIndex]->SetDescriptorSet(0, material.m_ShadowDescriptorSets[currentBackBufferIndex]);
				}
				else
				{
					if (!material.descriptorsInitilaized)
					{
						material.Initialize(m_Device->GetDevice(), m_DescriptorPool, m_DescriptorLayout->GetDescriptorSetLayout(), model->m_Name);
						material.descriptorsInitilaized = true;
					}

					if (m_DescriptorDirty)
					{
						material.UpdateDescriptorSet(currentBackBufferIndex, m_Descriptors);
					}

					m_GraphicsCommandBuffers[currentBackBufferIndex]->SetDescriptorSet(0, material.m_DescriptorSets[currentBackBufferIndex]);
				}

				uint32_t indexCount = static_cast<uint32_t>(primitive->m_Indices.size());
				commandBuffer->DrawIndexed(indexCount, 0, 0, 0, 0);

				//m_Descriptors.clear(); // TODO: Leak???
				if (!isShadow)
				{
					m_Descriptors.pop_back();
					m_Descriptors.pop_back();
					m_Descriptors.pop_back();
				}
				m_DescriptorDirty = false;
			}
			else
			{
				if (m_DescriptorDirty)
				{
					m_DescriptorLayout->UpdateDescriptorSet(currentBackBufferIndex, m_Descriptors);
				}

				m_GraphicsCommandBuffers[currentBackBufferIndex]->SetDescriptorSet(0,
					m_DescriptorLayout->GetDescriptorSet(currentBackBufferIndex));

				uint32_t indexCount = static_cast<uint32_t>(primitive->m_Indices.size());
				commandBuffer->DrawIndexed(indexCount, 0, 0, 0, 0);
			}
		}
	}

	for (auto& child : node->children)
	{
		DrawNode(child, isShadow);
	}
}

void DeviceState::DrawIndexed(bool bindTextures)
{
	BindGraphicsPipeline();
	PHOTON_ASSERT(m_CurrentModel.has_value());
	auto currentBackBufferIndex = Swapchain::Get().GetCurrentBackBufferIndex();

	Model* model = m_CurrentModel.value();
	for (auto& node : model->m_Nodes)
	{
		DrawNode(node, bindTextures);
	}

	UnbindResources();
}

void DeviceState::TransitionLayout(const Texture* texture,
	const Fluent::ResourceState stateBefore, const Fluent::ResourceState stateAfter)
{
	PHOTON_ASSERT(m_CurrentCommandBuffer.has_value());
	m_CurrentCommandBuffer.value()->TransitionResource(texture->GetResource(), stateBefore, stateAfter);
}

PHOTON_END_NAMESPACE
