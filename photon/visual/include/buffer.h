#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;

class Buffer
{
public:
	Buffer(Device* device, const Fluent::BufferUsageFlags usageFlags,
		const Fluent::MemoryAccess memoryAccessFlags, const uint32_t size, const std::string name);
	~Buffer();

private:
	void InitializeResource(Device* device);

public:
	void* Map() const;
	void Unmap() const;

public:
	Fluent::BufferResource* GetResource() const { return m_Resource; }
	Fluent::BufferUsageFlags GetUsageFlags() const { return m_UsageFlags; }
	Fluent::MemoryAccess GetMemoryAccessFlags() const { return m_MemoryAccessFlags; }
	uint32_t GetSize() const { return m_Size; }

private:
	Fluent::BufferResource* m_Resource;

	Fluent::BufferUsageFlags m_UsageFlags;
	Fluent::MemoryAccess m_MemoryAccessFlags;
	uint32_t m_Size;
	std::string m_Name;
};

PHOTON_END_NAMESPACE
