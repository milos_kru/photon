#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class RenderSet;
class RenderPass;

class Framebuffer
{
public:
	Framebuffer(const Device* device, const RenderSet* renderSet,
		const Fluent::RenderPass* renderPass, const std::string name);
	~Framebuffer();

public:
	inline Fluent::Framebuffer* GetFramebuffer() const { return m_Framebuffer; }

private:
	const Device* m_Device;

	Fluent::Framebuffer* m_Framebuffer;

	std::string m_Name;
};

PHOTON_END_NAMESPACE
