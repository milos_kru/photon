#pragma once

#include <fluent.h>

#include <string>
#include <vector>
#include <optional>
#include <iostream> 
#include <unordered_map>

#include "photon.h"

// TODO: Add ifdefs, because these are included in other header files.
#define BACK_BUFFER_COUNT 3
#define MAX_FRAMES_IN_FLIGHT 2

#define TexturePtr std::shared_ptr<Texture> // TODO: Make RefCounted base class.
#define TO_TexturePtr(texture) std::make_shared(texture)
