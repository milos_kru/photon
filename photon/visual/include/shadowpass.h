#pragma once

#include "visual.h"
#include "renderpass.h"

#include "engine/include/camera.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Framebuffer;
class UniformBuffer;
class Texture;
class Shader;
class Model;

class ShadowPass : public RenderPass
{
public:
	ShadowPass(Device* device, const RenderSet* renderSet, Model* model);
	~ShadowPass();

public:
	void Setup() override;
	void Draw(const Framebuffer* framebuffer) override;

private:
	Device* m_Device;
	UniformBuffer* m_ShadowData;
	Shader* m_VertexShader;
	Model* m_Model;
};

PHOTON_END_NAMESPACE
