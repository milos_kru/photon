#pragma once

#include "visual.h"

#include "engine/include/singleton.h"

PHOTON_BEGIN_NAMESPACE

class DeviceState;
class Buffer;
class Model;
class Shader;
class UniformBuffer;
class Texture;

class Device : public Photon::Singleton<Device>
{
public:
	Device();
	~Device();

private:
	void InitializeInstance();
	void InitializeDevice();
	void InitializeQueues();

public:
	void SetShader(const Fluent::ShaderType type, const Shader* shader);
	void SetTopology(const Fluent::Topology topology);
	void SetMultisampleType(const Fluent::MultisampleType multisampleType);
	void SetDepthWriteEnabled(const bool enabled);
	void SetDepthTestEnabled(const bool enabled);
	void SetDepthCompare(const Fluent::CompareOperation compareFunction);
	void SetDepthBias(const float depthBiasSlope, const float depthBias, const float depthBiasClamp);
	void SetStencilEnable(const bool enabled);
	void SetStencilOperation(const Fluent::StencilOperation stencilFail, const Fluent::StencilOperation depthFail,
		const Fluent::StencilOperation stencilPass);
	void SetStencilFunction(const Fluent::CompareOperation compareOperation, const uint8_t compareMask);
	void SetStencilWriteMask(const uint8_t mask);
	void SetWireFrameEnabled(const bool enabled);
	void SetCullMode(const Fluent::CullMode mode);
	void SetWindingOrder(const Fluent::WindingOrder winding);
	void SetColorWriteMask(const uint32_t mrtIndex, const Fluent::ColorWriteMaskFlags mask);
	void SetColorWriteMask(const uint32_t mrtIndex, const uint32_t mrtCount, const Fluent::ColorWriteMaskFlags masks[]);
	void SetAlphaBlendEnabled(const uint32_t mrtIndex, const bool enabled);
	void SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcBlend, const Fluent::BlendValue destBlend);
	void SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcColorBlend,
		const Fluent::BlendValue destColorBlend, const Fluent::BlendValue srcAlphaBlend, const Fluent::BlendValue dstAlphablend);
	void SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation);
	void SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation,
		const Fluent::BlendOperation alphaOperation);

public:
	void SetTexture(const uint32_t binding, const TexturePtr texture);
	void SetUniformBuffer(const uint32_t binding, UniformBuffer* uniformBuffer);

public:
	void BeginGraphicsCommands();
	void EndGraphicsCommands();
	void BeginRenderPass(Fluent::RenderPass* renderPass, const Fluent::Framebuffer* framebuffer);
	void EndRenderPass();
	void BeginDebugMarker(const char* name, const Fluent::Color color = Fluent::DefaultColor::White);
	void EndDebugMarker();
	void SetModel(Model* model);
	void SetViewport(const Fluent::Viewport viewport);
	void SetScissorRect(const Fluent::Rect scissorRect);
	void Draw(const uint32_t vertexCount);
	void DrawIndexed(bool bindTextures = false);

public:
	void BeginTransferCommands();
	void EndTransferCommands();
	void CopyBufferToBuffer(const Buffer* srcBuffer, const Buffer* dstBuffer);
	void CopyBufferToTexture(const Buffer* srcBuffer, const Fluent::BufferRegion srcRegion,
		const Texture* dstTexture, const Fluent::ImageRegion dstRegion);

public:
	void TransitionLayout(const Texture* texture,
		const Fluent::ResourceState stateBefore, const Fluent::ResourceState stateAfter);

public:
	Fluent::Instance* GetInstance() const { return m_Instance; }
	Fluent::Device* GetDevice() const { return m_Device; }
	Fluent::Queue* GetGraphicsQueue() const { return m_GraphicsQueue; }
	Fluent::Queue* GetPresentQueue() const { return m_PresentQueue; }
	Fluent::Queue* GetComputeQueue() const { return m_ComputeQueue; }
	Fluent::Queue* GetTransferQueue() const { return m_TransferQueue; }
	DeviceState* GetState() const { return m_State; }

private:
	Fluent::Instance* m_Instance;
	Fluent::Device* m_Device;
	Fluent::Queue* m_GraphicsQueue;
	Fluent::Queue* m_PresentQueue;
	Fluent::Queue* m_ComputeQueue;
	Fluent::Queue* m_TransferQueue;

	DeviceState* m_State;

	std::string m_Name;
};

PHOTON_END_NAMESPACE
