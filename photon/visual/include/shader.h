#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;

class Shader
{
public:
	Shader(const Device* device, const std::string fileName, const std::string name);
	~Shader();

private:
	void InitializeShader(const Device* device);

public:
	Fluent::Shader* GetShader() const { return m_Shader; }

private:
	Fluent::Shader* m_Shader;

	std::string m_FileName;
	std::string m_Name;
};

PHOTON_END_NAMESPACE
