#pragma once

#include "visual.h"
#include "renderpass.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Framebuffer;
class UniformBuffer;
class Texture;
class Shader;
class Model;
class Camera;

class GeometryPass : public RenderPass
{
public:
	GeometryPass(Device* device, const RenderSet* renderSet, Model* model);
	~GeometryPass();

public:
	void Setup() override;
	void Draw(const Framebuffer* framebuffer) override;

private:
	Device* m_Device;

	const Camera& m_Camera;

	UniformBuffer* m_GeometryData;

	const RenderSet* m_RenderSet;

	Shader* m_VertexShader;
	Shader* m_FragmentShader;

	Model* m_Model;
};

PHOTON_END_NAMESPACE
