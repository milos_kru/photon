#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Texture;

class RenderSet
{
public:
	RenderSet(const uint32_t width, const uint32_t height);
	~RenderSet();

public:
	void SetRenderTarget(TexturePtr renderTarget);
	void SetDepthStencil(TexturePtr depthStencil);

public:
	inline uint32_t GetRenderTargetCount() const { return m_RenderTargetCount; }
	inline bool HasDepthStencil() const { return m_DepthStencil.has_value(); }

public:
	inline uint32_t GetWidth() const { return m_Width; }
	inline uint32_t GetHeight() const { return m_Height; }

public:
	inline const std::shared_ptr<Texture> GetDepthStencil() const
	{
		PHOTON_ASSERT(m_DepthStencil.has_value());
		return m_DepthStencil.value();
	}

	inline const std::shared_ptr<Texture> GetRenderTarget(const uint32_t mrtIndex) const
	{
		PHOTON_ASSERT(mrtIndex < m_RenderTargetCount);
		return m_RenderTargets[mrtIndex];
	}

private:
	uint32_t m_Width;
	uint32_t m_Height;

	std::optional<TexturePtr> m_DepthStencil = std::nullopt;

	uint32_t m_RenderTargetCount = 0;
	TexturePtr m_RenderTargets[Fluent::MaxRenderTargetCount];
};

PHOTON_END_NAMESPACE
