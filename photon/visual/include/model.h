#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 Uv;
	glm::vec4 Tangent;

	Vertex() = default;
	Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 uv, glm::vec4 tangent)
		: Position(position), Normal(normal), Uv(uv), Tangent(tangent) {}

	bool operator==(const Vertex& other) const
	{
		return Position == other.Position && Normal == other.Normal && Uv == other.Uv && Tangent == other.Tangent;
	}
};

PHOTON_END_NAMESPACE

namespace std
{
	template <class T>
	inline void hash_combine(std::size_t& s, const T& v) // TODO: Generalize.
	{
		std::hash<T> h;
		s ^= h(v) + 0x9e3779b9 + (s << 6) + (s >> 2);
	}

	template<> struct hash<Photon::Vertex>
	{
		size_t operator()(Photon::Vertex const& vertex) const
		{
			std::size_t res = 0;
			hash_combine(res, vertex.Position);
			hash_combine(res, vertex.Normal);
			hash_combine(res, vertex.Uv);
			hash_combine(res, vertex.Tangent);
			return res;
		}
	};
}

PHOTON_BEGIN_NAMESPACE

class Device;
class Buffer;
class Texture;

enum FileLoadingFlags
{
	None = 0x00000000,
	PreTransformVertices = 0x00000001,
	FlipY = 0x00000002,
	DontLoadImages = 0x00000004
};

class Primitive
{
public:

	~Primitive(); // This has to be in the .cpp if buffer include is not in the .h file !!!

	struct Dimensions // TODO: FOR CULLING!!!!
	{
		glm::vec3 min = glm::vec3(FLT_MAX);
		glm::vec3 max = glm::vec3(-FLT_MAX);
		glm::vec3 size;
		glm::vec3 center;
		float radius;
	} dimensions;
	
	void SetDimensions(glm::vec3 min, glm::vec3 max)
	{
		dimensions.min = min;
		dimensions.max = max;
		dimensions.size = max - min;
		dimensions.center = (min + max) / 2.0f;
		dimensions.radius = glm::distance(min, max) / 2.0f;
	}

	std::vector<Vertex> m_Vertices;
	Buffer* m_VertexBuffer;

	std::vector<uint32_t> m_Indices;
	Buffer* m_IndexBuffer;

	std::optional<uint32_t> MaterialIndex;
};

struct Mesh
{
	std::vector<Primitive*> primitives;
	std::string name;

	~Mesh()
	{
		for (auto* primitive : primitives)
		{
			delete(primitive);
		}
		primitives.clear();
	}
};

struct Node
{
	Node* parent;
	uint32_t index;
	std::vector<Node*> children;
	glm::mat4 matrix;
	std::string name;
	Mesh* mesh;
	glm::vec3 translation{};
	glm::vec3 scale{ 1.0f };
	glm::quat rotation{};

	~Node()
	{
		delete(mesh);
	}

	glm::mat4 localMatrix()
	{
		return glm::translate(glm::mat4(1.0f), translation) * glm::mat4(rotation) * glm::scale(glm::mat4(1.0f), scale) * matrix;
	}

	glm::mat4 getMatrix()
	{
		glm::mat4 m = localMatrix();
		Node* p = parent;
		while (p) {
			m = p->localMatrix() * m;
			p = p->parent;
		}
		return m;
	}
};

struct Material
{
	~Material()
	{
		for (uint32_t i = 0; i < m_ShadowDescriptorSets.size(); ++i)
		{
			delete(m_ShadowDescriptorSets[i]);
		}
		m_ShadowDescriptorSets.clear();

		for (uint32_t i = 0; i < m_DescriptorSets.size(); ++i)
		{
			delete(m_DescriptorSets[i]);
		}
		m_DescriptorSets.clear();
	}

	uint32_t baseColorTextureIndex;
	uint32_t normalTextureIndex;
	uint32_t armTextureIndex;

	bool descriptorsInitilaized = false; // TODO: :/
	bool shadowDescriptorsInitilaized = false; // TODO: :/
	std::vector<Fluent::DescriptorSet*> m_DescriptorSets; // TODO: Wrap DescriptorSet?
	std::vector<Fluent::DescriptorSet*> m_ShadowDescriptorSets; // TODO: Wrap DescriptorSet?

	void Initialize(Fluent::Device* device, Fluent::DescriptorPool* pool,
		Fluent::DescriptorSetLayout* layout, std::string name)
	{
		m_DescriptorSets.resize(BACK_BUFFER_COUNT);
		for (uint32_t i = 0; i < m_DescriptorSets.size(); ++i)
		{
			m_DescriptorSets[i] = new Fluent::DescriptorSet(device, pool, layout, name.c_str());
		}
	}

	void InitializeShadow(Fluent::Device* device, Fluent::DescriptorPool* pool,
		Fluent::DescriptorSetLayout* layout, std::string name)
	{
		m_ShadowDescriptorSets.resize(BACK_BUFFER_COUNT);
		for (uint32_t i = 0; i < m_ShadowDescriptorSets.size(); ++i)
		{
			m_ShadowDescriptorSets[i] = new Fluent::DescriptorSet(device, pool, layout, name.c_str());
		}
	}

	inline void UpdateDescriptorSet(uint32_t backBufferIndex, std::vector<Fluent::Descriptor> descriptors) const
	{
		m_DescriptorSets[backBufferIndex]->Update(descriptors);
	}

	inline void UpdateShadowDescriptorSet(uint32_t backBufferIndex, std::vector<Fluent::Descriptor> descriptors) const
	{
		m_ShadowDescriptorSets[backBufferIndex]->Update(descriptors);
	}
};

class Model
{
public:
	Model(Device* device, const std::string filePath, const std::string name, bool loadImages = true);
	~Model();

private:
	void LoadFromFile(uint32_t fileLoadingFlags);

public: // TODO: To private.
	Device* m_Device;

	std::vector<Node*> m_Nodes;
	std::vector<Node*> m_LinearNodes;
	std::vector<TexturePtr> m_Textures;
	std::vector<Material> m_Materials;

	std::string m_FilePath;
	std::string m_Name;
};

PHOTON_END_NAMESPACE
