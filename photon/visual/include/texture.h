#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

enum class TextureFlags : uint8_t
{
	None			= 0x0 << 0,
	Color			= 0x1 << 0,
	DepthStencil	= 0x1 << 1,
	CubeMap			= 0x1 << 2,
	Read			= 0x1 << 3,
	RenderTarget	= 0x1 << 4,
	Swapchain		= 0x1 << 5
};
PHOTON_FLAG_TYPE_SETUP(TextureFlags)

struct TextureData
{
	unsigned char* Data;
	size_t Size;
};

class Device;
class TextureBuilder;

class Texture
{
public:
	static TextureBuilder Create();

public:
	~Texture();

private:
	void Initialize();
	void InitializeResource();
	void InitializeView();
	void InitializeSampler();

public:
	Fluent::ImageResource* GetResource() const { return m_Resource; }
	Fluent::ImageView* GetView() const { return m_View; }
	Fluent::ImageSampler* GetSampler() const { return m_Sampler; }

	uint32_t GetWidth() const { return m_Width; }
	uint32_t GetHeight() const { return m_Height; }
	uint32_t GetMipCount() const { return m_MipCount; }

	TextureFlags GetFlags() const { return m_Flags; }

	Fluent::Format GetFormat() const { return m_Format; }
	Fluent::AddressMode GetAddressMode() const { return m_AddressMode; }
	Fluent::FilterType GetFilterType() const { return m_FilterType; }

private:
	Device* m_Device = nullptr;

	Fluent::ImageResource* m_Resource = nullptr;
	Fluent::ImageView* m_View = nullptr;
	Fluent::ImageSampler* m_Sampler = nullptr;

	uint32_t m_Width = 0;
	uint32_t m_Height = 0;
	uint32_t m_MipCount = 1;

	TextureFlags m_Flags = TextureFlags::Read;

	Fluent::Format m_Format = Fluent::Format::RGBA8_UNORM;
	Fluent::AddressMode m_AddressMode = Fluent::AddressMode::Clamp;
	Fluent::FilterType m_FilterType = Fluent::FilterType::Anisotropic;

	std::optional<TextureData> m_Data = std::nullopt;

	std::optional<std::string> m_FilePath = std::nullopt;
	std::string m_Name = "Default";

	friend class TextureBuilder;
};

class TextureBuilder
{
public:
	TextureBuilder() : m_Texture(std::make_shared<Texture>()) {}

public:
	TextureBuilder& SetDevice(Device* device);
	TextureBuilder& SetFilePath(std::string filePath);
	TextureBuilder& SetData(TextureData data);
	TextureBuilder& SetWidth(uint32_t width);
	TextureBuilder& SetHeight(uint32_t height);
	TextureBuilder& SetMipCount(uint32_t mipCount);
	TextureBuilder& SetFormat(Fluent::Format format);
	TextureBuilder& SetFlags(TextureFlags flags);
	TextureBuilder& SetAddressMode(Fluent::AddressMode addressMode);
	TextureBuilder& SetFilterType(Fluent::FilterType filterType);
	TextureBuilder& SetResource(Fluent::ImageResource* resource);
	TextureBuilder& SetName(std::string name);
	TexturePtr Build();

private:
	TexturePtr m_Texture;
};

PHOTON_END_NAMESPACE
