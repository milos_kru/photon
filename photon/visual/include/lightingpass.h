#pragma once

#include "visual.h"
#include "renderpass.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Framebuffer;
class UniformBuffer;
class Texture;
class Shader;
class Model;
class Camera;

class LightingPass : public RenderPass
{
public:
	LightingPass(Device* device, const RenderSet* geometryRenderSet,
		const RenderSet* lightingRenderSet, const RenderSet* shadowRenderSet);
	~LightingPass();

public:
	void Setup() override;
	void Draw(const Framebuffer* framebuffer) override;

private:
	Device* m_Device;

	const Camera& m_Camera;

	const RenderSet* m_GeometryRenderSet;
	const RenderSet* m_ShadowRenderSet;

	TexturePtr m_DiffuseIrradiance;
	TexturePtr m_Prefiltered;
	TexturePtr m_Brdf;

	UniformBuffer* m_LightingData;
	UniformBuffer* m_LightsData;

	Shader* m_LightingVertexShader;
	Shader* m_LightingFragmentShader;
};

PHOTON_END_NAMESPACE
