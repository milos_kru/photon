#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Texture;
class Buffer;
class Model;
class Shader;
class DescriptorLayout;
class UniformBuffer;
struct Node;

class DeviceState
{
public:
	DeviceState(Device* device);
	~DeviceState();

private:
	void InitializeCommandPools();
	void InitializeCommandBuffers();
	void InitializeGraphicsPipelineDescriptor();
	void InitializeDescriptorPool();

public:
	void SetShader(const Fluent::ShaderType type, const Shader* shader);
	void SetTopology(const Fluent::Topology topology);
	void SetMultisampleType(const Fluent::MultisampleType multisampleType);
	void SetDepthWriteEnabled(const bool enabled);
	void SetDepthTestEnabled(const bool enabled);
	void SetDepthCompare(const Fluent::CompareOperation compareFunction);
	void SetDepthBias(const float depthBiasSlope, const float depthBias, const float depthBiasClamp);
	void SetStencilEnable(const bool enabled);
	void SetStencilOperation(const Fluent::StencilOperation stencilFail, const Fluent::StencilOperation depthFail,
		const Fluent::StencilOperation stencilPass);
	void SetStencilFunction(const Fluent::CompareOperation compareOperation, const uint8_t compareMask);
	void SetStencilWriteMask(const uint8_t mask);
	void SetWireFrameEnabled(const bool enabled);
	void SetCullMode(const Fluent::CullMode mode);
	void SetWindingOrder(const Fluent::WindingOrder winding);
	void SetColorWriteMask(const uint32_t mrtIndex, const Fluent::ColorWriteMaskFlags mask);
	void SetColorWriteMask(const uint32_t mrtIndex, const uint32_t mrtCount, const Fluent::ColorWriteMaskFlags masks[]);
	void SetAlphaBlendEnabled(const uint32_t mrtIndex, const bool enabled);
	void SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcBlend, const Fluent::BlendValue destBlend);
	void SetAlphaBlendFunction(const uint32_t mrtIndex, const Fluent::BlendValue srcColorBlend,
		const Fluent::BlendValue destColorBlend, const Fluent::BlendValue srcAlphaBlend, const Fluent::BlendValue dstAlphablend);
	void SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation);
	void SetAlphaBlendOperation(const uint32_t mrtIndex, const Fluent::BlendOperation colorOperation,
		const Fluent::BlendOperation alphaOperation);

public:
	void SetTexture(const uint32_t binding, const TexturePtr texture);
	void SetUniformBuffer(const uint32_t binding, UniformBuffer* uniformBuffer);

public:
	void BeginGraphicsCommands();
	void EndGraphicsCommands();
	void BeginRenderPass(Fluent::RenderPass* renderPass, const Fluent::Framebuffer* framebuffer);
	void EndRenderPass();
	void BeginDebugMarker(const char* name, const Fluent::Color color = Fluent::DefaultColor::White);
	void EndDebugMarker();
	void SetModel(Model* model);
	void SetViewport(const Fluent::Viewport viewport);
	void SetScissorRect(const Fluent::Rect scissorRect);
	void Draw(const uint32_t vertexCount);
	void DrawIndexed(bool bindTextures);

private:
	void DrawNode(Node* node, bool bindTextures);

private:
	void BindGraphicsPipeline();
	void UnbindResources();
	void ResetShaders();

public:
	void BeginTransferCommands();
	void EndTransferCommands();
	void CopyBufferToBuffer(const Buffer* srcBuffer, const Buffer* dstBuffer);
	void CopyBufferToTexture(const Buffer* srcBuffer, const Fluent::BufferRegion srcRegion,
		const Texture* dstTexture, const Fluent::ImageRegion dstRegion);

public:
	void TransitionLayout(const Texture* texture,
		const Fluent::ResourceState stateBefore, const Fluent::ResourceState stateAfter);

public:
	Fluent::GraphicsCommandBuffer* GetGraphicsCommandBuffer(const uint32_t index) const { return m_GraphicsCommandBuffers[index]; }
	Fluent::TransferCommandBuffer* GetTransferCommandBuffer() const { return m_TransferCommandBuffer; }

private:
	Device* m_Device;

	Fluent::CommandPool* m_GraphicsCommandPool;
	Fluent::CommandPool* m_TransferCommandPool;

	std::optional<Fluent::CommandBuffer*> m_CurrentCommandBuffer;
	std::vector<Fluent::GraphicsCommandBuffer*> m_GraphicsCommandBuffers;
	Fluent::TransferCommandBuffer* m_TransferCommandBuffer;

	const Shader* m_Shaders[static_cast<uint32_t>(Fluent::ShaderType::Count)];

	DescriptorLayout* m_DescriptorLayout;

	std::optional<Model*> m_CurrentModel;

	std::optional<Fluent::RenderPass*> m_CurrentRenderPass;

	Fluent::GraphicsPipelineDescription m_GraphicsPipelineDescription;
	std::unordered_map<uint32_t, Fluent::GraphicsPipeline*> m_GraphicsPipelineMap;

	Fluent::DescriptorPool* m_DescriptorPool;
	std::unordered_map<uint32_t, DescriptorLayout*> m_DescriptorLayoutMap;

	bool m_DescriptorDirty;
	std::vector<Fluent::Descriptor> m_Descriptors;

	std::optional<TexturePtr> DEFAULT_TEX;
};

PHOTON_END_NAMESPACE
