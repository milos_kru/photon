#pragma once

#include "visual.h"
#include "renderpass.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Framebuffer;
class UniformBuffer;
class Texture;
class Shader;
class Model;
class Camera;

class SkyboxPass : public RenderPass
{
public:
	SkyboxPass(Device* device, const RenderSet* geometryRenderSet, const RenderSet* lightingRenderSet);
	~SkyboxPass();

public:
	void Setup() override;
	void Draw(const Framebuffer* framebuffer) override;

private:
	Device* m_Device;

	const Camera& m_Camera;

	const RenderSet* m_GeometryRenderSet;

	UniformBuffer* m_SkyboxData;
	TexturePtr m_SkyboxTexture;

	Shader* m_SkyboxVertexShader;
	Shader* m_SkyboxFragmentShader;
	Model* m_SkyboxModel;
};

PHOTON_END_NAMESPACE
