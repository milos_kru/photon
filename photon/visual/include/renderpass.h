#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class RenderSet;
class Framebuffer;

class RenderPass
{
protected:
	RenderPass(const Device* device, const RenderSet* renderSet,
		const bool clearRenderTargets,
		const Fluent::ResourceState stateBeforeRenderTarget, const Fluent::ResourceState stateAfterRenderTarget,
		const Fluent::ResourceState stateBeforeDepthStencil, const Fluent::ResourceState stateAfterDepthStencil,
		const std::string name);

public:
	virtual ~RenderPass();

public:
	virtual void Setup() = 0;
	virtual void Draw(const Framebuffer* framebuffer) = 0;

public:
	inline Fluent::RenderPass* GetRenderPass() const { return m_RenderPass; }

protected:
	Fluent::RenderPass* m_RenderPass;

	std::string m_Name;
};

PHOTON_END_NAMESPACE
