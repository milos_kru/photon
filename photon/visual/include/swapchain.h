#pragma once

#include "visual.h"
#include "engine/include/singleton.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Framebuffer;
class RenderSet;
class RenderPass;
class Shader;
class Model;

class Swapchain : public Photon::Singleton<Swapchain>
{
public:
	Swapchain();
	~Swapchain();

private:
	void InitializePresentSurface();
	void InitailizeSwapchain();
	void InitializeRenderSets();
	void InitializeRenderPass();
	void InitializeShaders();
	void InitializeSynchronizationObjects();
	void InitializeFrameBuffers();

public:
	void BeginFrame();
	void EndFrame();

public:
	void Present();

public:
	Fluent::PresentSurface* GetPresentSurface() const { return m_PresentSurface; }
	Fluent::Swapchain* GetSwapchain() const { return m_Swapchain; }
	Framebuffer* GetFramebuffer() const { return m_Framebuffers[m_CurrentBackBufferIndex]; }
	uint32_t GetCurrentBackBufferIndex() const { return m_CurrentBackBufferIndex; }
	uint32_t GetCurrentFrameInFlightIndex() const { return m_CurrentFrame; }

private:
	Device* m_Device;

	Fluent::PresentSurface* m_PresentSurface;
	Fluent::Swapchain* m_Swapchain;

	std::vector<RenderSet*> m_RenderSets;

	Fluent::RenderPass* m_RenderPass;
	std::vector<Framebuffer*> m_Framebuffers;

	// TODO: Move these fields and logic to SwapchainPass.
	Shader* m_VertexShader;
	Shader* m_FragmentShader;

	std::vector<Fluent::Semaphore*> m_BackBufferAvailableSemaphores;
	std::vector<Fluent::Semaphore*> m_RenderFinishedSemaphores;
	std::vector<Fluent::Fence*> m_InFlightFences;
	std::vector<Fluent::Fence*> m_FencesByBackBufferIndex;

	uint32_t m_CurrentFrame;
	uint32_t m_CurrentBackBufferIndex;

	std::string m_Name;
};

PHOTON_END_NAMESPACE
