#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Shader;

class DescriptorLayout
{
public:
	static uint32_t CalculateHash(const std::vector<Fluent::Shader*> shaders);

public:
	DescriptorLayout(Device* device, const std::vector<Fluent::Shader*> shaders,
		const Fluent::DescriptorPool* descriptorPool, const std::string name);
	~DescriptorLayout();

public:
	void UpdateDescriptorSet(const uint32_t backBufferIndex,
		std::vector<Fluent::Descriptor> descriptors) const;

public:
	Fluent::DescriptorSetLayout* GetDescriptorSetLayout() const { return m_DescriptorSetLayout; }
	Fluent::DescriptorSet* GetDescriptorSet(const uint32_t backBufferIndex) const { return m_DescriptorSets[backBufferIndex]; }
	uint32_t GetHash() const { return m_Hash; }

private:
	Fluent::DescriptorSetLayout* m_DescriptorSetLayout;
	std::vector<Fluent::DescriptorSet*> m_DescriptorSets; // TODO: This shouldn't be here.

	uint32_t m_Hash;
	std::string m_Name;
};

PHOTON_END_NAMESPACE
