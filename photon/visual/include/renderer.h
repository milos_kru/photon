#pragma once

#include "visual.h"
#include "engine/include/singleton.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Swapchain;
class RenderPass;
class RenderSet;
class Framebuffer;
class Texture;
class Model;

class Renderer : public Photon::Singleton<Renderer>
{
public:
	Renderer();
	~Renderer();

private:
	void InitializeDepthStencil();
	void InitializeLightingRenderTarget();
	void InitializeGeometryPass();
	void InitializeShadowPass();
	void InitializeLightingPass();
	void InitializeSkyboxPass();
	void InitializeScene();

public:
	void DrawFrame();

private:
	void BeginFrame();
	void EndFrame();

public:
	inline RenderSet* GetGeometryRenderSet() const { return m_GeometryRenderSet; }
	inline RenderSet* GetShadowRenderSet() const { return m_ShadowRenderSet; }
	inline RenderSet* GetLightingRenderSet() const { return m_LightingRenderSet; }

private:
	RenderSet* m_GeometryRenderSet;
	Framebuffer* m_GeometryFramebuffer;
	RenderPass* m_GeometryPass;

	RenderSet* m_ShadowRenderSet;
	Framebuffer* m_ShadowFramebuffer;
	RenderPass* m_ShadowPass;

	RenderSet* m_LightingRenderSet;
	Framebuffer* m_LightingFramebuffer;
	RenderPass* m_LightingPass;

	RenderSet* m_SkyboxRenderSet;
	Framebuffer* m_SkyboxFramebuffer;
	RenderPass* m_SkyboxPass;

	TexturePtr m_DepthStencil;
	TexturePtr m_LightingRenderTarget;

	Model* m_MainModel;
};

PHOTON_END_NAMESPACE
