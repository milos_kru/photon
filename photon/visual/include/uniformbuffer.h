#pragma once

#include "visual.h"

PHOTON_BEGIN_NAMESPACE

class Device;
class Buffer;

class UniformBuffer
{
public:
	UniformBuffer(Device* device,
		const uint32_t size, const std::string name);
	~UniformBuffer();

public:
	void* Map() const;
	void Unmap() const;

public:
	Buffer* GetBuffer() const;

private:
	std::vector<Buffer*> m_Buffers;
	Device* m_Device;
};

PHOTON_END_NAMESPACE
