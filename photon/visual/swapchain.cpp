#include "include/swapchain.h"
#include "include/renderer.h"
#include "include/device.h" 
#include "include/framebuffer.h" 
#include "include/renderset.h" 
#include "include/devicestate.h"
#include "include/renderpass.h"
#include "include/texture.h"
#include "include/shader.h"
#include "include/model.h"

#include "engine/include/window.h"

static std::string VERTEX_SHADER_PATH = DATA_PATH + "shaders/fullscreen.vert.spv";
static std::string FRAGMENT_SHADER_PATH = DATA_PATH + "shaders/passthrough.frag.spv";

PHOTON_BEGIN_NAMESPACE

Swapchain::Swapchain()
	: m_Device(&Device::Get())
	, m_CurrentFrame(0)
	, m_CurrentBackBufferIndex(0)
	, m_Name("Main")
{
	InitializePresentSurface();
	InitailizeSwapchain();
	InitializeRenderSets();
	InitializeRenderPass();
	InitializeFrameBuffers();
	InitializeShaders();
	InitializeSynchronizationObjects();
}

Swapchain::~Swapchain()
{
	for (const auto& framebuffer : m_Framebuffers)
	{
		delete(framebuffer);
	}

	delete(m_Swapchain);

	for (const auto* renderSet : m_RenderSets)
	{
		delete(renderSet);
	}

	delete(m_RenderPass);

	delete(m_VertexShader);
	delete(m_FragmentShader);

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		delete(m_RenderFinishedSemaphores[i]);
		delete(m_BackBufferAvailableSemaphores[i]);
		delete(m_InFlightFences[i]);
	}

	delete(m_PresentSurface);
}

void Swapchain::InitializePresentSurface()
{
	Fluent::PresentSurfaceInfo viewSurfaceInfo{};
	viewSurfaceInfo.WindowInstance = Photon::Window::Get().GetInstance();
	viewSurfaceInfo.Windowhandle = Photon::Window::Get().GetHandle();

	Fluent::Instance* instance = m_Device->GetInstance();
	Fluent::Device* device = m_Device->GetDevice();
	Fluent::Queue* presentQueue = m_Device->GetPresentQueue();

	m_PresentSurface = new Fluent::PresentSurface(instance, device, presentQueue, viewSurfaceInfo, m_Name.c_str());
}

void Swapchain::InitailizeSwapchain()
{
	Fluent::SwapchainInfo swapchainInfo{};
	swapchainInfo.Extent.Width = SCREEN_WIDTH;
	swapchainInfo.Extent.Height = SCREEN_HEIGHT;
	swapchainInfo.EnableVSync = false;
	swapchainInfo.BufferCount = BACK_BUFFER_COUNT;
	swapchainInfo.Format = Fluent::Format::BGRA8_UNORM; // TODO: Make this SRGB and chaning everything else appropriately.
	swapchainInfo.ColorSpace = Fluent::ColorSpace::Linear;

	Fluent::Device* device = m_Device->GetDevice();
	Fluent::Queue* presentQueue = m_Device->GetPresentQueue();

	m_Swapchain = new Fluent::Swapchain(device, m_PresentSurface, presentQueue, swapchainInfo, m_Name.c_str());
}

void Swapchain::InitializeRenderSets()
{
	m_RenderSets.resize(m_Swapchain->GetBackBufferCount());

	for (uint32_t i = 0; i < m_Swapchain->GetBackBufferCount(); ++i)
	{
		m_RenderSets[i] = new RenderSet(SCREEN_WIDTH, SCREEN_HEIGHT);
		m_RenderSets[i]->SetRenderTarget(Texture::Create()
			.SetDevice(m_Device)
			.SetResource(m_Swapchain->GetBackBuffer(i))
			.SetFormat(Fluent::Format::BGRA8_UNORM_SRGB)
			.SetFlags(TextureFlags::RenderTarget | TextureFlags::Swapchain)
			.SetName("Back Buffer " + std::to_string(i))
			.Build());
	}
}

void Swapchain::InitializeRenderPass()
{
	Fluent::RenderPassInfo renderPassInfo;
	renderPassInfo.ClearDepth = true;
	renderPassInfo.ClearStencil = true;
	renderPassInfo.ClearRenderTargets = true;
	renderPassInfo.RenderTargetStateBefore = Fluent::ResourceState::None;
	renderPassInfo.RenderTargetStateAfter = Fluent::ResourceState::Present;
	renderPassInfo.DepthStencilStateBefore = Fluent::ResourceState::None;
	renderPassInfo.DepthStencilStateAfter = Fluent::ResourceState::None;

	m_RenderPass = new Fluent::RenderPass(m_Device->GetDevice(),
		{ m_RenderSets[0]->GetRenderTarget(0)->GetView() }, std::nullopt, renderPassInfo, "Swapchain");
}

void Swapchain::InitializeShaders()
{
	m_VertexShader = new Shader(m_Device, VERTEX_SHADER_PATH, "Swapchain");
	m_FragmentShader = new Shader(m_Device, FRAGMENT_SHADER_PATH, "Swapchain");
}

void Swapchain::InitializeSynchronizationObjects()
{
	m_BackBufferAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	m_RenderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	m_InFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
	m_FencesByBackBufferIndex.resize(BACK_BUFFER_COUNT, nullptr);

	Fluent::Device* device = m_Device->GetDevice();
	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		m_BackBufferAvailableSemaphores[i] = new Fluent::Semaphore(device, "Back Buffer Available");
		m_RenderFinishedSemaphores[i] = new Fluent::Semaphore(device, "Render Finished");

		m_InFlightFences[i] = new Fluent::Fence(device, "Inflight");
	}
}

void Swapchain::InitializeFrameBuffers()
{
	m_Framebuffers.resize(m_Swapchain->GetBackBufferCount());
	for (size_t i = 0; i < m_Framebuffers.size(); ++i)
	{
		m_Framebuffers[i] = new Framebuffer(m_Device, m_RenderSets[i], m_RenderPass, "Swapchain");
	}
}

void Swapchain::BeginFrame()
{
	m_InFlightFences[m_CurrentFrame]->Wait();

	auto currentBackBufferIndex = m_Swapchain->AcquireNextImage();
	if (!currentBackBufferIndex.has_value())
	{
		PHOTON_ASSERT_ALWAYS("Image not available. Swapchain recreate unsupported yet.")
		return;
	}

	m_CurrentBackBufferIndex = currentBackBufferIndex.value();
}

void Swapchain::EndFrame()
{
	if (m_FencesByBackBufferIndex[m_CurrentBackBufferIndex] != nullptr)
	{
		m_FencesByBackBufferIndex[m_CurrentBackBufferIndex]->Wait();
	}

	m_FencesByBackBufferIndex[m_CurrentBackBufferIndex] = m_InFlightFences[m_CurrentFrame];
	m_InFlightFences[m_CurrentFrame]->Reset();

	m_Device->GetGraphicsQueue()->Submit(
		{ m_Device->GetState()->GetGraphicsCommandBuffer(m_CurrentBackBufferIndex) },
		{ m_Swapchain->GetCurrentImageAcquiredSemaphore() },
		{ m_RenderFinishedSemaphores[m_CurrentFrame] },
		m_InFlightFences[m_CurrentFrame]);

	m_Swapchain->Present(m_RenderFinishedSemaphores[m_CurrentFrame]);
	m_CurrentFrame = (m_CurrentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Swapchain::Present()
{
	const TexturePtr texture = Renderer::Get().GetLightingRenderSet()->GetRenderTarget(0);

	m_Device->BeginDebugMarker("Present Pass", Fluent::DefaultColor::Cyan);
	m_Device->BeginRenderPass(m_RenderPass, m_Framebuffers[m_CurrentBackBufferIndex]->GetFramebuffer());

	m_Device->SetShader(Fluent::ShaderType::Vertex, m_VertexShader);
	m_Device->SetShader(Fluent::ShaderType::Fragment, m_FragmentShader);
	m_Device->SetCullMode(Fluent::CullMode::Front);
	m_Device->SetWindingOrder(Fluent::WindingOrder::CounterClockwise);
	m_Device->SetColorWriteMask(0, Fluent::ColorWriteMaskFlags::RGB);

	m_Device->SetTexture(0, texture);

	m_Device->Draw(3);

	m_Device->EndRenderPass();
	m_Device->EndDebugMarker();
}

PHOTON_END_NAMESPACE
