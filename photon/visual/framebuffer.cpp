#include "include/framebuffer.h"
#include "include/device.h"
#include "include/renderset.h"
#include "include/texture.h"
#include "include/renderpass.h"

PHOTON_BEGIN_NAMESPACE

Framebuffer::Framebuffer(const Device* device, const RenderSet* renderSet,
	const Fluent::RenderPass* renderPass, const std::string name)
	: m_Device(device)
	, m_Name(name)
{
	Fluent::FramebufferInfo framebufferInfo;
	framebufferInfo.Width = renderSet->GetWidth();
	framebufferInfo.Height = renderSet->GetHeight();

	std::vector<Fluent::ImageView*> renderTargets;
	for (uint32_t i = 0; i < renderSet->GetRenderTargetCount(); ++i)
	{
		const TexturePtr renderTarget = renderSet->GetRenderTarget(i);
		renderTargets.push_back(renderTarget->GetView());
	}

	std::optional<Fluent::ImageView*> depthStencil;
	if (renderSet->HasDepthStencil())
	{
		depthStencil = std::make_optional(renderSet->GetDepthStencil()->GetView());
	}

	m_Framebuffer = new Fluent::Framebuffer(device->GetDevice(), renderTargets, depthStencil,
		renderPass, framebufferInfo, m_Name.c_str());
}

Framebuffer::~Framebuffer()
{
	delete(m_Framebuffer);
}

PHOTON_END_NAMESPACE
