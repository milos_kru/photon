#include "include/renderset.h"
#include "include/texture.h"

PHOTON_BEGIN_NAMESPACE

RenderSet::RenderSet(const uint32_t width, const uint32_t height)
	: m_Width(width), m_Height(height) {}

RenderSet::~RenderSet()
{
	for (uint32_t i = 0; i < m_RenderTargetCount; ++i)
	{
		m_RenderTargets[m_RenderTargetCount] = nullptr;
	}

	if (m_DepthStencil.has_value())
	{
		m_DepthStencil.value() = nullptr;
	}
}

void RenderSet::SetRenderTarget(std::shared_ptr<Texture> renderTarget)
{
	PHOTON_ASSERT(m_RenderTargetCount < Fluent::MaxRenderTargetCount);
	PHOTON_ASSERT(renderTarget->GetWidth() == m_Width);
	PHOTON_ASSERT(renderTarget->GetHeight() == m_Height);

	m_RenderTargets[m_RenderTargetCount++] = renderTarget;
}

void RenderSet::SetDepthStencil(std::shared_ptr<Texture> depthStencil)
{
	PHOTON_ASSERT(!m_DepthStencil.has_value());
	PHOTON_ASSERT(depthStencil->GetWidth() == m_Width);
	PHOTON_ASSERT(depthStencil->GetHeight() == m_Height);

	m_DepthStencil = depthStencil;
}

PHOTON_END_NAMESPACE
