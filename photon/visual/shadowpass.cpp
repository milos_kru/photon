#include "include/shadowpass.h"
#include "include/device.h"
#include "include/framebuffer.h"
#include "include/buffer.h"
#include "include/texture.h"
#include "include/model.h"
#include "include/shader.h"
#include "include/uniformbuffer.h"

#include "engine/include/engine.h"
#include "engine/include/camera.h"

PHOTON_BEGIN_NAMESPACE

static const std::string SHADOW_VERTEX_SHADER_PATH = DATA_PATH + "shaders/geometry.vert.spv";

struct ShadowData
{
	glm::mat4 Projection;
	glm::mat4 Model;
	glm::mat4 View;
	glm::vec3 CameraPosition;
};

ShadowPass::ShadowPass(Device* device, const RenderSet* renderSet, Model* model)
	: RenderPass(device, renderSet, true, Fluent::ResourceState::None, Fluent::ResourceState::GenericRead,
	Fluent::ResourceState::None, Fluent::ResourceState::GenericRead, "Shadow")
	, m_Device(device)
	, m_Model(model)
{
	m_ShadowData = new UniformBuffer(m_Device, static_cast<uint32_t>(sizeof(ShadowData)), "Shadow Data");
	m_VertexShader = new Shader(m_Device, SHADOW_VERTEX_SHADER_PATH, "Shadow");
}

ShadowPass::~ShadowPass()
{
	delete(m_ShadowData);
	delete(m_VertexShader);
}

void ShadowPass::Setup()
{
	static ShadowData shadowData{};
	// TODO: Hardcoded...
	shadowData.Model = glm::mat4(1.0f);
	glm::vec3 lightPosition = glm::vec3(-3.0f, -3.0f, -3.0f);
	shadowData.View = glm::lookAt(lightPosition, glm::vec3(0.0f), glm::vec3(0, 1, 0));
	shadowData.Projection = glm::perspective(glm::radians(45.0f), 1.0f, 1.0f, 96.0f);

	void* data = m_ShadowData->Map();
	memcpy(data, &shadowData, sizeof(shadowData));
	m_ShadowData->Unmap();
}

void ShadowPass::Draw(const Framebuffer* framebuffer)
{
	m_Device->BeginDebugMarker("Shadow Pass", Fluent::DefaultColor::White);
	m_Device->BeginRenderPass(m_RenderPass, framebuffer->GetFramebuffer());

	m_Device->SetShader(Fluent::ShaderType::Vertex, m_VertexShader);
	m_Device->SetCullMode(Fluent::CullMode::Back);
	m_Device->SetWindingOrder(Fluent::WindingOrder::CounterClockwise);
	m_Device->SetDepthTestEnabled(true);
	m_Device->SetDepthWriteEnabled(true);
	m_Device->SetDepthCompare(Fluent::CompareOperation::Less);

	m_Device->SetDepthBias(1.75f, 1.25f, 0.0f);

	m_Device->SetUniformBuffer(0, m_ShadowData);

	m_Device->SetModel(m_Model);
	m_Device->DrawIndexed(true);

	m_Device->SetDepthBias(0.0f, 0.0f, 0.0f);

	m_Device->EndRenderPass();
	m_Device->EndDebugMarker();
}

PHOTON_END_NAMESPACE
