#include "include/skyboxpass.h"
#include "include/device.h"
#include "include/renderset.h"
#include "include/framebuffer.h"
#include "include/buffer.h"
#include "include/texture.h"
#include "include/model.h"
#include "include/shader.h"
#include "include/uniformbuffer.h"

#include "engine/include/engine.h"
#include "engine/include/camera.h"

#include <chrono>

PHOTON_BEGIN_NAMESPACE

static const std::string SKYBOX_TEXTURE_PATH = DATA_PATH + "textures/hdri/pisa.ktx";
static const std::string SKYBOX_VERTEX_SHADER_PATH = DATA_PATH + "shaders/skybox.vert.spv";
static const std::string SKYBOX_FRAGMENT_SHADER_PATH = DATA_PATH + "shaders/skybox.frag.spv";
static const std::string SKYBOX_MODEL_PATH = DATA_PATH + "models/cube.gltf";

struct SkyboxData
{
	glm::mat4 Projection;
	glm::mat4 Model;
};

SkyboxPass::SkyboxPass(Device* device, const RenderSet* geometryRenderSet, const RenderSet* lightingRenderSet)
	: RenderPass(device, lightingRenderSet, false, Fluent::ResourceState::RenderTarget, Fluent::ResourceState::GenericRead,
	Fluent::ResourceState::GenericRead, Fluent::ResourceState::DepthStencilWrite, "Skybox")
	, m_Device(device)
	, m_GeometryRenderSet(geometryRenderSet)
	, m_Camera(Photon::Engine::Get().GetCamera())
{
	m_SkyboxData = new UniformBuffer(m_Device, static_cast<uint32_t>(sizeof(SkyboxData)), "Skybox Data");

	m_SkyboxTexture = Texture::Create()
		.SetDevice(m_Device)
		.SetFilePath(SKYBOX_TEXTURE_PATH)
		.SetFormat(Fluent::Format::RGBA16_FLOAT)
		.SetFlags(TextureFlags::Color | TextureFlags::Read | TextureFlags::CubeMap)
		.SetName("Skybox")
		.Build();

	m_SkyboxVertexShader = new Shader(m_Device, SKYBOX_VERTEX_SHADER_PATH, "Skybox");
	m_SkyboxFragmentShader = new Shader(m_Device, SKYBOX_FRAGMENT_SHADER_PATH, "Skybox");

	m_SkyboxModel = new Model(m_Device, SKYBOX_MODEL_PATH, "Skybox", false);
}

SkyboxPass::~SkyboxPass()
{
	delete(m_SkyboxData);

	delete(m_SkyboxVertexShader);
	delete(m_SkyboxFragmentShader);

	delete(m_SkyboxModel);
}

void SkyboxPass::Setup()
{
	static SkyboxData skyboxData{};
	skyboxData.Model = glm::mat4(glm::mat3(m_Camera.GetViewMatrix()));
	skyboxData.Projection = m_Camera.GetPerspectiveMatrix();

	void* data = m_SkyboxData->Map();
	memcpy(data, &skyboxData, sizeof(skyboxData));
	m_SkyboxData->Unmap();
}

void SkyboxPass::Draw(const Framebuffer* framebuffer)
{
	m_Device->BeginDebugMarker("Skybox Pass", Fluent::DefaultColor::Blue);
	m_Device->BeginRenderPass(m_RenderPass, framebuffer->GetFramebuffer());

	m_Device->SetShader(Fluent::ShaderType::Vertex, m_SkyboxVertexShader);
	m_Device->SetShader(Fluent::ShaderType::Fragment, m_SkyboxFragmentShader);
	m_Device->SetCullMode(Fluent::CullMode::Front);
	m_Device->SetWindingOrder(Fluent::WindingOrder::CounterClockwise);
	m_Device->SetDepthTestEnabled(true);
	m_Device->SetDepthWriteEnabled(false);
	m_Device->SetColorWriteMask(0, Fluent::ColorWriteMaskFlags::RGB);

	m_Device->SetUniformBuffer(0, m_SkyboxData);
	m_Device->SetTexture(1, m_SkyboxTexture);

	m_Device->SetModel(m_SkyboxModel);
	m_Device->DrawIndexed();

	m_Device->EndRenderPass();
	m_Device->EndDebugMarker();
}

PHOTON_END_NAMESPACE
