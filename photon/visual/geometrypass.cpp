#include "include/geometrypass.h"
#include "include/device.h"
#include "include/framebuffer.h"
#include "include/buffer.h"
#include "include/texture.h"
#include "include/model.h"
#include "include/shader.h"
#include "include/uniformbuffer.h"
#include "include/renderset.h"

#include "engine/include/engine.h"
#include "engine/include/camera.h"

PHOTON_BEGIN_NAMESPACE

static const std::string LIGHTING_VERTEX_SHADER_PATH = DATA_PATH + "shaders/geometry.vert.spv";
static const std::string LIGHTING_FRAGMENT_SHADER_PATH = DATA_PATH + "shaders/geometry.frag.spv";

struct GeometryData
{
	glm::mat4 Projection;
	glm::mat4 Model;
	glm::mat4 View;
	glm::vec3 CameraPosition;
};

GeometryPass::GeometryPass(Device* device, const RenderSet* renderSet, Model* model)
	: RenderPass(device, renderSet, true, Fluent::ResourceState::None, Fluent::ResourceState::GenericRead,
	Fluent::ResourceState::None, Fluent::ResourceState::GenericRead, "Geometry")
	, m_Device(device)
	, m_RenderSet(renderSet)
	, m_Camera(Photon::Engine::Get().GetCamera())
	, m_Model(model)
{
	m_GeometryData = new UniformBuffer(m_Device, static_cast<uint32_t>(sizeof(GeometryData)), "Geometry Data");

	m_VertexShader = new Shader(m_Device, LIGHTING_VERTEX_SHADER_PATH, "Geometry");
	m_FragmentShader = new Shader(m_Device, LIGHTING_FRAGMENT_SHADER_PATH, "Geometry");
}

GeometryPass::~GeometryPass()
{
	delete(m_GeometryData);

	delete(m_VertexShader);
	delete(m_FragmentShader);
}

void GeometryPass::Setup()
{
	static GeometryData geometryData{};
	geometryData.Model = glm::mat4(1.0f);
	geometryData.View = m_Camera.GetViewMatrix();
	geometryData.Projection = m_Camera.GetPerspectiveMatrix();
	geometryData.CameraPosition = m_Camera.GetPosition() * -1.0f;

	void* data = m_GeometryData->Map();
	memcpy(data, &geometryData, sizeof(geometryData));
	m_GeometryData->Unmap();
}

void GeometryPass::Draw(const Framebuffer* framebuffer)
{
	m_Device->BeginDebugMarker("Geometry Pass", Fluent::DefaultColor::Green);
	m_Device->BeginRenderPass(m_RenderPass, framebuffer->GetFramebuffer());

	{
		m_Device->SetShader(Fluent::ShaderType::Vertex, m_VertexShader);
		m_Device->SetShader(Fluent::ShaderType::Fragment, m_FragmentShader);
		m_Device->SetCullMode(Fluent::CullMode::Back);
		m_Device->SetWindingOrder(Fluent::WindingOrder::CounterClockwise);
		m_Device->SetDepthTestEnabled(true);
		m_Device->SetDepthWriteEnabled(true);
		m_Device->SetDepthCompare(Fluent::CompareOperation::Less);
		m_Device->SetColorWriteMask(0, Fluent::ColorWriteMaskFlags::RGB);
		m_Device->SetColorWriteMask(1, Fluent::ColorWriteMaskFlags::RGB);
		m_Device->SetColorWriteMask(2, Fluent::ColorWriteMaskFlags::RGB);
		m_Device->SetColorWriteMask(3, Fluent::ColorWriteMaskFlags::RGB);

		m_Device->SetUniformBuffer(0, m_GeometryData);

		m_Device->SetModel(m_Model);
		m_Device->DrawIndexed(); // TODO: Maybe pass model as argument here and have multiple Draw signatures.
	}

	m_Device->EndRenderPass();
	m_Device->EndDebugMarker();
}

PHOTON_END_NAMESPACE
