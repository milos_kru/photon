#include "include/renderpass.h"
#include "include/device.h"
#include "include/renderset.h"
#include "include/texture.h"

PHOTON_BEGIN_NAMESPACE

RenderPass::RenderPass(const Device* device, const RenderSet* renderSet,
	const bool clearRenderTargets,
	const Fluent::ResourceState stateBeforeRenderTarget, const Fluent::ResourceState stateAfterRenderTarget,
	const Fluent::ResourceState stateBeforeDepthStencil, const Fluent::ResourceState stateAfterDepthStencil,
	const std::string name)
	: m_Name(name)
{
	Fluent::RenderPassInfo renderPassInfo;
	renderPassInfo.ClearDepth = clearRenderTargets;
	renderPassInfo.ClearStencil = clearRenderTargets;
	renderPassInfo.ClearRenderTargets = clearRenderTargets;
	renderPassInfo.RenderTargetStateBefore = stateBeforeRenderTarget;
	renderPassInfo.RenderTargetStateAfter = stateAfterRenderTarget;
	renderPassInfo.DepthStencilStateBefore = stateBeforeDepthStencil;
	renderPassInfo.DepthStencilStateAfter = stateAfterDepthStencil;

	std::vector<Fluent::ImageView*> renderTargetViews;

	for (uint32_t i = 0; i < renderSet->GetRenderTargetCount(); ++i)
	{
		renderTargetViews.push_back(renderSet->GetRenderTarget(i)->GetView());
	}

	std::optional<Fluent::ImageView*> depthStencil = std::nullopt;
	if (renderSet->HasDepthStencil())
	{
		depthStencil = std::make_optional(renderSet->GetDepthStencil()->GetView());
	}

	m_RenderPass = new Fluent::RenderPass(device->GetDevice(),
		renderTargetViews, depthStencil, renderPassInfo, m_Name.c_str());
}

RenderPass::~RenderPass()
{
	delete(m_RenderPass);
}

PHOTON_END_NAMESPACE
