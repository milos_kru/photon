#pragma once

FLUENT_BEGIN_NAMESPACE

class GraphicsPipeline;

class GraphicsCommandBuffer : public CommandBuffer
{
public:
	GraphicsCommandBuffer(const Device* device,
		const CommandPool* commandPool,
		const char* name);

public:
	void SetVertexBuffer(const BufferResource* vertexBuffer, const uint32_t vertexBufferOffset);
	void SetIndexBuffer(const BufferResource* indexBuffer, const uint32_t indexBufferOffset);
	void SetViewport(const Viewport viewport);
	void SetScissorRect(const Rect scissorRect);
	void SetStencilReference(const uint32_t stencilRef);
	void SetGraphicsPipeline(const GraphicsPipeline* pipeline);
	void BeginRenderPass(const RenderPass* renderPass, const Framebuffer* framebuffer);
	void EndRenderPass(const RenderPass* renderPass);
	void ClearRenderTarget(const ImageView* renderTarget, const Color clearValue);
	void ClearDepthStencil(const ImageView* depthStencil, const ClearDepthStencilFlags flags, const DepthStencil clearValue);
	void Draw(const uint32_t numVertices, const uint32_t numInstances, const uint32_t startVertex, const uint32_t startInstance);
	void DrawIndexed(const uint32_t numIndices, const uint32_t numInstances, const uint32_t startIndex, const uint32_t startVertex, const uint32_t startInstance);
	void DrawInstancedIndirect(const BufferResource* indirectBuffer, const uint32_t indirectBufferOffset, const uint32_t drawCount);
	void DrawIndexedInstancedIndirect(const BufferResource* indirectBuffer, const uint32_t indirectBufferOffset, const uint32_t drawCount);
	void SetDescriptorSet(const uint32_t slotIndex, const DescriptorSet* descriptorSet);
};

FLUENT_END_NAMESPACE
