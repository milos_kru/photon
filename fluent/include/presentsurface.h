#pragma once

FLUENT_BEGIN_NAMESPACE

struct PresentSurfaceInfo
{
	HINSTANCE WindowInstance;
	HWND Windowhandle;
};

class PresentSurface
{
public:
	PresentSurface(const Instance* instance,
		const Device* device,
		const Queue* presentQueue,
		const PresentSurfaceInfo& info,
		const char* name);
	~PresentSurface();

public:
	inline const PresentSurfaceInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const PresentSurfaceInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
