#pragma once

#include <vector>
#include <optional>
#include <memory>

#ifdef _WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#endif // _WIN32

#define FLUENT_BEGIN_NAMESPACE namespace Fluent \
    {

#define FLUENT_END_NAMESPACE }

#ifndef NDEBUG
	#define FLUENT_DEBUG
#endif // !NDEBUG

FLUENT_BEGIN_NAMESPACE

typedef std::shared_ptr<void> StateHandle;

FLUENT_END_NAMESPACE
