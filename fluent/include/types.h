#pragma once

FLUENT_BEGIN_NAMESPACE

enum class MemoryAccess : uint8_t
{
	Host,
	Device,

	Count
};

enum class QueueType : uint8_t
{
	Graphics,
	Present,
	Compute,
	Transfer,

	Count
};

enum class PresentResult : uint8_t
{
	Valid,
	Invalid,

	Count
};

enum class Format : uint8_t
{
	UNDEFINED,
	D32_SFLOAT_S8_UINT,
	D32_SFLOAT,
	BGRA8_UNORM,
	BGRA8_UNORM_SRGB,
	RGBA8_UNORM,
	RGBA8_UNORM_SRGB,
	R8_UNORM,
	R32_SFLOAT,
	RGBA16_FLOAT,
	RG16_FLOAT,
	RGBA32_FLOAT,
	RGB32_FLOAT,
	RG32_FLOAT,

	Count
};

enum class ColorSpace : uint8_t
{
	SRGB,
	Linear,

	Count
};

enum class ResourceState : uint8_t
{
	None,
	TransferSrc,
	TransferDst,
	DepthStencilRead,
	DepthStencilWrite,
	GenericRead,
	RenderTarget,
	Present,

	Count
};

enum class ShaderType : uint8_t
{
	Vertex,
	Fragment,
	Geometry,
	Hull,
	Domain,
	Compute,

	Count
};

enum class Topology : uint8_t
{
	PointList,
	LineList,
	LineStrip,
	TriangleList,
	TriangleStrip,
	PatchList,

	Count
};

enum class MultisampleType : uint8_t
{
	Sample_1,
	Sample_2,
	Sample_4,
	Sample_8,

	Count
};

enum class CompareOperation : uint8_t
{
	Never,
	Less,
	Equal,
	LessEqual,
	Greater,
	NotEqual,
	GreaterEqual,
	Always,

	Count
};

enum class StencilOperation : uint8_t
{
	Keep,
	Zero,
	Replace,
	IncrementAndClamp,
	DecrementAndClamp,
	Invert,
	Increment,
	Decrement,

	Count
};

enum class CullMode : uint8_t
{
	None,
	Back,
	Front,

	Count
};

enum class WindingOrder : uint8_t
{
	Clockwise,
	CounterClockwise,

	Count
};

enum class FilterType : uint8_t
{
	Point,
	Linear,
	Anisotropic,

	Count
};

enum class AddressMode : uint8_t
{
	Repeat,
	Mirror,
	Clamp,
	Border,
	MirrorClamp,

	Count
};

enum class BlendValue : uint8_t
{
	Zero,
	One,
	SrcColor,
	InvSrcColor,
	SrcAlpha,
	InvSrcAlpha,
	DestAlpha,
	InvDestAlpha,
	DstColor,
	InvDstColor,
	BlendFactor,
	InvBlendFactor,
	SrcAlphaSat,
	Src1Color,
	InvSrc1Color,
	Src1Alpha,
	InvSrc1Alpha,

	Count
};

enum class BlendOperation : uint8_t
{
	Add,
	Subtract,
	ReverseSubtract,
	Min,
	Max,

	Count
};

enum class VertexInputRate : uint8_t
{
	PerVertex,
	PerInstance,

	Count
};

enum class DescriptorType : uint8_t
{
	CombinedSampler,
	UniformBuffer,

	Count
};

FLUENT_END_NAMESPACE
