#pragma once

FLUENT_BEGIN_NAMESPACE

class DescriptorSet
{
public:
	DescriptorSet(const Device* device,
		const DescriptorPool* desciptorPool,
		const DescriptorSetLayout* descriptorSetLayout,
		const char* name);

public:
	void Update(const std::vector<Descriptor> descriptors) const;

public:
	inline const StateHandle GetState() const { return m_State; }

private:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
