#pragma once

FLUENT_BEGIN_NAMESPACE

class Fence
{
public:
	Fence(const Device* device,
		const char* name);
	~Fence();

public:
	void Wait();
	void Reset();

public:
	inline const StateHandle GetState() const { return m_State; }

private:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
