#pragma once

FLUENT_BEGIN_NAMESPACE

class CommandBuffer;
class Semaphore;
class Fence;

struct QueueInfo
{
	QueueType Type;
	uint32_t Index;
};

class Queue
{
public:
	Queue(const Device* device,
		const QueueInfo& info,
		const char* name);

public:
	void Submit(const std::vector<CommandBuffer*> commandBuffers,
		const std::vector<Semaphore*> semaphoresToWait = {},
		const std::vector<Semaphore*> semaphoresToSignal = {},
		const std::optional<Fence*> fenceToWait = std::nullopt);
	void WaitIdle();

public:
	inline const QueueInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const QueueInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
