#pragma once

FLUENT_BEGIN_NAMESPACE

struct BufferResourceInfo
{
	BufferUsageFlags UsageFlags;
	MemoryAccess MemoryAccess;
	uint32_t Size;
};

class BufferResource
{
public:
	BufferResource(const Device* device,
		const BufferResourceInfo& info,
		const char* name);
	~BufferResource();

public:
	void* Map() const;
	void Unmap() const;

public:
	inline const BufferResourceInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const BufferResourceInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
