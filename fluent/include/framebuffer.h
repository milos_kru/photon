#pragma once

FLUENT_BEGIN_NAMESPACE

const uint32_t MaxRenderTargetCount = 5;

struct FramebufferInfo
{
	uint32_t Width;
	uint32_t Height;
};

class Framebuffer
{
public:
	Framebuffer(const Device* device,
		const std::vector<ImageView*> renderTargetViews,
		const std::optional<ImageView*> depthStencilView,
		const RenderPass* renderPass,
		const FramebufferInfo& info,
		const char* name);
	~Framebuffer();

public:
	inline const FramebufferInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }
	inline ImageView* GetRenderTarget(const uint32_t index) const { return m_RenderTargets[index]; }
	inline uint32_t GetRenderTargetCount() const { return static_cast<uint32_t>(m_RenderTargets.size()); }
	inline std::optional<ImageView*> GetDepthStencil() const { return m_DepthStencil; }

private:
	const FramebufferInfo m_Info;
	const StateHandle m_State;
	const std::vector<ImageView*> m_RenderTargets;
	const std::optional<ImageView*> m_DepthStencil;
};

FLUENT_END_NAMESPACE
