#pragma once

FLUENT_BEGIN_NAMESPACE

struct DeviceInfo
{
	bool EnableDebugMarkers;
};

class Device
{
public:
	Device(const Instance* instance,
		const DeviceInfo& info,
		const char* name);
	~Device();

public:
	void WaitIdle();

public:
	inline const DeviceInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const DeviceInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
