#pragma once

FLUENT_BEGIN_NAMESPACE

class TransferCommandBuffer : public CommandBuffer
{
public:
	TransferCommandBuffer(const Device* device,
		const CommandPool* commandPool,
		const char* name);

public:
	void CopyResource(const BufferResource* srcBuffer, const BufferResource* dstBuffer);
	void CopyResource(const ImageResource* srcImage, const ImageResource* dstImage);
	void CopyResourceRegion(const BufferResource* srcBuffer, const BufferRegion& srcBufferRegion,
		const BufferResource* dstBuffer, const BufferRegion& dstBufferRegion);
	void CopyResourceRegion(const BufferResource* srcBuffer, const BufferRegion& srcBufferRegion,
		const ImageResource* dstImage, const ImageRegion& dstImageRegion);
	void CopyResourceRegion(const ImageResource* srcImage, const ImageRegion& srcImageRegion,
		const BufferResource* dstBuffer, const BufferRegion& dstBufferRegion);
	void CopyResourceRegion(const ImageResource* srcImage, const ImageRegion& srcImageRegion,
		const ImageResource* dstImage, const ImageRegion& dstImageRegion);
};

FLUENT_END_NAMESPACE
