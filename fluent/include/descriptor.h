#pragma once

FLUENT_BEGIN_NAMESPACE

struct CombinedImageSampler
{
	ImageView* ImageView;
	ImageSampler* ImageSampler;
};

union DescriptorResourceHandle
{
	CombinedImageSampler* CombinedImageSampler;
	BufferResource* UniformBuffer;
};

struct DescriptorInfo
{
	DescriptorType Type;
	DescriptorResourceHandle Resource;
	uint32_t Binding;
};

class Descriptor
{
public:
	Descriptor(const DescriptorInfo& info);

public:
	inline const DescriptorInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const DescriptorInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
