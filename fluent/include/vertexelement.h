#pragma once

FLUENT_BEGIN_NAMESPACE

struct VertexElementInfo
{
	uint32_t Binding;
	uint32_t Location;
	Format Format;
	uint32_t Offset;
};

class VertexElement
{
public:
	VertexElement(const VertexElementInfo& info);

public:
	inline const VertexElementInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const VertexElementInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
