#pragma

FLUENT_BEGIN_NAMESPACE

class ImageResource;
class Semaphore;

struct SwapchainInfo
{
	Extent2D Extent;
	bool EnableVSync;
	uint32_t BufferCount;
	Format Format;
	ColorSpace ColorSpace;
};

class Swapchain
{
public:
	Swapchain(const Device* device,
		const PresentSurface* presentSurface,
		const Queue* presentQueue,
		const SwapchainInfo& info,
		const char* name);
	~Swapchain();

public:
	void Recreate(const Device* device, const Extent2D extent = { 0, 0 });
	std::optional<uint32_t> AcquireNextImage();
	PresentResult Present(const Semaphore* waitSemaphore) const;

public:
	inline const SwapchainInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }
	inline ImageResource* GetBackBuffer(const uint32_t index) const { return m_BackBuffers[index]; }
	inline size_t GetBackBufferCount() const { return m_BackBuffers.size(); }
	inline Semaphore* GetCurrentImageAcquiredSemaphore() const { return m_ImageAcquiredSemaphores[m_CurrentSemaphoreIndex]; }

private:
	SwapchainInfo m_Info;
	const StateHandle m_State;
	uint32_t m_CurrentBackBufferIndex;
	std::vector<ImageResource*> m_BackBuffers;
	uint32_t m_CurrentSemaphoreIndex;
	std::vector<Semaphore*> m_ImageAcquiredSemaphores;
};

FLUENT_END_NAMESPACE
