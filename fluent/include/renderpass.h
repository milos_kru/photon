#pragma once

FLUENT_BEGIN_NAMESPACE

struct RenderPassInfo
{
	bool ClearRenderTargets;
	bool ClearDepth;
	bool ClearStencil;
	ResourceState RenderTargetStateBefore;
	ResourceState RenderTargetStateAfter;
	ResourceState DepthStencilStateBefore;
	ResourceState DepthStencilStateAfter;
};

class RenderPass
{
public:
	RenderPass(const Device* device,
		const std::vector<ImageView*> renderTargetViews,
		const std::optional<ImageView*> depthStencilView,
		const RenderPassInfo& info,
		const char* name);
	~RenderPass();

public:
	inline const RenderPassInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const RenderPassInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
