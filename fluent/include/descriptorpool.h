#pragma once

FLUENT_BEGIN_NAMESPACE

struct DescriptorPoolSize
{
	DescriptorType Type;
	uint32_t Count;
};

struct DescriptorPoolInfo
{
	uint32_t MaxDescriptorSets;
	std::vector<DescriptorPoolSize> DescriptorSizes;
};

class DescriptorPool
{
public:
	DescriptorPool(const Device* device,
		const DescriptorPoolInfo& descriptor,
		const char* name);
	~DescriptorPool();

public:
	inline const DescriptorPoolInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const DescriptorPoolInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
