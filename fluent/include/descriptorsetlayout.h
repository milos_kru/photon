#pragma once

FLUENT_BEGIN_NAMESPACE

class DescriptorSetLayout
{
public:
	DescriptorSetLayout(const Device* device,
		const std::vector<Shader*> shaders,
		const char* name);
	~DescriptorSetLayout();

public:
	inline const StateHandle GetState() const { return m_State; }

private:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
