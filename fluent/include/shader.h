#pragma once

FLUENT_BEGIN_NAMESPACE

struct ShaderInfo
{
	ShaderType Type;
	const char* CodeEntry;
};

class Shader
{
public:
	Shader(const Device* device,
		size_t byteCodeSize,
		const char* byteCode,
		const ShaderInfo& info,
		const char* name);
	~Shader();

public:
	uint32_t GetHash() const;

public:
	inline const ShaderInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const ShaderInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
