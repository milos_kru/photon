#pragma once

FLUENT_BEGIN_NAMESPACE

struct VertexBindingInfo
{
	uint32_t Binding;
	uint32_t Stride;
	VertexInputRate InputRate;
};

class VertexBinding
{
public:
	VertexBinding(const std::vector<VertexElement*> elements,
		const VertexBindingInfo& descriptor);

public:
	inline const VertexBindingInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const VertexBindingInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
