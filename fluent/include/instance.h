#pragma once

FLUENT_BEGIN_NAMESPACE

struct InstanceInfo
{
	const char* ApplicationName;
	const char* EngineName;
	bool EnableValidations;
	bool EnableRenderDoc;
};

class Instance
{
public:
	Instance(const InstanceInfo& info);
	~Instance();

public:
	inline const InstanceInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const InstanceInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
