#pragma once

FLUENT_BEGIN_NAMESPACE

class Semaphore
{
public:
	Semaphore(const Device* device,
		const char* name);
	~Semaphore();

public:
	inline const StateHandle GetState() const { return m_State; }

private:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
