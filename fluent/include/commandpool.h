#pragma once

FLUENT_BEGIN_NAMESPACE

class CommandPool
{
public:
	CommandPool(const Device* device,
		const Queue* queue,
		const char* name);
	~CommandPool();

public:
	inline const StateHandle GetState() const { return m_State; }

private:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
