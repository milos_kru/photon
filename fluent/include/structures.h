#pragma once

FLUENT_BEGIN_NAMESPACE

struct Color
{
	float value[4];
};

namespace DefaultColor
{
	static const Color White = { 1.0f, 1.0f, 1.0f, 1.0f };
	static const Color Black = { 0.0f, 0.0f, 0.0f, 1.0f };
	static const Color Gray = { 0.5f, 0.5f, 0.5f, 1.0f };
	static const Color Red = { 1.0f, 0.0f, 0.0f, 1.0f };
	static const Color Green = { 0.0f, 1.0f, 0.0f, 1.0f };
	static const Color Blue = { 0.0f, 0.0f, 1.0f, 1.0f };
	static const Color Cyan = { 0.0f, 1.0f, 1.0f, 1.0f };
	static const Color Magenta = { 1.0f, 0.0f, 1.0f, 1.0f };
	static const Color Yellow = { 1.0f, 1.0f, 0.0f, 1.0f };
}

struct Offset2D
{
	int32_t X;
	int32_t Y;
};

struct Offset3D
{
	int32_t X;
	int32_t Y;
	int32_t Z;
};

struct Extent2D
{
	uint32_t Width;
	uint32_t Height;
};

struct Extent3D
{
	uint32_t Width;
	uint32_t Height;
	uint32_t Depth;
};

struct Rect
{
	Offset2D Offset;
	Extent2D Extent;
};

struct Viewport
{
	float X;
	float Y;
	float Width;
	float Height;
	float MinDepth;
	float MaxDepth;
};

struct BufferRegion
{
	uint32_t Offset;
	uint32_t Size;
};

struct ImageSubresourceRange
{
	uint32_t MipSlice;
	uint32_t MipSize;
	uint32_t ArraySlice;
	uint32_t ArraySize;
};

struct ImageRegion
{
	ImageSubresourceRange Subresource;
	Offset3D Offset;
	Extent3D Extent;
};

struct DepthStencil
{
	float Depth;
	uint8_t Stencil;
};

union ClearValue
{
	Color Color;
	DepthStencil DepthStencil;
};

FLUENT_END_NAMESPACE
