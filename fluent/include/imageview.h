#pragma once

FLUENT_BEGIN_NAMESPACE

struct ImageViewInfo
{
	ImageResource* Resource;
	ImageViewFlags Flags;
	Format Format;
	ClearValue ClearValue;
	ImageSubresourceRange Subresource;
};

class ImageView
{
public:
	ImageView(const Device* device,
		const ImageViewInfo& info,
		const char* name);
	~ImageView();

public:
	inline const ImageViewInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const ImageViewInfo m_Info;
	const StateHandle m_State;
	const ImageResource* m_Image;
};

FLUENT_END_NAMESPACE
