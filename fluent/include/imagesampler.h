#pragma once

FLUENT_BEGIN_NAMESPACE

struct ImageSamplerInfo
{
	AddressMode AddressModeU;
	AddressMode AddressModeV;
	AddressMode AddressModeW;
	FilterType MagFilterType;
	FilterType MinFilterType;
	FilterType MipFilterType;
	CompareOperation CompareOperation;
	uint8_t MaxLevelOfAnisotropy;
	Color BorderColor;
	float MipLODBias;
	float MinLOD;
	float MaxLOD;
};

class ImageSampler
{
public:
	ImageSampler(const Device* device,
		const ImageSamplerInfo& info,
		const char* name);
	~ImageSampler();

public:
	inline const ImageSamplerInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	const ImageSamplerInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
