#pragma once

FLUENT_BEGIN_NAMESPACE

class CommandBuffer
{
protected:
	CommandBuffer(const Device* device,
		const CommandPool* commandPool,
		const StateHandle stateHandle,
		const char* name);
	virtual ~CommandBuffer() = 0;

public:
	void Begin();
	void End();

	void TransitionResource(const BufferResource* buffer,
		const ResourceState stateBefore, const ResourceState stateAfter,
		const uint32_t offset = 0);
	void TransitionResource(const ImageResource* image,
		const ResourceState stateBefore, const ResourceState stateAfter);
	void TransitionResource(const ImageView* imageView,
		const ResourceState stateBefore, const ResourceState stateAfter);

	void BeginDebugMarker(const char* name, const Color color = DefaultColor::White);
	void EndDebugMarker();

protected:
	void CommitBarriers();

public:
	inline const StateHandle GetState() const { return m_State; }

protected:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
