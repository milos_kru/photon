#pragma once

FLUENT_BEGIN_NAMESPACE

class RenderPass;
class DescriptorSetLayout;

class GraphicsPipeline
{
public:
	GraphicsPipeline(const Device* device,
		const RenderPass* renderPass,
		const DescriptorSetLayout* descriptorSetLayout,
		const GraphicsPipelineDescription* descriptor,
		const char* name);
	~GraphicsPipeline();

public:
	inline const StateHandle GetState() const { return m_State; }

private:
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
