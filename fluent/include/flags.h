#pragma once

FLUENT_BEGIN_NAMESPACE

#define FLUENT_FLAG_TYPE_SETUP(flagType) \
    inline flagType operator&(flagType a, flagType b) \
    { \
        return static_cast<flagType>(static_cast<std::underlying_type<flagType>::type>(a) & \
			static_cast<std::underlying_type<flagType>::type>(b) ); \
    } \
	\
    inline flagType operator|(flagType a, flagType b) \
    { \
        return static_cast<flagType>( static_cast<std::underlying_type<flagType>::type>(a) | \
			static_cast<std::underlying_type<flagType>::type>(b)); \
    } \
	\
    inline bool IsFlagSet(flagType x) \
    { \
        return (static_cast<uint32_t>(x) != 0); \
    }

enum class ImageFlags : uint8_t
{
	None = 0x0 << 0,
	CubeMap = 0x1 << 0,
	LinearTiling = 0x1 << 1,
};
FLUENT_FLAG_TYPE_SETUP(ImageFlags)

enum class BufferUsageFlags : uint8_t
{
	None = 0x0 << 0,
	TransferSrc = 0x1 << 0,
	TransferDst = 0x1 << 1,
	VertexBuffer = 0x1 << 4,
	IndexBuffer = 0x1 << 5,
	UniformBuffer = 0x1 << 6,
	IndirectBuffer = 0x1 << 7,
};
FLUENT_FLAG_TYPE_SETUP(BufferUsageFlags)

enum class ImageUsageFlags : uint8_t
{
	None = 0x0 << 0,
	TransferSrc = 0x1 << 0,
	TransferDst = 0x1 << 1,
	Resource = 0x1 << 2,
	Storage = 0x1 << 3,
	RenderTarget = 0x1 << 4,
	DepthStencil = 0x1 << 5,
};
FLUENT_FLAG_TYPE_SETUP(ImageUsageFlags)

enum class ImageViewFlags : uint8_t
{
	None = 0x0 << 0,
	Color = 0x1 << 0,
	ReadDepth = 0x1 << 1,
	ReadStencil = 0x1 << 2,
};
FLUENT_FLAG_TYPE_SETUP(ImageViewFlags)

enum class ClearDepthStencilFlags : uint8_t
{
	None = 0x0 << 0,
	ClearDepth = 0x1 << 0,
	ClearStencil = 0x1 << 1,
	ClearDepthStencil = ClearDepth | ClearStencil,
};
FLUENT_FLAG_TYPE_SETUP(ClearDepthStencilFlags)

enum class ColorWriteMaskFlags : uint8_t
{
	None = 0x0 << 0,
	R = 0x1 << 0,
	G = 0x1 << 1,
	B = 0x1 << 2,
	A = 0x1 << 3,
	RG = R | G,
	RGB = RG | B,
	RGBA = RGB | A,
};
FLUENT_FLAG_TYPE_SETUP(ColorWriteMaskFlags)

#undef FLUENT_FLAG_TYPE_SETUP

FLUENT_END_NAMESPACE
