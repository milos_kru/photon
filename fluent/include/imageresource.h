#pragma once

FLUENT_BEGIN_NAMESPACE

struct ImageResourceInfo
{
	ImageFlags Flags;
	ImageUsageFlags UsageFlags;
	MemoryAccess MemoryAccess;
	Format Format;
	Extent3D Extent;
	uint16_t ArraySize;
	uint16_t MipCount;
	MultisampleType MultisampleType;
};

class ImageResource
{
public:
	ImageResource(const Device* device,
		const ImageResourceInfo& info,
		const char* name);
	~ImageResource();

private:
	friend class Swapchain;
	ImageResource(const Device* device, const Swapchain* swapchain,
		const uint32_t backBufferIndex);

public:
	inline ImageResourceInfo GetInfo() const { return m_Info; }
	inline const StateHandle GetState() const { return m_State; }

private:
	ImageResourceInfo m_Info;
	const StateHandle m_State;
};

FLUENT_END_NAMESPACE
