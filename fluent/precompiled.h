#pragma once

#include "include/fluent.h"

#ifdef FLUENT_DEBUG
	#include <iostream>
#endif // FLUENT_DEBUG

#ifdef FLUENT_DEBUG
	#define FLUENT_ASSERT(condition) if(!(condition)) __debugbreak()

	#define FLUENT_ASSERT_WITH_MESSAGE(condition, message) \
		if(!(condition)) \
		{ \
			std::cerr << message << std::endl; \
			__debugbreak(); \
		}

	#define FLUENT_ASSERT_ALWAYS(message) FLUENT_ASSERT_WITH_MESSAGE(0, message)
#else
	#define FLUENT_ASSERT(condition)
	#define FLUENT_ASSERT_WITH_MESSAGE(condition, message)
	#define FLUENT_ASSERT_ALWAYS(message)
#endif // FLUENT_DEBUG

#ifdef FLUENT_VULKAN_API
	#define FLUENT_API_CALL(call) \
		{ \
			VkResult result = call; \
			FLUENT_ASSERT(result == VK_SUCCESS); \
		}
#else
	#error Unsupported Api
#endif // FLUENT_API

#define FLUENT_OPTIMIZE_OFF __pragma(optimize("", off))
#define FLUENT_OPTIMIZE_ON __pragma(optimize("", on))

#define FLUENT_DEFINE_GET_STATE(type) \
__forceinline std::shared_ptr<type##State> ConvertState(const type* object) \
{ \
	return std::static_pointer_cast<type##State>(object->GetState()); \
}

#define this_state ConvertState(this)
#define get_state(object) ConvertState(object)
#define new_state(type) std::make_shared<type##State>()

#ifdef FLUENT_VULKAN_API
	#include "vulkan/include/includes_vulkan.h"
#else
	#error Unsupported Api
#endif // FLUENT_API
