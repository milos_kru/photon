#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

Semaphore::Semaphore(const Device* device, const char* name)
	: m_State(new_state(Semaphore))
{
	this_state->m_Device = get_state(device)->m_Device;

	VkSemaphoreCreateInfo semaphoreCreateInfo = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };

	FLUENT_API_CALL(vkCreateSemaphore(get_state(device)->m_Device, &semaphoreCreateInfo, nullptr, &this_state->m_Semaphore));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Semaphore), VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT, name);
#endif // FLUENT_DEBUG
}

Semaphore::~Semaphore()
{
	vkDestroySemaphore(this_state->m_Device, this_state->m_Semaphore, nullptr);
}

FLUENT_END_NAMESPACE
