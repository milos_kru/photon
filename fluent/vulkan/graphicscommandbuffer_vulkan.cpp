#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

GraphicsCommandBuffer::GraphicsCommandBuffer(const Device* device, const CommandPool* commandPool, const char* name)
	: CommandBuffer(device, commandPool, new_state(GraphicsCommandBuffer), name)
{
	this_state->m_PipelineLayout = VK_NULL_HANDLE;
}

void GraphicsCommandBuffer::SetVertexBuffer(const BufferResource* vertexBuffer, const uint32_t vertexBufferOffset)
{
	VkBuffer vertexBuffers[] = { get_state(vertexBuffer)->m_Buffer };
	VkDeviceSize offsets[] = { vertexBufferOffset };
	vkCmdBindVertexBuffers(this_state->m_CommandBuffer, 0, 1, vertexBuffers, offsets);
}

void GraphicsCommandBuffer::SetIndexBuffer(const BufferResource* indexBuffer, const uint32_t indexBufferOffset)
{
	vkCmdBindIndexBuffer(this_state->m_CommandBuffer, get_state(indexBuffer)->m_Buffer, indexBufferOffset, VK_INDEX_TYPE_UINT32);
}

void GraphicsCommandBuffer::SetViewport(const Viewport viewport)
{
	VkViewport vkViewport;
	vkViewport.x = viewport.X;
	vkViewport.y = viewport.Y;
	vkViewport.width = viewport.Width;
	vkViewport.height = viewport.Height;
	vkViewport.minDepth = viewport.MinDepth;
	vkViewport.maxDepth = viewport.MaxDepth;

	vkCmdSetViewport(this_state->m_CommandBuffer, 0, 1, &vkViewport);
}

void GraphicsCommandBuffer::SetScissorRect(const Rect scissorRect)
{
	VkRect2D vkScissorRect;
	vkScissorRect.offset.x = scissorRect.Offset.X;
	vkScissorRect.offset.y = scissorRect.Offset.X;
	vkScissorRect.extent.width = scissorRect.Extent.Width;
	vkScissorRect.extent.height = scissorRect.Extent.Height;

	vkCmdSetScissor(this_state->m_CommandBuffer, 0, 1, &vkScissorRect);
}

void GraphicsCommandBuffer::SetStencilReference(const uint32_t stencilRef)
{
	vkCmdSetStencilReference(this_state->m_CommandBuffer, VK_STENCIL_FRONT_AND_BACK, stencilRef);
}

void GraphicsCommandBuffer::SetGraphicsPipeline(const GraphicsPipeline* pipeline)
{
	vkCmdBindPipeline(this_state->m_CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, get_state(pipeline)->m_Pipeline);
	this_state->m_PipelineLayout = get_state(pipeline)->m_PipelineLayout;
}

void GraphicsCommandBuffer::BeginRenderPass(const RenderPass* renderPass, const Framebuffer* framebuffer)
{
	// TODO: BeginRenderPass should be called on Fluent level with dirty flag in graphics command buffer, checking if the render pass has changed.

	VkRenderPassBeginInfo renderPassInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO };
	renderPassInfo.renderPass = get_state(renderPass)->m_RenderPass;
	renderPassInfo.framebuffer = get_state(framebuffer)->m_Framebuffer;
	renderPassInfo.renderArea.offset.x = 0;
	renderPassInfo.renderArea.offset.y = 0;
	renderPassInfo.renderArea.extent.width = framebuffer->GetInfo().Width;
	renderPassInfo.renderArea.extent.height = framebuffer->GetInfo().Height;
	renderPassInfo.clearValueCount = get_state(renderPass)->AttachmentCount; // TODO: Pass clear in arguments here? And you can stage them in device state.
	renderPassInfo.pClearValues = get_state(renderPass)->m_ClearValues; // TODO: Pass clear in arguments here?

	vkCmdBeginRenderPass(this_state->m_CommandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
}

void GraphicsCommandBuffer::EndRenderPass(const RenderPass* renderPass)
{
	vkCmdEndRenderPass(this_state->m_CommandBuffer);
}

void GraphicsCommandBuffer::ClearRenderTarget(const ImageView* renderTargetView, const Color clearValue)
{
	TransitionResource(renderTargetView->GetInfo().Resource, ResourceState::RenderTarget, ResourceState::TransferDst);
	CommitBarriers();

	VkClearColorValue clearColorValue;
	clearColorValue.float32[0] = clearValue.value[0];
	clearColorValue.float32[1] = clearValue.value[1];
	clearColorValue.float32[2] = clearValue.value[2];
	clearColorValue.float32[3] = clearValue.value[3];

	ImageResource* image = renderTargetView->GetInfo().Resource;

	VkImageSubresourceRange subresourceRange =
	{
		GetVkImageAspectFlagsFrom(get_state(image)->m_Format),
		renderTargetView->GetInfo().Subresource.MipSlice, renderTargetView->GetInfo().Subresource.MipSize,
		renderTargetView->GetInfo().Subresource.ArraySlice, renderTargetView->GetInfo().Subresource.MipSize
	};

	vkCmdClearColorImage(this_state->m_CommandBuffer, get_state(image)->m_Image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearColorValue, 1, &subresourceRange);

	TransitionResource(renderTargetView->GetInfo().Resource, ResourceState::TransferDst, ResourceState::RenderTarget);
	CommitBarriers();
}

void GraphicsCommandBuffer::ClearDepthStencil(const ImageView* depthStencil, const ClearDepthStencilFlags flags, const DepthStencil clearValue)
{
	TransitionResource(depthStencil->GetInfo().Resource, ResourceState::DepthStencilWrite, ResourceState::TransferDst);
	CommitBarriers();

	ImageResource* image = depthStencil->GetInfo().Resource;

	VkImageSubresourceRange subresourceRange =
	{
		GetVkImageAspectFlagsFrom(get_state(image)->m_Format),
		depthStencil->GetInfo().Subresource.MipSlice, depthStencil->GetInfo().Subresource.MipSize,
		depthStencil->GetInfo().Subresource.ArraySlice, depthStencil->GetInfo().Subresource.MipSize
	};

	if (!IsFlagSet(flags & ClearDepthStencilFlags::ClearDepth))
	{
		subresourceRange.aspectMask = subresourceRange.aspectMask & (~VK_IMAGE_ASPECT_DEPTH_BIT);
	}
	
	if (!IsFlagSet(flags & ClearDepthStencilFlags::ClearStencil))
	{
		subresourceRange.aspectMask = subresourceRange.aspectMask & (~VK_IMAGE_ASPECT_STENCIL_BIT);
	}

	VkClearDepthStencilValue depthStencilValue;
	depthStencilValue.depth = clearValue.Depth;
	depthStencilValue.stencil = clearValue.Stencil;
	vkCmdClearDepthStencilImage(this_state->m_CommandBuffer, get_state(image)->m_Image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &depthStencilValue, 1, &subresourceRange);

	TransitionResource(depthStencil->GetInfo().Resource, ResourceState::TransferDst, ResourceState::DepthStencilWrite);
	CommitBarriers();
}

void GraphicsCommandBuffer::Draw(const uint32_t numVertices, const uint32_t numInstances, const uint32_t startVertex, const uint32_t startInstance)
{
	CommitBarriers();

	vkCmdDraw(this_state->m_CommandBuffer, numVertices, numInstances, startVertex, startInstance);
}

void GraphicsCommandBuffer::DrawIndexed(const uint32_t numIndices, const uint32_t numInstances, const uint32_t startIndex, const uint32_t startVertex, const uint32_t startInstance)
{
	CommitBarriers();

	vkCmdDrawIndexed(this_state->m_CommandBuffer, numIndices, numInstances == 0 ? 1 : numInstances, startIndex, startVertex, startInstance);
}

void GraphicsCommandBuffer::DrawInstancedIndirect(const BufferResource* indirectBuffer, const uint32_t indirectBufferOffset, const uint32_t drawCount)
{
	CommitBarriers();

	vkCmdDrawIndirect(this_state->m_CommandBuffer, get_state(indirectBuffer)->m_Buffer, indirectBufferOffset, drawCount, sizeof(VkDrawIndirectCommand));
}

void GraphicsCommandBuffer::DrawIndexedInstancedIndirect(const BufferResource* indirectBuffer, const uint32_t indirectBufferOffset, const uint32_t drawCount)
{
	CommitBarriers();

	vkCmdDrawIndexedIndirect(this_state->m_CommandBuffer, get_state(indirectBuffer)->m_Buffer, indirectBufferOffset, drawCount, sizeof(VkDrawIndexedIndirectCommand));
}

void GraphicsCommandBuffer::SetDescriptorSet(const uint32_t slotIndex, const DescriptorSet* descriptorSet)
{
	FLUENT_ASSERT(this_state->m_PipelineLayout != VK_NULL_HANDLE);

	vkCmdBindDescriptorSets(this_state->m_CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this_state->m_PipelineLayout, slotIndex, 1, &get_state(descriptorSet)->m_DescriptorSet, 0, nullptr);
}

FLUENT_END_NAMESPACE
