#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

bool IsFormatSupported(const VkPhysicalDevice physicalDevice, const VkSurfaceKHR presentSurface,
	const VkFormat format, const VkColorSpaceKHR colorSpace)
{
	std::uint32_t availableFormatsCount = 0;
	FLUENT_API_CALL(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, presentSurface,
		&availableFormatsCount, nullptr));
	FLUENT_ASSERT(availableFormatsCount != 0);

	std::vector<VkSurfaceFormatKHR> availableFormats(availableFormatsCount);
	FLUENT_API_CALL(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, presentSurface,
		&availableFormatsCount, availableFormats.data()));

	for (const VkSurfaceFormatKHR& availableFormat : availableFormats)
	{
		if ((availableFormat.format == format) && (availableFormat.colorSpace == colorSpace))
		{
			return true;
		}
	}

	return false;
}

VkPresentModeKHR GetSurfacePresentMode(const VkPhysicalDevice physicalDevice,
	const VkSurfaceKHR presentSurface, const bool vSyncEnabled)
{
	uint32_t presentModeCount;
	FLUENT_API_CALL(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, presentSurface,
		&presentModeCount, nullptr));
	FLUENT_ASSERT(presentModeCount > 0);

	std::vector<VkPresentModeKHR> presentModes(presentModeCount);
	FLUENT_API_CALL(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, presentSurface,
		&presentModeCount, presentModes.data()));

	if (vSyncEnabled)
	{
		return VK_PRESENT_MODE_FIFO_KHR;
	}

	for (const VkPresentModeKHR& presentMode : presentModes)
	{
		if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
		{
			return VK_PRESENT_MODE_MAILBOX_KHR;
		}
	}

	for (const VkPresentModeKHR& presentMode : presentModes)
	{
		if (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
		{
			return VK_PRESENT_MODE_IMMEDIATE_KHR;
		}
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

std::optional<VkCompositeAlphaFlagBitsKHR> GetCompositeAlpha(const VkSurfaceCapabilitiesKHR surfaceCapabilities,
	const VkSurfaceKHR presentSurface)
{
	const std::vector<VkCompositeAlphaFlagBitsKHR> compositeAlphaFlags =
	{
		VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
		VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
		VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
	};

	for (const VkCompositeAlphaFlagBitsKHR compositeAlpha : compositeAlphaFlags)
	{
		if (surfaceCapabilities.supportedCompositeAlpha & compositeAlpha)
		{
			return std::make_optional(compositeAlpha);
		};
	}

	return std::nullopt;
}

void SetupSwapchain(const Device* device, std::shared_ptr<SwapchainState> swapchainState,
	std::vector<ImageResource*>& backBuffers, std::vector<Semaphore*>& imageAcquiredSemaphores,
	const SwapchainInfo& info)
{
	VkSwapchainCreateInfoKHR swapchainCreateInfo = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };

	VkSurfaceCapabilitiesKHR surfaceCapabilities{};
	FLUENT_API_CALL(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(swapchainState->m_PhysicalDevice,
		swapchainState->m_Surface, &surfaceCapabilities));

	uint32_t swapchainImageCount = info.BufferCount > surfaceCapabilities.minImageCount ?
		info.BufferCount : surfaceCapabilities.minImageCount;
	swapchainCreateInfo.minImageCount = swapchainImageCount;

	VkFormat vkFormat = GetVkFormatFrom(info.Format);

	VkColorSpaceKHR colorSpace = GetVkColorSpaceFrom(info.ColorSpace);
	FLUENT_ASSERT(IsFormatSupported(swapchainState->m_PhysicalDevice,
		swapchainState->m_Surface, vkFormat, colorSpace));
	swapchainCreateInfo.imageFormat = vkFormat;
	swapchainCreateInfo.imageColorSpace = colorSpace;

	if (surfaceCapabilities.currentExtent.width == static_cast<uint32_t>(-1))
	{
		swapchainState->m_Extent.width = info.Extent.Width;
		swapchainState->m_Extent.height = info.Extent.Height;
	}
	else
	{
		swapchainState->m_Extent = surfaceCapabilities.currentExtent;
	}

	swapchainCreateInfo.imageExtent = swapchainState->m_Extent;

	if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
	{
		swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	}
	else
	{
		swapchainCreateInfo.preTransform = surfaceCapabilities.currentTransform;
	}

	swapchainCreateInfo.surface = swapchainState->m_Surface;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchainCreateInfo.clipped = VK_TRUE;
	swapchainCreateInfo.presentMode = GetSurfacePresentMode(swapchainState->m_PhysicalDevice,
		swapchainState->m_Surface, info.EnableVSync);
	swapchainCreateInfo.oldSwapchain = swapchainState->m_Swapchain;

	std::optional<VkCompositeAlphaFlagBitsKHR> alphaComposite =
		GetCompositeAlpha(surfaceCapabilities, swapchainState->m_Surface);
	FLUENT_ASSERT(alphaComposite.has_value());
	swapchainCreateInfo.compositeAlpha = alphaComposite.value();

	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(swapchainState->m_PhysicalDevice,
		swapchainCreateInfo.imageFormat, &formatProperties);
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if (formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_BLIT_DST_BIT)
	{
		swapchainCreateInfo.imageUsage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	}

	if (surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_SRC_BIT)
	{
		swapchainCreateInfo.imageUsage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	}

	FLUENT_API_CALL(vkCreateSwapchainKHR(swapchainState->m_Device,
		&swapchainCreateInfo, nullptr, &swapchainState->m_Swapchain));

	if (swapchainCreateInfo.oldSwapchain != VK_NULL_HANDLE)
	{
		vkDestroySwapchainKHR(swapchainState->m_Device,
			swapchainCreateInfo.oldSwapchain, nullptr);
	}

	FLUENT_API_CALL(vkGetSwapchainImagesKHR(swapchainState->m_Device,
		swapchainState->m_Swapchain, &swapchainImageCount, nullptr));
	FLUENT_ASSERT(swapchainImageCount == swapchainCreateInfo.minImageCount);

	swapchainState->m_BackBufferImages.resize(swapchainImageCount);
	FLUENT_API_CALL(vkGetSwapchainImagesKHR(swapchainState->m_Device,
		swapchainState->m_Swapchain, &swapchainImageCount, swapchainState->m_BackBufferImages.data()));

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		Semaphore* semaphore = new Semaphore(device, "Swapchain Image Acquired");
		imageAcquiredSemaphores.push_back(semaphore);
	}
}

Swapchain::Swapchain(const Device* device, const PresentSurface* presentSurface,
	const Queue* presentQueue, const SwapchainInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(Swapchain))
	, m_CurrentBackBufferIndex(0)
	, m_CurrentSemaphoreIndex(0)
{
	this_state->m_Device = get_state(device)->m_Device;
	this_state->m_PhysicalDevice = get_state(device)->m_PhysicalDevice;
	this_state->m_Swapchain = VK_NULL_HANDLE;
	this_state->m_Surface = get_state(presentSurface)->m_Surface;
	this_state->m_Queue = get_state(presentQueue)->m_Queue;

	Recreate(device);

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Swapchain), VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT, name);
#endif // FLUENT_DEBUG
}

void DestroyBackBuffers(std::vector<ImageResource*> backBuffers)
{
	for (uint32_t i = 0; i < backBuffers.size(); ++i)
	{
		delete(backBuffers[i]);
	}

	backBuffers.clear();
}

void DestroySemaphores(std::vector<Semaphore*> imageAcquiredSemaphores)
{
	for (uint32_t i = 0; i < imageAcquiredSemaphores.size(); ++i)
	{
		delete(imageAcquiredSemaphores[i]);
	}

	imageAcquiredSemaphores.clear();
}

Swapchain::~Swapchain()
{
	DestroyBackBuffers(m_BackBuffers);
	DestroySemaphores(m_ImageAcquiredSemaphores);

	vkDestroySwapchainKHR(this_state->m_Device, this_state->m_Swapchain, nullptr);
}

void Swapchain::Recreate(const Device* device, const Extent2D extent)
{
	if (extent.Width != 0 && extent.Height != 0)
	{
		m_Info.Extent = extent;
	}

	DestroyBackBuffers(m_BackBuffers);
	DestroySemaphores(m_ImageAcquiredSemaphores);

	SetupSwapchain(device, this_state, m_BackBuffers, m_ImageAcquiredSemaphores, m_Info);

	FLUENT_ASSERT(m_BackBuffers.size() == 0);
	for (uint32_t i = 0; i < this_state->m_BackBufferImages.size(); ++i)
	{
		m_BackBuffers.push_back(new ImageResource(device, this, i));
	}
}

std::optional<uint32_t> Swapchain::AcquireNextImage()
{
	m_CurrentSemaphoreIndex = (m_CurrentSemaphoreIndex + 1) % m_BackBuffers.size();
	Semaphore* imageAcquiredSemaphore = m_ImageAcquiredSemaphores[m_CurrentSemaphoreIndex];

    VkResult result = vkAcquireNextImageKHR(this_state->m_Device, this_state->m_Swapchain, ~0ULL,
		get_state(imageAcquiredSemaphore)->m_Semaphore, nullptr, &m_CurrentBackBufferIndex);

	FLUENT_ASSERT(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR || result == VK_ERROR_OUT_OF_DATE_KHR);
	return result == VK_SUCCESS ? std::make_optional(m_CurrentBackBufferIndex) : std::nullopt;
}

PresentResult Swapchain::Present(const Semaphore* waitSemaphore) const
{
	VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &this_state->m_Swapchain;
	presentInfo.pImageIndices = &m_CurrentBackBufferIndex;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &get_state(waitSemaphore)->m_Semaphore;

	VkResult result = vkQueuePresentKHR(this_state->m_Queue, &presentInfo);
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
	{
		return PresentResult::Invalid;
	}

	return PresentResult::Valid;
}

FLUENT_END_NAMESPACE
