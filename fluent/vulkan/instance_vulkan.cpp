#include "precompiled.h"

#include <iostream>
#include <sstream>

FLUENT_BEGIN_NAMESPACE

#ifdef FLUENT_DEBUG
const char* ConvertDebugMessageSeverityToString(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity)
{
	if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
	{
		return "VERBOSE";
	}
	else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
	{
		return "INFO";
	}
	else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
	{
		return "WARNING";
	}
	else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
	{
		return "ERROR";
	}

	return "UNDEFINED";
}

const char* ConvertDebugMessageTypeToString(VkDebugUtilsMessageTypeFlagsEXT messageType)
{
	if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)
	{
		return "GENERAL";
	}
	else if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
	{
		return "PERFORMANCE";
	}
	else if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
	{
		return "VALIDATION";
	}

	return "UNDEFINED";
}

VKAPI_ATTR VkBool32 VKAPI_CALL DebugMessageCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
	void* userData)
{
	std::string messagePrefix = std::string(ConvertDebugMessageSeverityToString(messageSeverity)) +
		" (" + std::string(ConvertDebugMessageTypeToString(messageType)) + ")";
	std::string messageId = "[ID: " + std::to_string(callbackData->messageIdNumber) +
		", NAME: " + callbackData->pMessageIdName + "]";
	std::string message = messagePrefix + " : " + messageId + " : " + callbackData->pMessage;

	if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
	{
		std::cerr << message << std::endl;
	}
	else
	{
		std::cout << message << std::endl;
	}

	return VK_FALSE;
}
#endif // FLUENT_DEBUG

bool IsLayerAvailable(const std::vector<VkLayerProperties> availableLayers,
	const char* layerName)
{
	for (const VkLayerProperties& availableLayer : availableLayers)
	{
		if (strcmp(availableLayer.layerName, layerName) == 0)
		{
			return true;
		}
	}

	return false;
}

bool CheckLayersSupport(const std::vector<VkLayerProperties> availableLayers,
	const std::vector<const char*> layerNames)
{
	for (const char* layerName : layerNames)
	{
		if (!IsLayerAvailable(availableLayers, layerName))
		{
			return false;
		}
	}

	return true;
}

std::vector<const char*> GetRequiredLayerNames(const bool enableValidations, const bool enableRenderDoc)
{
	uint32_t layerCount;
	FLUENT_API_CALL(vkEnumerateInstanceLayerProperties(&layerCount, nullptr));

	std::vector<VkLayerProperties> availableLayers(layerCount);
	FLUENT_API_CALL(vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data()));

	std::vector<const char*> requiredLayerNames;

#ifdef FLUENT_DEBUG
	if (enableValidations)
	{
		requiredLayerNames.push_back("VK_LAYER_KHRONOS_validation");
	}

	if (enableRenderDoc)
	{
		requiredLayerNames.push_back("VK_LAYER_RENDERDOC_Capture");
	}
#endif // FLUENT_DEBUG

	FLUENT_ASSERT(CheckLayersSupport(availableLayers, requiredLayerNames));

	return requiredLayerNames;
}

bool IsInstanceExtensionAvailable(const std::vector<VkExtensionProperties> availableExtensions,
	const char* extensionName)
{
	for (const VkExtensionProperties& availableExtension : availableExtensions)
	{
		if (strcmp(availableExtension.extensionName, extensionName) == 0)
		{
			return true;
		}
	}

	return false;
}

bool CheckInstanceExtensionsSupport(const std::vector<VkExtensionProperties> availableExtensions,
	const std::vector<const char*> extensionNames)
{
	for (const char* extensionName : extensionNames)
	{
		if (!IsInstanceExtensionAvailable(availableExtensions, extensionName))
		{
			return false;
		}
	}

	return true;
}

std::vector<const char*> GetRequiredInstanceExtensionNames(const bool enableValidations)
{
	uint32_t extensionCount;
	FLUENT_API_CALL(vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr));

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	FLUENT_API_CALL(vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableExtensions.data()));

	std::vector<const char*> requiredExtensionNames;

#ifdef FLUENT_DEBUG
	if (enableValidations)
	{
		requiredExtensionNames.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		requiredExtensionNames.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}
#endif // FLUENT_DEBUG

	requiredExtensionNames.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
	requiredExtensionNames.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
	requiredExtensionNames.push_back(VK_EXT_SWAPCHAIN_COLOR_SPACE_EXTENSION_NAME);
	requiredExtensionNames.push_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);

	FLUENT_ASSERT(CheckInstanceExtensionsSupport(availableExtensions, requiredExtensionNames));

	return requiredExtensionNames;
}

Instance::Instance(const InstanceInfo& info)
	: m_Info(info)
	, m_State(new_state(Instance))
{
	VkApplicationInfo applicationCreateInfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO };
	applicationCreateInfo.pApplicationName = m_Info.ApplicationName;
	applicationCreateInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationCreateInfo.pEngineName = m_Info.EngineName;
	applicationCreateInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationCreateInfo.apiVersion = FLUENT_VULKAN_API_VERSION;

	std::vector<const char*> requiredLayerNames = GetRequiredLayerNames(m_Info.EnableValidations, m_Info.EnableRenderDoc);
	std::vector<const char*> requiredExtensionNames = GetRequiredInstanceExtensionNames(m_Info.EnableValidations);

	VkInstanceCreateInfo instanceCreateInfo = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
	instanceCreateInfo.pApplicationInfo = &applicationCreateInfo;
	instanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(requiredLayerNames.size());
	instanceCreateInfo.ppEnabledLayerNames = requiredLayerNames.data();
	instanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensionNames.size());
	instanceCreateInfo.ppEnabledExtensionNames = requiredExtensionNames.data();
	FLUENT_API_CALL(vkCreateInstance(&instanceCreateInfo, nullptr, &this_state->m_Instance));

#ifdef FLUENT_DEBUG
	if (!m_Info.EnableValidations)
	{
		this_state->m_DebugMessenger = VK_NULL_HANDLE;
		return;
	}

	PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
		vkGetInstanceProcAddr(this_state->m_Instance, "vkCreateDebugUtilsMessengerEXT"));
	FLUENT_ASSERT(vkCreateDebugUtilsMessengerEXT);

	VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo = { VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT };
	debugMessengerCreateInfo.messageSeverity =
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	debugMessengerCreateInfo.messageType =
		VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	debugMessengerCreateInfo.pfnUserCallback = DebugMessageCallback;
	FLUENT_API_CALL(vkCreateDebugUtilsMessengerEXT(this_state->m_Instance, &debugMessengerCreateInfo, nullptr, &this_state->m_DebugMessenger));
#endif // FLUENT_DEBUG
}

Instance::~Instance()
{
#ifdef FLUENT_DEBUG
	if (this_state->m_DebugMessenger != VK_NULL_HANDLE)
	{
		PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
			vkGetInstanceProcAddr(this_state->m_Instance, "vkDestroyDebugUtilsMessengerEXT"));
		FLUENT_ASSERT(vkDestroyDebugUtilsMessengerEXT);

		vkDestroyDebugUtilsMessengerEXT(this_state->m_Instance, this_state->m_DebugMessenger, nullptr);
	}
#endif // FLUENT_DEBUG

	vkDestroyInstance(this_state->m_Instance, nullptr);
}

FLUENT_END_NAMESPACE
