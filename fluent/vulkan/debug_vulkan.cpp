#include "precompiled.h"

#ifdef FLUENT_DEBUG

FLUENT_BEGIN_NAMESPACE

#define FLUENT_DEBUG_NAME_MAX_SIZE 128

const char* ConvertDebugReportObjectTypeToString(const VkDebugReportObjectTypeEXT type)
{
	switch (type)
	{
	case VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT:
		return "Instance";
	case VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT:
		return "Physical Device";
	case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT:
		return "Device";
	case VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT:
		return "Queue";
	case VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT:
		return "Semaphore";
	case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT:
		return "Command Buffer";
	case VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT:
		return "Fence";
	case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT:
		return "Device Memory";
	case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT:
		return "Buffer";
	case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT:
		return "Image";
	case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT:
		return "Image";
	case VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT:
		return "Shader Module";
	case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT:
		return "Pipeline Layout";
	case VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT:
		return "Render Pass";
	case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT:
		return "Pipeline";
	case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT:
		return "Descriptor Set Layout";
	case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT:
		return "Sampler";
	case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT:
		return "Descriptor Pool";
	case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT:
		return "Descriptor Set";
	case VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT:
		return " Framebuffer";
	case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT:
		return "Command Pool";
	case VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT:
		return "Surface";
	case VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT:
		return "Swapchain";
	case VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT:
		return "Debug Report Callback";
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported DebugReportObjectType.");
		return nullptr;
	}
}

void SetDebugName(const Device* device, const uint64_t object, const VkDebugReportObjectTypeEXT type, const char* name)
{
	if (!device->GetInfo().EnableDebugMarkers)
	{
		return;
	}

	char fullName[FLUENT_DEBUG_NAME_MAX_SIZE];
	strcpy_s(fullName, name);
	strcat_s(fullName, " ");
	strcat_s(fullName, ConvertDebugReportObjectTypeToString(type));

	VkDebugMarkerObjectNameInfoEXT debugNameCreateInfo = { VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT };
	debugNameCreateInfo.objectType = type;
	debugNameCreateInfo.object = object;
	debugNameCreateInfo.pObjectName = fullName;
	get_state(device)->m_DebugMarkerSetObjectName(get_state(device)->m_Device, &debugNameCreateInfo);
}

void BeginDebugMarker(const Device* device, const VkCommandBuffer commandBuffer, const char* name, const Color color)
{
	if (!device->GetInfo().EnableDebugMarkers)
	{
		return;
	}

	VkDebugMarkerMarkerInfoEXT debugMarkerCreateInfo = { VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT };
	debugMarkerCreateInfo.pMarkerName = name;
	debugMarkerCreateInfo.color[0] = color.value[0];
	debugMarkerCreateInfo.color[1] = color.value[1];
	debugMarkerCreateInfo.color[2] = color.value[2];
	debugMarkerCreateInfo.color[3] = color.value[3];

	get_state(device)->m_CmdDebugMarkerBegin(commandBuffer, &debugMarkerCreateInfo);
}

void EndDebugMarker(const Device* device, const VkCommandBuffer commandBuffer)
{
	if (!device->GetInfo().EnableDebugMarkers)
	{
		return;
	}

	get_state(device)->m_CmdDebugMarkerEnd(commandBuffer);
}

FLUENT_END_NAMESPACE

#endif // FLUENT_DEBUG
