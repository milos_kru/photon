#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

VertexElement::VertexElement(const VertexElementInfo& info)
	: m_Info(info)
	, m_State(new_state(VertexElement))
{
	this_state->m_VertexAttributeDescription.binding = m_Info.Binding;
	this_state->m_VertexAttributeDescription.location = m_Info.Location;
	this_state->m_VertexAttributeDescription.format = GetVkFormatFrom(m_Info.Format);
	this_state->m_VertexAttributeDescription.offset = m_Info.Offset;
}

FLUENT_END_NAMESPACE
