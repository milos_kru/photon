#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

Descriptor::Descriptor(const DescriptorInfo& info)
	: m_Info(info)
	, m_State(new_state(Descriptor))
{
	switch (m_Info.Type)
	{
		case DescriptorType::CombinedSampler:
		{
			CombinedImageSampler* combinedImageSampler = m_Info.Resource.CombinedImageSampler;
			this_state->m_ResourceInfo.ImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			this_state->m_ResourceInfo.ImageInfo.imageView = get_state(combinedImageSampler->ImageView)->m_ImageView;
			this_state->m_ResourceInfo.ImageInfo.sampler = get_state(combinedImageSampler->ImageSampler)->m_Sampler;
			break;
		}
		case DescriptorType::UniformBuffer:
		{
			BufferResource* uniformBuffer = m_Info.Resource.UniformBuffer;
			this_state->m_ResourceInfo.BufferInfo.buffer = get_state(uniformBuffer)->m_Buffer;
			this_state->m_ResourceInfo.BufferInfo.offset = 0;
			this_state->m_ResourceInfo.BufferInfo.range = uniformBuffer->GetInfo().Size;
			break;
		}
		default:
			FLUENT_ASSERT_ALWAYS("Unsupported DescriptorType.");
			break;
	}
}

FLUENT_END_NAMESPACE
