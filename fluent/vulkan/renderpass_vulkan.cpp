#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

RenderPass::RenderPass(const Device* device, const std::vector<ImageView*> renderTargetViews,
	const std::optional<ImageView*> depthStencilView, const RenderPassInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(RenderPass))
{
	this_state->m_Device = get_state(device)->m_Device;

	uint32_t colorAttachmentCount = static_cast<uint32_t>(renderTargetViews.size());
	this_state->AttachmentCount = colorAttachmentCount;

	std::vector<VkAttachmentDescription> attachmentDescriptions;
	std::vector<VkAttachmentReference> colorAttachmentReferences;
	for (uint32_t i = 0; i < colorAttachmentCount; ++i)
	{
		ImageView* renderTargetView = renderTargetViews[i];

		ClearValue clearColor = renderTargetView->GetInfo().ClearValue;
		VkClearValue clearValue{};
		clearValue.color.float32[0] = clearColor.Color.value[0];
		clearValue.color.float32[1] = clearColor.Color.value[1];
		clearValue.color.float32[2] = clearColor.Color.value[2];
		clearValue.color.float32[3] = clearColor.Color.value[3];
		this_state->m_ClearValues[i] = clearValue;

		VkAttachmentDescription attachmentDescription{};
		attachmentDescription.format = get_state(renderTargetView)->m_Format;
		attachmentDescription.samples = GetVkSampleFlagsFrom(renderTargetView->GetInfo().Resource->GetInfo().MultisampleType);
		attachmentDescription.loadOp = m_Info.ClearRenderTargets ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
		attachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		attachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		attachmentDescription.initialLayout = GetVkImageLayoutFrom(m_Info.RenderTargetStateBefore);
		attachmentDescription.finalLayout = GetVkImageLayoutFrom(m_Info.RenderTargetStateAfter);
		attachmentDescriptions.push_back(attachmentDescription);

		VkAttachmentReference attachmentReference{};
		attachmentReference.attachment = static_cast<uint32_t>(attachmentDescriptions.size() - 1);
		attachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		colorAttachmentReferences.push_back(attachmentReference);
	}

	VkAttachmentReference depthStencilAttachmentReference;
	if (depthStencilView.has_value())
	{
		++this_state->AttachmentCount;
		ImageView* depthStencilViewValue = depthStencilView.value();

		ClearValue clearDepthStencil = depthStencilViewValue->GetInfo().ClearValue;
		VkClearValue depthStencil{};
		depthStencil.depthStencil.depth = clearDepthStencil.DepthStencil.Depth;
		depthStencil.depthStencil.stencil = clearDepthStencil.DepthStencil.Stencil;
		this_state->m_ClearValues[colorAttachmentCount] = depthStencil;

		VkAttachmentDescription attachmentDescription{};
		attachmentDescription.format = get_state(depthStencilViewValue)->m_Format;
		attachmentDescription.samples = GetVkSampleFlagsFrom(depthStencilViewValue->GetInfo().Resource->GetInfo().MultisampleType);
		attachmentDescription.loadOp = m_Info.ClearDepth ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
		attachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachmentDescription.stencilLoadOp = m_Info.ClearStencil ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
		attachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachmentDescription.initialLayout = GetVkImageLayoutFrom(m_Info.DepthStencilStateBefore);
		attachmentDescription.finalLayout = GetVkImageLayoutFrom(m_Info.DepthStencilStateAfter);
		attachmentDescriptions.push_back(attachmentDescription);

		depthStencilAttachmentReference.attachment = static_cast<uint32_t>(attachmentDescriptions.size() - 1);
		depthStencilAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	}

	VkSubpassDescription subpassDescription{};
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = colorAttachmentCount;
	subpassDescription.pColorAttachments = colorAttachmentCount ? colorAttachmentReferences.data() : nullptr;
	subpassDescription.pDepthStencilAttachment = depthStencilView.has_value() ? &depthStencilAttachmentReference : nullptr;

	VkRenderPassCreateInfo renderPassCreateInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO };
	renderPassCreateInfo.attachmentCount = static_cast<uint32_t>(attachmentDescriptions.size());
	renderPassCreateInfo.pAttachments = attachmentDescriptions.data();
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpassDescription;

	if (m_Info.RenderTargetStateAfter != ResourceState::RenderTarget)
	{
		VkSubpassDependency dependencies[2];

		dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
		dependencies[0].dstSubpass = 0;
		dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		dependencies[1].srcSubpass = 0;
		dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
		dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

		renderPassCreateInfo.dependencyCount = 2;
		renderPassCreateInfo.pDependencies = dependencies;
	}

	FLUENT_API_CALL(vkCreateRenderPass(this_state->m_Device, &renderPassCreateInfo, nullptr, &this_state->m_RenderPass));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_RenderPass), VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT, name);
#endif // FLUENT_DEBUG
}

RenderPass::~RenderPass()
{
	vkDestroyRenderPass(this_state->m_Device, this_state->m_RenderPass, nullptr);
}

FLUENT_END_NAMESPACE
