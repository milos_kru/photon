#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

PresentSurface::PresentSurface(const Instance* instance, const Device* device,
	const Queue* presentQueue, const PresentSurfaceInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(PresentSurface))
{
	this_state->m_Instance = get_state(instance)->m_Instance;

	VkWin32SurfaceCreateInfoKHR surfaceCreateInfo = { VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR };
	surfaceCreateInfo.hinstance = m_Info.WindowInstance;
	surfaceCreateInfo.hwnd = m_Info.Windowhandle;
	FLUENT_API_CALL(vkCreateWin32SurfaceKHR(this_state->m_Instance, &surfaceCreateInfo, nullptr, &this_state->m_Surface));

	VkBool32 supportedSurface;
	FLUENT_API_CALL(vkGetPhysicalDeviceSurfaceSupportKHR(get_state(device)->m_PhysicalDevice,
		get_state(presentQueue)->m_FamilyIndex, this_state->m_Surface, &supportedSurface));
	FLUENT_ASSERT(supportedSurface == VK_TRUE);

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Surface), VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT, name);
#endif // FLUENT_DEBUG
}

PresentSurface::~PresentSurface()
{
	vkDestroySurfaceKHR(this_state->m_Instance, this_state->m_Surface, nullptr);
}

FLUENT_END_NAMESPACE
