#include "precompiled.h"

#include <Crc32.h>

FLUENT_BEGIN_NAMESPACE

Shader::Shader(const Device* device, size_t byteCodeSize,
	const char* byteCode, const ShaderInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(Shader))
{
	this_state->m_Device = get_state(device)->m_Device;

	VkShaderModuleCreateInfo shaderCreateInfo = { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO };
	shaderCreateInfo.codeSize = byteCodeSize;
	shaderCreateInfo.pCode = reinterpret_cast<const uint32_t*>(byteCode);

	FLUENT_API_CALL(vkCreateShaderModule(this_state->m_Device, &shaderCreateInfo, nullptr, &this_state->m_ShaderModule));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_ShaderModule), VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT, name);
#endif // FLUENT_DEBUG

	SpvReflectShaderModule spvModule;
	FLUENT_SPIRV_CALL(spvReflectCreateShaderModule(byteCodeSize, byteCode, &spvModule));

	uint32_t bindingCount = 0;
	FLUENT_SPIRV_CALL(spvReflectEnumerateDescriptorBindings(&spvModule, &bindingCount, nullptr));

	std::vector<SpvReflectDescriptorBinding*> bindings(bindingCount);
	FLUENT_SPIRV_CALL(spvReflectEnumerateDescriptorBindings(&spvModule, &bindingCount, bindings.data()));

	this_state->m_DescriptorSetLayoutBindings.resize(bindingCount);
	for (uint32_t i = 0; i < bindingCount; ++i)
	{
		VkDescriptorSetLayoutBinding& descriptorSetLayoutBinding = this_state->m_DescriptorSetLayoutBindings[i];
		descriptorSetLayoutBinding.binding = bindings[i]->binding;
		descriptorSetLayoutBinding.descriptorCount = 1;
		descriptorSetLayoutBinding.descriptorType = GetVkDescriptorTypeFrom(bindings[i]->descriptor_type);
		descriptorSetLayoutBinding.stageFlags = GetVkShaderStageFrom(m_Info.Type);
		descriptorSetLayoutBinding.pImmutableSamplers = nullptr;
	}

	spvReflectDestroyShaderModule(&spvModule);
}

Shader::~Shader()
{
	vkDestroyShaderModule(this_state->m_Device, this_state->m_ShaderModule, nullptr);
}

uint32_t Shader::GetHash() const
{
	return crc32_16bytes(&this_state->m_ShaderModule, sizeof(this_state->m_ShaderModule), 0);
}

FLUENT_END_NAMESPACE
