#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

CommandPool::CommandPool(const Device* device, const Queue* queue, const char* name)
	: m_State(new_state(CommandPool))
{
	this_state->m_Device = get_state(device)->m_Device;

	VkCommandPoolCreateInfo commandPoolCreateInfo = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
	commandPoolCreateInfo.queueFamilyIndex = get_state(queue)->m_FamilyIndex;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	FLUENT_API_CALL(vkCreateCommandPool(get_state(device)->m_Device, &commandPoolCreateInfo, nullptr, &this_state->m_CommandPool));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_CommandPool), VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT, name);
#endif // FLUENT_DEBUG
}

CommandPool::~CommandPool()
{
	vkDestroyCommandPool(this_state->m_Device, this_state->m_CommandPool, nullptr);
}

FLUENT_END_NAMESPACE
