#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

Fence::Fence(const Device* device, const char* name)
	: m_State(new_state(Fence))
{
	this_state->m_Device = get_state(device)->m_Device;

	VkFenceCreateInfo fenceCreateInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
	fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	FLUENT_API_CALL(vkCreateFence(this_state->m_Device, &fenceCreateInfo, nullptr, &this_state->m_Fence));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Fence), VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT, name);
#endif // FLUENT_DEBUG
}

Fence::~Fence()
{
	vkDestroyFence(this_state->m_Device, this_state->m_Fence, nullptr);
}

void Fence::Wait()
{
	FLUENT_API_CALL(vkWaitForFences(this_state->m_Device, 1, &this_state->m_Fence, VK_TRUE, UINT64_MAX));
}

void Fence::Reset()
{
	FLUENT_API_CALL(vkResetFences(this_state->m_Device, 1, &this_state->m_Fence));
}

FLUENT_END_NAMESPACE
