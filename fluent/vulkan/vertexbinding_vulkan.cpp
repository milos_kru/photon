#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

VertexBinding::VertexBinding(const std::vector<VertexElement*> elements, const VertexBindingInfo& info)
	: m_Info(info)
	, m_State(new_state(VertexBinding))
{
	this_state->m_VertexAttributeDescriptions.resize(elements.size());
	for (uint32_t i = 0; i < elements.size(); ++i)
	{
		FLUENT_ASSERT(elements[i]->GetInfo().Binding == m_Info.Binding);

		this_state->m_VertexAttributeDescriptions[i] = get_state(elements[i])->m_VertexAttributeDescription;
	}

	this_state->m_VertexInputBindingDescription.binding = m_Info.Binding;
	this_state->m_VertexInputBindingDescription.stride = m_Info.Stride;
	this_state->m_VertexInputBindingDescription.inputRate = GetVkVertexInputRateFrom(m_Info.InputRate);
}

FLUENT_END_NAMESPACE
