#include "precompiled.h"
#include "include\converters_vulkan.h"

FLUENT_BEGIN_NAMESPACE

VmaMemoryUsage GetVmaMemoryUsageFrom(const MemoryAccess memoryAccess)
{
	switch (memoryAccess)
	{
	case MemoryAccess::Host:
		return VMA_MEMORY_USAGE_CPU_ONLY;
	case MemoryAccess::Device:
		return VMA_MEMORY_USAGE_GPU_ONLY;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported MemoryAccessFlags.");
		return VMA_MEMORY_USAGE_UNKNOWN;
	}
}

VkBufferUsageFlags GetVkBufferUsageFlags(const BufferUsageFlags usageFlags)
{
	VkBufferUsageFlags bufferUsageFlags = 0;
	if (IsFlagSet(usageFlags & BufferUsageFlags::TransferSrc))
	{
		bufferUsageFlags |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	}

	if (IsFlagSet(usageFlags & BufferUsageFlags::TransferDst))
	{
		bufferUsageFlags |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	}

	if (IsFlagSet(usageFlags & BufferUsageFlags::VertexBuffer))
	{
		bufferUsageFlags |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	}

	if (IsFlagSet(usageFlags & BufferUsageFlags::IndexBuffer))
	{
		bufferUsageFlags |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
	}

	if (IsFlagSet(usageFlags & BufferUsageFlags::UniformBuffer))
	{
		bufferUsageFlags |= VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	}

	if (IsFlagSet(usageFlags & BufferUsageFlags::IndirectBuffer))
	{
		bufferUsageFlags |= VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
	}

	return bufferUsageFlags;
}

VkFormat GetVkFormatFrom(const Format format)
{
	switch (format)
	{
	case Format::D32_SFLOAT_S8_UINT:
		return VK_FORMAT_D32_SFLOAT_S8_UINT;
	case Format::D32_SFLOAT:
		return VK_FORMAT_D32_SFLOAT;
	case Format::BGRA8_UNORM:
		return VK_FORMAT_B8G8R8A8_UNORM;
	case Format::BGRA8_UNORM_SRGB:
		return VK_FORMAT_B8G8R8A8_SRGB;
	case Format::RGBA8_UNORM:
		return VK_FORMAT_R8G8B8A8_UNORM;
	case Format::RGBA8_UNORM_SRGB:
		return VK_FORMAT_R8G8B8A8_SRGB;
	case Format::R8_UNORM:
		return VK_FORMAT_R8_UNORM;
	case Format::R32_SFLOAT:
		return VK_FORMAT_R32_SFLOAT;
	case Format::RGBA16_FLOAT:
		return VK_FORMAT_R16G16B16A16_SFLOAT;
	case Format::RG16_FLOAT:
		return VK_FORMAT_R16G16_SFLOAT;
	case Format::RGBA32_FLOAT:
		return VK_FORMAT_R32G32B32A32_SFLOAT;
	case Format::RGB32_FLOAT:
		return VK_FORMAT_R32G32B32_SFLOAT;
	case Format::RG32_FLOAT:
		return VK_FORMAT_R32G32_SFLOAT;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported Format.");
		return VK_FORMAT_UNDEFINED;
	}
}

VkColorSpaceKHR GetVkColorSpaceFrom(const ColorSpace colorSpace)
{
	switch (colorSpace)
	{
	case ColorSpace::SRGB:
		return VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	case ColorSpace::Linear:
		return VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported ColorSpace.");
		return VK_COLOR_SPACE_MAX_ENUM_KHR;
	}
}

VkImageUsageFlags GetVkImageUsageFlagsFrom(const ImageUsageFlags usageFlags)
{
	VkImageUsageFlags imageUsageFlags = 0;
	if (IsFlagSet(usageFlags & ImageUsageFlags::TransferSrc))
	{
		imageUsageFlags |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	}

	if (IsFlagSet(usageFlags & ImageUsageFlags::TransferDst))
	{
		imageUsageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	}

	if (IsFlagSet(usageFlags & ImageUsageFlags::Resource))
	{
		imageUsageFlags |= VK_IMAGE_USAGE_SAMPLED_BIT;
	}

	if (IsFlagSet(usageFlags & ImageUsageFlags::Storage))
	{
		imageUsageFlags |= VK_IMAGE_USAGE_STORAGE_BIT;
	}

	if (IsFlagSet(usageFlags & ImageUsageFlags::RenderTarget))
	{
		imageUsageFlags |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	}

	if (IsFlagSet(usageFlags & ImageUsageFlags::DepthStencil))
	{
		imageUsageFlags |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	}

	return imageUsageFlags;
}

VkImageType GetVkImageTypeFrom(const uint32_t imageDepth)
{
	if (imageDepth > 1)
	{
		return VK_IMAGE_TYPE_3D;
	}
	else
	{
		return VK_IMAGE_TYPE_2D;
	}
}

VkImageAspectFlags GetVkImageAspectFlagsFrom(VkFormat format)
{
	switch (format)
	{
	case VK_FORMAT_D16_UNORM:
	case VK_FORMAT_X8_D24_UNORM_PACK32:
	case VK_FORMAT_D32_SFLOAT:
		return VK_IMAGE_ASPECT_DEPTH_BIT;
	case VK_FORMAT_S8_UINT:
		return VK_IMAGE_ASPECT_STENCIL_BIT;
	case VK_FORMAT_D16_UNORM_S8_UINT:
	case VK_FORMAT_D24_UNORM_S8_UINT:
	case VK_FORMAT_D32_SFLOAT_S8_UINT:
		return VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
	default:
		return VK_IMAGE_ASPECT_COLOR_BIT;
	}
}

VkImageLayout GetVkImageLayoutFrom(const ResourceState resourceState)
{
	if (resourceState == ResourceState::None)
	{
		return VK_IMAGE_LAYOUT_UNDEFINED;
	}
	else if (resourceState == ResourceState::TransferSrc)
	{
		return VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	}
	else if (resourceState == ResourceState::TransferDst)
	{
		return VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	}
	else if (resourceState == ResourceState::DepthStencilRead)
	{
		return VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
	}
	else if (resourceState == ResourceState::DepthStencilWrite)
	{
		return VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	}
	else if (resourceState == ResourceState::GenericRead)
	{
		return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	}
	else if (resourceState == ResourceState::RenderTarget)
	{
		return VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	}
	else if (resourceState == ResourceState::Present)
	{
		return VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	}

	FLUENT_ASSERT_ALWAYS("Unsupported ResourceState.");
	return VK_IMAGE_LAYOUT_UNDEFINED;
}

VkAccessFlags GetVkAccessFlagsFrom(const ResourceState resourceState)
{
	if (resourceState == ResourceState::None)
	{
		return VkAccessFlagBits(0);
	}
	else if (resourceState == ResourceState::TransferSrc)
	{
		return VK_ACCESS_TRANSFER_READ_BIT;
	}
	else if (resourceState == ResourceState::TransferDst)
	{
		return VK_ACCESS_TRANSFER_WRITE_BIT;
	}
	else if (resourceState == ResourceState::DepthStencilRead)
	{
		return VkAccessFlagBits(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
			VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_UNIFORM_READ_BIT);
	}
	else if (resourceState == ResourceState::DepthStencilWrite)
	{
		return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	}
	else if (resourceState == ResourceState::GenericRead)
	{
		return VkAccessFlagBits(VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_UNIFORM_READ_BIT |
			VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT | VK_ACCESS_INDEX_READ_BIT |
			VK_ACCESS_INDIRECT_COMMAND_READ_BIT | VK_ACCESS_TRANSFER_READ_BIT);
	}
	else if (resourceState == ResourceState::RenderTarget)
	{
		return VkAccessFlagBits(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
			VK_ACCESS_COLOR_ATTACHMENT_READ_BIT);
	}
	else if (resourceState == ResourceState::Present)
	{
		return VK_ACCESS_MEMORY_READ_BIT;
	}

	FLUENT_ASSERT_ALWAYS("Unsupported ResourceState.");
	return VK_IMAGE_LAYOUT_UNDEFINED;
}

VkPipelineStageFlagBits GetGraphicsPipelineStage(const ResourceState resourceState)
{
	switch (resourceState)
	{
	case ResourceState::TransferSrc:
	case ResourceState::TransferDst:
		return VK_PIPELINE_STAGE_TRANSFER_BIT;
	case ResourceState::DepthStencilRead:
		return VkPipelineStageFlagBits(
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT |
			VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT |
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
			VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
			VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT);
	case ResourceState::DepthStencilWrite:
		return VkPipelineStageFlagBits(
			VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
			VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT);
	case ResourceState::GenericRead:
		return VkPipelineStageFlagBits(
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT |
			VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT |
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
			VK_PIPELINE_STAGE_VERTEX_INPUT_BIT |
			VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT |
			VK_PIPELINE_STAGE_TRANSFER_BIT);
	case ResourceState::RenderTarget:
		return VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	case ResourceState::None:
	case ResourceState::Present:
		return VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported ResourceState.");
	}

	return VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
}

VkPipelineStageFlagBits GetTransferPipelineStage(const ResourceState resourceState)
{
	if ((resourceState == ResourceState::TransferSrc) || (resourceState == ResourceState::TransferDst))
	{
		return VK_PIPELINE_STAGE_TRANSFER_BIT;
	}

	return VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
}

VkShaderStageFlagBits GetVkShaderStageFrom(const ShaderType shaderType)
{
	switch (shaderType)
	{
	case ShaderType::Vertex:
		return VK_SHADER_STAGE_VERTEX_BIT;
	case ShaderType::Fragment:
		return VK_SHADER_STAGE_FRAGMENT_BIT;
	case ShaderType::Geometry:
		return VK_SHADER_STAGE_GEOMETRY_BIT;
	case ShaderType::Hull:
		return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
	case ShaderType::Domain:
		return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported ShaderType.");
		return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
	}
}

VkPrimitiveTopology GetVkPrimitiveTopologyFrom(const Topology topology)
{
	switch (topology)
	{
	case Topology::PointList:
		return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
	case Topology::LineList:
		return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
	case Topology::LineStrip:
		return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
	case Topology::TriangleList:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	case Topology::TriangleStrip:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
	case Topology::PatchList:
		return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported Topology.");
		return VK_PRIMITIVE_TOPOLOGY_MAX_ENUM;
	}
}

VkSampleCountFlagBits GetVkSampleFlagsFrom(const MultisampleType multiSampleType)
{
	switch (multiSampleType)
	{
	case MultisampleType::Sample_1:
		return VK_SAMPLE_COUNT_1_BIT;
	case MultisampleType::Sample_2:
		return VK_SAMPLE_COUNT_2_BIT;
	case MultisampleType::Sample_4:
		return VK_SAMPLE_COUNT_4_BIT;
	case MultisampleType::Sample_8:
		return VK_SAMPLE_COUNT_8_BIT;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported MultisampleType.");
		return VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
	}
}

VkCompareOp GetVkCompareOperationFrom(const CompareOperation compareOperation)
{
	switch (compareOperation)
	{
	case CompareOperation::Never:
		return VK_COMPARE_OP_NEVER;
	case CompareOperation::Less:
		return VK_COMPARE_OP_LESS;
	case CompareOperation::Equal:
		return VK_COMPARE_OP_EQUAL;
	case CompareOperation::LessEqual:
		return VK_COMPARE_OP_LESS_OR_EQUAL;
	case CompareOperation::Greater:
		return VK_COMPARE_OP_GREATER;
	case CompareOperation::NotEqual:
		return VK_COMPARE_OP_NOT_EQUAL;
	case CompareOperation::GreaterEqual:
		return VK_COMPARE_OP_GREATER_OR_EQUAL;
	case CompareOperation::Always:
		return VK_COMPARE_OP_ALWAYS;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported CompareOperation.");
		return VK_COMPARE_OP_MAX_ENUM;
	}
}

VkStencilOp GetVkStencilOpFrom(const StencilOperation stencilOperation)
{
	switch (stencilOperation)
	{
	case StencilOperation::Keep:
		return VK_STENCIL_OP_KEEP;
	case StencilOperation::Zero:
		return VK_STENCIL_OP_ZERO;
	case StencilOperation::Replace:
		return VK_STENCIL_OP_REPLACE;
	case StencilOperation::IncrementAndClamp:
		return VK_STENCIL_OP_INCREMENT_AND_CLAMP;
	case StencilOperation::DecrementAndClamp:
		return VK_STENCIL_OP_DECREMENT_AND_CLAMP;
	case StencilOperation::Invert:
		return VK_STENCIL_OP_INVERT;
	case StencilOperation::Increment:
		return VK_STENCIL_OP_INCREMENT_AND_WRAP;
	case StencilOperation::Decrement:
		return VK_STENCIL_OP_DECREMENT_AND_WRAP;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported StencilOperation.");
		return VK_STENCIL_OP_MAX_ENUM;
	}
}

VkFilter GetVkFilterFrom(const FilterType filterType)
{
	switch (filterType)
	{
	case FilterType::Point:
		return VK_FILTER_NEAREST;
	case FilterType::Linear:
		return VK_FILTER_LINEAR;
	case FilterType::Anisotropic:
		return VK_FILTER_LINEAR;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported FilterType.");
		return VK_FILTER_MAX_ENUM;
	}
}

VkSamplerMipmapMode GetVkMipmapModeFrom(const FilterType filterType)
{
	switch (filterType)
	{
	case FilterType::Point:
		return VK_SAMPLER_MIPMAP_MODE_NEAREST;
	case FilterType::Linear:
		return VK_SAMPLER_MIPMAP_MODE_LINEAR;
	case FilterType::Anisotropic:
		return VK_SAMPLER_MIPMAP_MODE_LINEAR;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported FilterType.");
		return VK_SAMPLER_MIPMAP_MODE_MAX_ENUM;
	}
}

VkSamplerAddressMode GetVkAddressModeFrom(const AddressMode addressMode)
{
	switch (addressMode)
	{
	case AddressMode::Repeat:
		return VK_SAMPLER_ADDRESS_MODE_REPEAT;
	case AddressMode::Mirror:
		return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
	case AddressMode::Clamp:
		return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	case AddressMode::Border:
		return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	case AddressMode::MirrorClamp:
		return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported AddressMode.");
		return VK_SAMPLER_ADDRESS_MODE_MAX_ENUM;
	}
}

VkBorderColor GetVkBorderColorFrom(const Color color)
{
	if (color.value[0] == 0.0f && color.value[1] == 0.0f && color.value[2] == 0.0f)
	{
		return color.value[3] == 1.0f ?
			VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK : VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
	}
	else if (color.value[0] == 1.0f && color.value[1] == 1.0f &&
		color.value[2] == 1.0f && color.value[3] == 1.0f)
	{
		return VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
	}
	else
	{
		FLUENT_ASSERT_ALWAYS("Unsupported border Color.");
		return VK_BORDER_COLOR_MAX_ENUM;
	}
}

VkBlendFactor GetVkBlendFactorFrom(const BlendValue blendValue)
{
	switch (blendValue)
	{
	case BlendValue::Zero:
		return VK_BLEND_FACTOR_ZERO;
	case BlendValue::One:
		return VK_BLEND_FACTOR_ONE;
	case BlendValue::SrcColor:
		return VK_BLEND_FACTOR_SRC_COLOR;
	case BlendValue::InvSrcColor:
		return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
	case BlendValue::SrcAlpha:
		return VK_BLEND_FACTOR_SRC_ALPHA;
	case BlendValue::InvSrcAlpha:
		return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	case BlendValue::DestAlpha:
		return VK_BLEND_FACTOR_DST_ALPHA;
	case BlendValue::InvDestAlpha:
		return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
	case BlendValue::DstColor:
		return VK_BLEND_FACTOR_DST_COLOR;
	case BlendValue::InvDstColor:
		return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
	case BlendValue::BlendFactor:
		return VK_BLEND_FACTOR_CONSTANT_COLOR;
	case BlendValue::InvBlendFactor:
		return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
	case BlendValue::SrcAlphaSat:
		return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
	case BlendValue::Src1Color:
		return VK_BLEND_FACTOR_SRC1_COLOR;
	case BlendValue::InvSrc1Color:
		return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
	case BlendValue::Src1Alpha:
		return VK_BLEND_FACTOR_SRC1_ALPHA;
	case BlendValue::InvSrc1Alpha:
		return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported BlendValue.");
		return VK_BLEND_FACTOR_MAX_ENUM;
	}
}

VkBlendOp GetVkBlendOpFrom(const BlendOperation blendOperation)
{
	switch (blendOperation)
	{
	case BlendOperation::Add:
		return VK_BLEND_OP_ADD;
	case BlendOperation::Subtract:
		return VK_BLEND_OP_SUBTRACT;
	case BlendOperation::ReverseSubtract:
		return VK_BLEND_OP_REVERSE_SUBTRACT;
	case BlendOperation::Min:
		return VK_BLEND_OP_MIN;
	case BlendOperation::Max:
		return VK_BLEND_OP_MAX;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported BlendOperation.");
		return VK_BLEND_OP_MAX_ENUM;
	}
}

VkVertexInputRate GetVkVertexInputRateFrom(const VertexInputRate inputRate)
{
	switch (inputRate)
	{
		case VertexInputRate::PerVertex:
			return VK_VERTEX_INPUT_RATE_VERTEX;
		case VertexInputRate::PerInstance:
			return VK_VERTEX_INPUT_RATE_INSTANCE;
		default:
			FLUENT_ASSERT_ALWAYS("Unsupported VertexInputRate.");
			return VK_VERTEX_INPUT_RATE_MAX_ENUM;
	}
}

VkDescriptorType GetVkDescriptorTypeFrom(const DescriptorType descriptorSetBinding)
{
	switch (descriptorSetBinding)
	{
	case DescriptorType::CombinedSampler:
		return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	case DescriptorType::UniformBuffer:
		return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported DescriptorSetBindingType.");
		return VK_DESCRIPTOR_TYPE_MAX_ENUM;
	}
}

VkDescriptorType GetVkDescriptorTypeFrom(const SpvReflectDescriptorType reflectDescriptorType)
{
	switch (reflectDescriptorType)
	{
	case SPV_REFLECT_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
		return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
		return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	default:
		FLUENT_ASSERT_ALWAYS("Unsupported SpvReflectDescriptorBinding.");
		return VK_DESCRIPTOR_TYPE_MAX_ENUM;
	}
}
FLUENT_END_NAMESPACE
