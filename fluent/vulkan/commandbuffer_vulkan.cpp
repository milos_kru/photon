#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

CommandBuffer::CommandBuffer(const Device* device, const CommandPool* commandPool,
	const StateHandle stateHandle, const char* name)
	: m_State(stateHandle)
{
	this_state->m_Device = device;
	this_state->m_CommandPool = get_state(commandPool)->m_CommandPool;
	this_state->m_SrcPipelineStage = 0;
	this_state->m_DstPipelineStage = 0;
	this_state->m_Recording = false;

	VkCommandBufferAllocateInfo commandBufferAllocateInfo;
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.pNext = nullptr;
	commandBufferAllocateInfo.commandBufferCount = 1;
	commandBufferAllocateInfo.commandPool = this_state->m_CommandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	FLUENT_API_CALL(vkAllocateCommandBuffers(get_state(device)->m_Device, &commandBufferAllocateInfo, &this_state->m_CommandBuffer));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_CommandBuffer), VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT, name);
#endif // FLUENT_DEBUG
}

CommandBuffer::~CommandBuffer()
{
	vkFreeCommandBuffers(get_state(this_state->m_Device)->m_Device, this_state->m_CommandPool, 1, &this_state->m_CommandBuffer);
}

void CommandBuffer::Begin()
{
	FLUENT_ASSERT(!this_state->m_Recording);

	VkCommandBufferBeginInfo commandBufferBeginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	FLUENT_API_CALL(vkBeginCommandBuffer(this_state->m_CommandBuffer, &commandBufferBeginInfo));

	this_state->m_Recording = true;
}

void CommandBuffer::End()
{
	FLUENT_ASSERT(this_state->m_Recording);

	CommitBarriers();
	FLUENT_API_CALL(vkEndCommandBuffer(this_state->m_CommandBuffer));

	this_state->m_Recording = false;
}

void UpdatePipelineStage(const CommandBuffer* commandBuffer,
	const ResourceState stateBefore, const ResourceState stateAfter)
{
	if (stateBefore == stateAfter)
	{
		return;
	}

	if (dynamic_cast<const GraphicsCommandBuffer*>(commandBuffer))
	{
		get_state(commandBuffer)->m_SrcPipelineStage |= GetGraphicsPipelineStage(stateBefore);
		get_state(commandBuffer)->m_DstPipelineStage |= GetGraphicsPipelineStage(stateAfter);
	}
	else if (dynamic_cast<const TransferCommandBuffer*>(commandBuffer))
	{
		get_state(commandBuffer)->m_SrcPipelineStage |= GetTransferPipelineStage(stateBefore);
		get_state(commandBuffer)->m_DstPipelineStage |= GetTransferPipelineStage(stateAfter);
	}
	else
	{
		FLUENT_ASSERT_ALWAYS("Unsupported CommandBuffer type");
	}
}

void CommandBuffer::TransitionResource(const BufferResource* buffer,
	const ResourceState stateBefore, const ResourceState stateAfter, const uint32_t offset)
{
	UpdatePipelineStage(this, stateBefore, stateAfter);

	this_state->m_BufferBarriers.resize(this_state->m_BufferBarriers.size() + 1);
	VkBufferMemoryBarrier& bufferBarrier = this_state->m_BufferBarriers.back();

	bufferBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
	bufferBarrier.srcAccessMask = GetVkAccessFlagsFrom(stateBefore);
	bufferBarrier.dstAccessMask = GetVkAccessFlagsFrom(stateAfter);
	bufferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	bufferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	bufferBarrier.buffer = get_state(buffer)->m_Buffer;
	bufferBarrier.offset = offset;
	bufferBarrier.size = VkDeviceSize(buffer->GetInfo().Size - offset);
}

void AppendNewImageBarrier(std::shared_ptr<CommandBufferState> commandBufferState, const VkImageSubresourceRange subresourceRange,
	const VkImage image, const ResourceState stateBefore, const ResourceState stateAfter)
{
	commandBufferState->m_ImageBarriers.resize(commandBufferState->m_ImageBarriers.size() + 1);
	VkImageMemoryBarrier& imageBarrier = commandBufferState->m_ImageBarriers.back();

	imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageBarrier.srcAccessMask = GetVkAccessFlagsFrom(stateBefore);
	imageBarrier.dstAccessMask = GetVkAccessFlagsFrom(stateAfter);
	imageBarrier.oldLayout = GetVkImageLayoutFrom(stateBefore);
	imageBarrier.newLayout = GetVkImageLayoutFrom(stateAfter);
	imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.image = image;
	imageBarrier.subresourceRange = subresourceRange;
}

void CommandBuffer::TransitionResource(const ImageResource* image,
	const ResourceState stateBefore, const ResourceState stateAfter)
{
	UpdatePipelineStage(this, stateBefore, stateAfter);

	VkImageSubresourceRange subresourceRange;
	subresourceRange.aspectMask = GetVkImageAspectFlagsFrom(get_state(image)->m_Format);
	subresourceRange.baseMipLevel = 0;
	subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
	subresourceRange.baseArrayLayer = 0;
	subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

	AppendNewImageBarrier(this_state, subresourceRange, get_state(image)->m_Image, stateBefore, stateAfter);
}

void CommandBuffer::TransitionResource(const ImageView* imageView,
	const ResourceState stateBefore, const ResourceState stateAfter)
{
	UpdatePipelineStage(this, stateBefore, stateAfter);

	ImageResource* image = imageView->GetInfo().Resource;

	VkImageSubresourceRange subresourceRange;
	subresourceRange.aspectMask = GetVkImageAspectFlagsFrom(get_state(image)->m_Format);
	subresourceRange.baseMipLevel = imageView->GetInfo().Subresource.MipSlice;
	subresourceRange.levelCount = imageView->GetInfo().Subresource.MipSize;
	subresourceRange.baseArrayLayer = imageView->GetInfo().Subresource.ArraySlice;
	subresourceRange.layerCount = imageView->GetInfo().Subresource.ArraySize;

	AppendNewImageBarrier(this_state, subresourceRange, get_state(image)->m_Image, stateBefore, stateAfter);
}

void CommandBuffer::BeginDebugMarker(const char* name, const Color color)
{
#ifdef FLUENT_DEBUG
	Fluent::BeginDebugMarker(this_state->m_Device, this_state->m_CommandBuffer, name, color);
#endif // FLUENT_DEBUG
}

void CommandBuffer::EndDebugMarker()
{
#ifdef FLUENT_DEBUG
	Fluent::EndDebugMarker(this_state->m_Device, this_state->m_CommandBuffer);
#endif // FLUENT_DEBUG
}

void CommandBuffer::CommitBarriers()
{
	bool hasPendingBarriers = this_state->m_BufferBarriers.size() || this_state->m_ImageBarriers.size() || this_state->m_MemoryBarriers.size();
	if (!hasPendingBarriers)
	{
		return;
	}

	FLUENT_ASSERT(this_state->m_SrcPipelineStage);
	FLUENT_ASSERT(this_state->m_DstPipelineStage);

	vkCmdPipelineBarrier(this_state->m_CommandBuffer, this_state->m_SrcPipelineStage, this_state->m_DstPipelineStage, VK_DEPENDENCY_BY_REGION_BIT,
		static_cast<uint32_t>(this_state->m_MemoryBarriers.size()), this_state->m_MemoryBarriers.size() ? this_state->m_MemoryBarriers.data() : nullptr,
		static_cast<uint32_t>(this_state->m_BufferBarriers.size()), this_state->m_BufferBarriers.size() ? this_state->m_BufferBarriers.data() : nullptr,
		static_cast<uint32_t>(this_state->m_ImageBarriers.size()), this_state->m_ImageBarriers.size() ? this_state->m_ImageBarriers.data() : nullptr);

	this_state->m_BufferBarriers.clear();
	this_state->m_ImageBarriers.clear();
	this_state->m_MemoryBarriers.clear();

	this_state->m_SrcPipelineStage = 0;
	this_state->m_DstPipelineStage = 0;
}

FLUENT_END_NAMESPACE
