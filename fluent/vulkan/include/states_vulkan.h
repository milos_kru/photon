#pragma once

FLUENT_BEGIN_NAMESPACE

struct InstanceState
{
	VkInstance m_Instance;

#ifdef FLUENT_DEBUG
	VkDebugUtilsMessengerEXT m_DebugMessenger;
#endif // FLUENT_DEBUG
};
FLUENT_DEFINE_GET_STATE(Instance)

struct DeviceState
{
	VkDevice m_Device;
	VkPhysicalDevice m_PhysicalDevice;
	std::vector<uint32_t> m_QueueFamilyIndices;
	VmaAllocator m_Allocator;

#ifdef FLUENT_DEBUG
	PFN_vkDebugMarkerSetObjectNameEXT m_DebugMarkerSetObjectName;
	PFN_vkCmdDebugMarkerBeginEXT m_CmdDebugMarkerBegin;
	PFN_vkCmdDebugMarkerEndEXT m_CmdDebugMarkerEnd;
#endif // FLUENT_DEBUG
};
FLUENT_DEFINE_GET_STATE(Device)

struct QueueState
{
	VkQueue m_Queue;
	uint32_t m_FamilyIndex;
};
FLUENT_DEFINE_GET_STATE(Queue)

struct PresentSurfaceState
{
	VkSurfaceKHR m_Surface;
	VkInstance m_Instance;
};
FLUENT_DEFINE_GET_STATE(PresentSurface)

struct SwapchainState
{
	VkSwapchainKHR m_Swapchain;
	VkExtent2D m_Extent;
	VkSurfaceKHR m_Surface;
	VkQueue m_Queue;
	VkPhysicalDevice m_PhysicalDevice;
	std::vector<VkImage> m_BackBufferImages;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(Swapchain)

struct SemaphoreState
{
	VkSemaphore m_Semaphore;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(Semaphore)

struct FenceState
{
	VkFence m_Fence;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(Fence)

struct BufferResourceState
{
	VkBuffer m_Buffer;
	VmaAllocation m_Allocation;
	VkDevice m_Device;
	VmaAllocator m_Allocator;
	void* m_HostVisibleData;
};
FLUENT_DEFINE_GET_STATE(BufferResource)

struct ImageResourceState
{
	VkImage m_Image;
	VmaAllocation m_Allocation;
	VkFormat m_Format;
	VkImageType m_ImageType;
	VkDevice m_Device;
	VmaAllocator m_Allocator;
};
FLUENT_DEFINE_GET_STATE(ImageResource)

struct ImageViewState
{
	VkImageView m_ImageView;
	VkFormat m_Format;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(ImageView)

struct ImageSamplerState
{
	VkSampler m_Sampler;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(ImageSampler)

struct ShaderState
{
	VkShaderModule m_ShaderModule;
	std::vector<VkDescriptorSetLayoutBinding> m_DescriptorSetLayoutBindings;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(Shader)

struct GraphicsPipelineDescriptionState
{
	uint32_t m_ShaderStageCount;
	VkPipelineShaderStageCreateInfo m_ShaderStageCreateInfo[static_cast<uint32_t>(ShaderType::Count)];
	VkPipelineVertexInputStateCreateInfo m_VertexInputStateCreateInfo;
	VkPipelineInputAssemblyStateCreateInfo m_InputAssemblyStateCreateInfo;
	VkPipelineViewportStateCreateInfo m_ViewportStateCreateInfo;
	VkPipelineMultisampleStateCreateInfo m_MultisampleStateCreateInfo;
	VkPipelineDynamicStateCreateInfo m_DynamicStateCreateInfo;
	VkPipelineDepthStencilStateCreateInfo m_DepthStencilStateCreateInfo;
	VkPipelineRasterizationStateCreateInfo m_RasterizationStateCreateInfo;
	VkPipelineColorBlendStateCreateInfo m_ColorBlendStateCreateInfo;
	VkPipelineColorBlendAttachmentState m_ColorBlendAttachmentState[MaxRenderTargetCount];
	VkPipelineTessellationStateCreateInfo m_TessellatinStateCreateInfo;
};
FLUENT_DEFINE_GET_STATE(GraphicsPipelineDescription)

struct GraphicsPipelineState
{
	VkPipeline m_Pipeline;
	VkPipelineLayout m_PipelineLayout;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(GraphicsPipeline)

struct RenderPassState
{
	VkRenderPass m_RenderPass;
	uint32_t AttachmentCount;
	VkClearValue m_ClearValues[MaxRenderTargetCount];
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(RenderPass)

struct FramebufferState
{
	VkFramebuffer m_Framebuffer;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(Framebuffer)

struct DescriptorSetLayoutState
{
	VkDescriptorSetLayout m_DescriptorSetLayout;
	VkPipelineLayout m_PipelineLayout;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(DescriptorSetLayout)

struct DescriptorPoolState
{
	VkDescriptorPool m_DescriptorPool;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(DescriptorPool)

struct DescriptorState
{
	union
	{
		VkDescriptorImageInfo ImageInfo;
		VkDescriptorBufferInfo BufferInfo;
	} m_ResourceInfo;
};
FLUENT_DEFINE_GET_STATE(Descriptor)

struct DescriptorSetState
{
	VkDescriptorSet m_DescriptorSet;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(DescriptorSet)

struct VertexElementState
{
	VkVertexInputAttributeDescription m_VertexAttributeDescription;
};
FLUENT_DEFINE_GET_STATE(VertexElement)

struct VertexBindingState
{
	VkVertexInputBindingDescription m_VertexInputBindingDescription;
	std::vector<VkVertexInputAttributeDescription> m_VertexAttributeDescriptions;
};
FLUENT_DEFINE_GET_STATE(VertexBinding)

struct CommandPoolState
{
	VkCommandPool m_CommandPool;
	VkDevice m_Device;
};
FLUENT_DEFINE_GET_STATE(CommandPool)

struct CommandBufferState
{
	VkCommandBuffer m_CommandBuffer;
	VkCommandPool m_CommandPool;
	VkPipelineStageFlags m_SrcPipelineStage;
	VkPipelineStageFlags m_DstPipelineStage;

	std::vector<VkMemoryBarrier> m_MemoryBarriers;
	std::vector<VkImageMemoryBarrier> m_ImageBarriers;
	std::vector<VkBufferMemoryBarrier> m_BufferBarriers;

	const Device* m_Device;
	bool m_Recording;
};
FLUENT_DEFINE_GET_STATE(CommandBuffer)

struct GraphicsCommandBufferState : public CommandBufferState
{
	VkPipelineLayout m_PipelineLayout;
};
FLUENT_DEFINE_GET_STATE(GraphicsCommandBuffer)

struct TransferCommandBufferState : public CommandBufferState {};
FLUENT_DEFINE_GET_STATE(TransferCommandBuffer)

FLUENT_END_NAMESPACE
