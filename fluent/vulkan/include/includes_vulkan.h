#pragma once

#ifdef _WIN32
	#define VK_USE_PLATFORM_WIN32_KHR
#endif // _WIN32

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <spirv_reflect.h>

#define FLUENT_SPIRV_CALL(call) \
		{ \
			SpvReflectResult result = call; \
			FLUENT_ASSERT(result == SPV_REFLECT_RESULT_SUCCESS); \
		}

#define FLUENT_VULKAN_API_VERSION VK_API_VERSION_1_1

#include "debug_vulkan.h"
#include "converters_vulkan.h"
#include "states_vulkan.h"
