#pragma once

#ifdef FLUENT_DEBUG

FLUENT_BEGIN_NAMESPACE

void SetDebugName(const Device* device, const uint64_t object, const VkDebugReportObjectTypeEXT type, const char* name);
void BeginDebugMarker(const Device* device, const VkCommandBuffer commandBuffer, const char* name, const Color color);
void EndDebugMarker(const Device* device, const VkCommandBuffer commandBuffer);

FLUENT_END_NAMESPACE

#endif // FLUENT_DEBUG
