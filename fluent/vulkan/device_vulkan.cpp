#include "precompiled.h"

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

FLUENT_BEGIN_NAMESPACE

bool DoesSupportGraphicsAndComputeQueue(const VkPhysicalDevice physicalDevice)
{
	uint32_t queueFamilyPropertiesCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertiesCount, nullptr);
	FLUENT_ASSERT(queueFamilyPropertiesCount > 0);

	std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyPropertiesCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice,
		&queueFamilyPropertiesCount, queueFamilyProperties.data());
	FLUENT_ASSERT(queueFamilyPropertiesCount == queueFamilyProperties.size());

	for (const VkQueueFamilyProperties& properties : queueFamilyProperties)
	{
		if (properties.queueFlags & VK_QUEUE_GRAPHICS_BIT &&
			properties.queueFlags & VK_QUEUE_COMPUTE_BIT)
		{
			return true;
		}
	}

	return false;
}

std::optional<VkPhysicalDevice> FindSuitablePhysicalDevice(const VkInstance vkInstance)
{
	uint32_t deviceCount = 0;
	FLUENT_API_CALL(vkEnumeratePhysicalDevices(vkInstance, &deviceCount, nullptr));
	FLUENT_ASSERT(deviceCount > 0);

	std::vector<VkPhysicalDevice> availablePhysicalDevices(deviceCount);
	FLUENT_API_CALL(vkEnumeratePhysicalDevices(vkInstance, &deviceCount, availablePhysicalDevices.data()));

	std::optional<VkPhysicalDevice> selectedPhysicalDevice = std::nullopt;
	for (const VkPhysicalDevice& physicalDevice : availablePhysicalDevices)
	{
		VkPhysicalDeviceProperties properties;
		vkGetPhysicalDeviceProperties(physicalDevice, &properties);
		if (DoesSupportGraphicsAndComputeQueue(physicalDevice))
		{
			selectedPhysicalDevice = physicalDevice;
			if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			{
				break;
			}
		}
	}

	return selectedPhysicalDevice;
}

std::optional<uint32_t> GetQueueFamilyIndex(const VkPhysicalDevice physicalDevice, const VkQueueFlagBits queueFlags)
{
	uint32_t queueFamilyCount;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
	FLUENT_ASSERT(queueFamilyCount > 0);

	std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProperties.data());

	if (queueFlags & VK_QUEUE_COMPUTE_BIT)
	{
		for (uint32_t i = 0; i < static_cast<uint32_t>(queueFamilyProperties.size()); ++i)
		{
			if ((queueFamilyProperties[i].queueFlags & queueFlags) &&
				((queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0))
			{
				return i;
			}
		}
	}

	if (queueFlags & VK_QUEUE_TRANSFER_BIT)
	{
		for (uint32_t i = 0; i < static_cast<uint32_t>(queueFamilyProperties.size()); ++i)
		{
			if ((queueFamilyProperties[i].queueFlags & queueFlags) &&
				((queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0) &&
				((queueFamilyProperties[i].queueFlags & VK_QUEUE_COMPUTE_BIT) == 0))
			{
				return i;
			}
		}
	}

	for (uint32_t i = 0; i < static_cast<uint32_t>(queueFamilyProperties.size()); ++i)
	{
		if (queueFamilyProperties[i].queueFlags & queueFlags)
		{
			return i;
		}
	}

	return std::nullopt;
}

bool IsDeviceExtensionAvailable(const std::vector<VkExtensionProperties> availableExtensions,
	const char* extensionName)
{
	for (const VkExtensionProperties& availableExtension : availableExtensions)
	{
		if (strcmp(availableExtension.extensionName, extensionName) == 0)
		{
			return true;
		}
	}

	return false;
}

bool CheckDeviceExtensionsSupport(const std::vector<VkExtensionProperties> availableExtensions,
	const std::vector<const char*> extensionNames)
{
	for (const char* extensionName : extensionNames)
	{
		if (!IsDeviceExtensionAvailable(availableExtensions, extensionName))
		{
			return false;
		}
	}

	return true;
}

std::vector<const char*> GetRequiredExtensionNames(const VkPhysicalDevice physicalDevice, const bool enableDebugMarkers)
{
	uint32_t extensionCount;
	FLUENT_API_CALL(vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, nullptr));

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	FLUENT_API_CALL(vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, availableExtensions.data()));

	std::vector<const char*> requiredExtensionNames = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

#ifdef FLUENT_DEBUG
	if (enableDebugMarkers)
	{
		requiredExtensionNames.push_back(VK_EXT_DEBUG_MARKER_EXTENSION_NAME);
	}
#endif // FLUENT_DEBUG

	FLUENT_ASSERT(CheckDeviceExtensionsSupport(availableExtensions, requiredExtensionNames));

	return requiredExtensionNames;
}

Device::Device(const Instance* instance, const DeviceInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(Device))
{
	std::optional<VkPhysicalDevice> physicalDevice = FindSuitablePhysicalDevice(get_state(instance)->m_Instance);
	FLUENT_ASSERT(physicalDevice.has_value());
	this_state->m_PhysicalDevice = physicalDevice.value();

	this_state->m_QueueFamilyIndices.resize(static_cast<uint32_t>(QueueType::Count));

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos{};
	const float defaultQueuePriority = 0.0f;

	std::optional<uint32_t> graphicsIndex = GetQueueFamilyIndex(this_state->m_PhysicalDevice, VK_QUEUE_GRAPHICS_BIT);
	FLUENT_ASSERT(graphicsIndex.has_value());
	this_state->m_QueueFamilyIndices[static_cast<uint32_t>(QueueType::Graphics)] = graphicsIndex.value();
	this_state->m_QueueFamilyIndices[static_cast<uint32_t>(QueueType::Present)] = graphicsIndex.value();

	VkDeviceQueueCreateInfo graphicsQueueCreateInfo{};
	graphicsQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	graphicsQueueCreateInfo.queueFamilyIndex = graphicsIndex.value();
	graphicsQueueCreateInfo.queueCount = 1;
	graphicsQueueCreateInfo.pQueuePriorities = &defaultQueuePriority;
	queueCreateInfos.push_back(graphicsQueueCreateInfo);

	std::optional<uint32_t> computeIndex = GetQueueFamilyIndex(this_state->m_PhysicalDevice, VK_QUEUE_COMPUTE_BIT);
	FLUENT_ASSERT(computeIndex.has_value());
	this_state->m_QueueFamilyIndices[static_cast<uint32_t>(QueueType::Compute)] = computeIndex.value();

	if (computeIndex != graphicsIndex)
	{
		VkDeviceQueueCreateInfo computeQueueCreateInfo{};
		computeQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		computeQueueCreateInfo.queueFamilyIndex = computeIndex.value();
		computeQueueCreateInfo.queueCount = 1;
		computeQueueCreateInfo.pQueuePriorities = &defaultQueuePriority;
		queueCreateInfos.push_back(computeQueueCreateInfo);
	}

	std::optional<uint32_t> transferIndex = GetQueueFamilyIndex(this_state->m_PhysicalDevice, VK_QUEUE_TRANSFER_BIT);
	FLUENT_ASSERT(transferIndex.has_value());
	this_state->m_QueueFamilyIndices[static_cast<uint32_t>(QueueType::Transfer)] = transferIndex.value();

	if ((transferIndex != graphicsIndex) && (transferIndex != computeIndex))
	{
		VkDeviceQueueCreateInfo transferQueueCreateInfo{};
		transferQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		transferQueueCreateInfo.queueFamilyIndex = transferIndex.value();
		transferQueueCreateInfo.queueCount = 1;
		transferQueueCreateInfo.pQueuePriorities = &defaultQueuePriority;
		queueCreateInfos.push_back(transferQueueCreateInfo);
	}

	std::vector<const char*> requiredExtensionNames = GetRequiredExtensionNames(this_state->m_PhysicalDevice, m_Info.EnableDebugMarkers);

	VkPhysicalDeviceFeatures requiredFeatures{};
	requiredFeatures.samplerAnisotropy = VK_TRUE;

	VkDeviceCreateInfo deviceCreateInfo = { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO };
	deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
	deviceCreateInfo.pEnabledFeatures = &requiredFeatures;
	deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensionNames.size());
	deviceCreateInfo.ppEnabledExtensionNames = requiredExtensionNames.data();
	deviceCreateInfo.enabledLayerCount = 0;
	deviceCreateInfo.ppEnabledLayerNames = nullptr;

	FLUENT_API_CALL(vkCreateDevice(this_state->m_PhysicalDevice, &deviceCreateInfo, nullptr, &this_state->m_Device));

#ifdef FLUENT_DEBUG
	if (m_Info.EnableDebugMarkers)
	{
		this_state->m_DebugMarkerSetObjectName = reinterpret_cast<PFN_vkDebugMarkerSetObjectNameEXT>(
			vkGetDeviceProcAddr(this_state->m_Device, "vkDebugMarkerSetObjectNameEXT"));
		FLUENT_ASSERT(this_state->m_DebugMarkerSetObjectName);
		this_state->m_CmdDebugMarkerBegin = reinterpret_cast<PFN_vkCmdDebugMarkerBeginEXT>(
			vkGetDeviceProcAddr(this_state->m_Device, "vkCmdDebugMarkerBeginEXT"));
		FLUENT_ASSERT(this_state->m_CmdDebugMarkerBegin);
		this_state->m_CmdDebugMarkerEnd = reinterpret_cast<PFN_vkCmdDebugMarkerEndEXT>(
			vkGetDeviceProcAddr(this_state->m_Device, "vkCmdDebugMarkerEndEXT"));
		FLUENT_ASSERT(this_state->m_CmdDebugMarkerEnd);
	}
#endif // FLUENT_DEBUG

	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.vulkanApiVersion = FLUENT_VULKAN_API_VERSION;
	allocatorInfo.instance = get_state(instance)->m_Instance;
	allocatorInfo.physicalDevice = this_state->m_PhysicalDevice;
	allocatorInfo.device = this_state->m_Device;

	FLUENT_API_CALL(vmaCreateAllocator(&allocatorInfo, &this_state->m_Allocator));

#ifdef FLUENT_DEBUG
	SetDebugName(this, uint64_t(get_state(instance)->m_Instance), VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT, instance->GetInfo().EngineName);
	SetDebugName(this, uint64_t(get_state(instance)->m_DebugMessenger), VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT, instance->GetInfo().EngineName);
	SetDebugName(this, uint64_t(this_state->m_Device), VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT, name);
#endif // FLUENT_DEBUG
}

Device::~Device()
{
	vmaDestroyAllocator(this_state->m_Allocator);
	vkDestroyDevice(this_state->m_Device, nullptr);
}

void Device::WaitIdle()
{
	vkDeviceWaitIdle(this_state->m_Device);
}

FLUENT_END_NAMESPACE
