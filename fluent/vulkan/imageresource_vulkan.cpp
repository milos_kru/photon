#include "precompiled.h"

#include "vk_mem_alloc.h"

FLUENT_BEGIN_NAMESPACE

ImageResource::ImageResource(const Device* device, const ImageResourceInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(ImageResource))
{
	this_state->m_Device = get_state(device)->m_Device;
	this_state->m_Allocator = get_state(device)->m_Allocator;
	this_state->m_Format = GetVkFormatFrom(m_Info.Format);
	this_state->m_ImageType = GetVkImageTypeFrom(m_Info.Extent.Depth);

	VkImageCreateInfo imageCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };

	uint32_t arraySizeMultiplier = 1;
	if (IsFlagSet(m_Info.Flags & ImageFlags::CubeMap))
	{
		arraySizeMultiplier = 6;
		imageCreateInfo.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
	}

	imageCreateInfo.imageType = m_Info.Extent.Depth == 1 ? VK_IMAGE_TYPE_2D : VK_IMAGE_TYPE_3D;
	imageCreateInfo.format = this_state->m_Format;
	imageCreateInfo.extent.width = m_Info.Extent.Width;
	imageCreateInfo.extent.height = m_Info.Extent.Height;
	imageCreateInfo.extent.depth = m_Info.Extent.Depth;
	imageCreateInfo.mipLevels = m_Info.MipCount;
	imageCreateInfo.arrayLayers = arraySizeMultiplier * m_Info.ArraySize;
	imageCreateInfo.samples = GetVkSampleFlagsFrom(m_Info.MultisampleType);
	imageCreateInfo.tiling = IsFlagSet(m_Info.Flags & ImageFlags::LinearTiling) ?
		VK_IMAGE_TILING_LINEAR : VK_IMAGE_TILING_OPTIMAL;
	imageCreateInfo.usage = GetVkImageUsageFlagsFrom(m_Info.UsageFlags);
	imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	VmaAllocationCreateInfo allocationCreateInfo = {};
	allocationCreateInfo.usage = GetVmaMemoryUsageFrom(m_Info.MemoryAccess);

	FLUENT_API_CALL(vmaCreateImage(this_state->m_Allocator, &imageCreateInfo,
		&allocationCreateInfo, &this_state->m_Image, &this_state->m_Allocation, nullptr));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Image), VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT, name);
#endif // FLUENT_DEBUG
}

ImageResource::ImageResource(const Device* device, const Swapchain* swapchain, const uint32_t backBufferIndex)
	: m_State(new_state(ImageResource))
{
	m_Info.Flags = ImageFlags::None;
	m_Info.UsageFlags =
		ImageUsageFlags::TransferDst |
		ImageUsageFlags::Resource |
		ImageUsageFlags::Storage |
		ImageUsageFlags::RenderTarget;
	m_Info.MemoryAccess = MemoryAccess::Device;
	m_Info.Format = swapchain->GetInfo().Format;
	m_Info.Extent.Width = get_state(swapchain)->m_Extent.width;
	m_Info.Extent.Height = get_state(swapchain)->m_Extent.height;
	m_Info.Extent.Depth = 1;
	m_Info.ArraySize = 1;
	m_Info.MipCount = 1;
	m_Info.MultisampleType = MultisampleType::Sample_1;

	this_state->m_Device = nullptr;
	this_state->m_Image = get_state(swapchain)->m_BackBufferImages[backBufferIndex];
	this_state->m_Allocation = VK_NULL_HANDLE;
	this_state->m_Format = GetVkFormatFrom(m_Info.Format);
	this_state->m_ImageType = VK_IMAGE_TYPE_2D;
	
#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Image), VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT, "Back Buffer");
#endif // FLUENT_DEBUG
}

ImageResource::~ImageResource()
{
	if (this_state->m_Allocation != VK_NULL_HANDLE)
	{
		vmaDestroyImage(this_state->m_Allocator, this_state->m_Image, this_state->m_Allocation);
	}
}

FLUENT_END_NAMESPACE
