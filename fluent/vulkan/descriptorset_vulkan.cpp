#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

DescriptorSet::DescriptorSet(const Device* device, const DescriptorPool* desciptorPool,
	const DescriptorSetLayout* descriptorSetLayout, const char* name)
	: m_State(new_state(DescriptorSet))
{
	this_state->m_Device = get_state(device)->m_Device;

	VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
	descriptorSetAllocateInfo.pSetLayouts = &get_state(descriptorSetLayout)->m_DescriptorSetLayout;
	descriptorSetAllocateInfo.descriptorSetCount = 1;
	descriptorSetAllocateInfo.descriptorPool = get_state(desciptorPool)->m_DescriptorPool;
	FLUENT_API_CALL(vkAllocateDescriptorSets(get_state(device)->m_Device, &descriptorSetAllocateInfo, &this_state->m_DescriptorSet));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_DescriptorSet), VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT, name);
#endif // FLUENT_DEBUG
}

void DescriptorSet::Update(const std::vector<Descriptor> descriptors) const
{
	std::vector<VkWriteDescriptorSet> writeDescriptorSets(descriptors.size());
	for (uint32_t i = 0; i < descriptors.size(); ++i)
	{
		VkWriteDescriptorSet& writeDescriptorSet = writeDescriptorSets[i];
		writeDescriptorSet = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
		writeDescriptorSet.dstSet = this_state->m_DescriptorSet;
		writeDescriptorSet.dstBinding = descriptors[i].GetInfo().Binding;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorCount = 1;

		switch (descriptors[i].GetInfo().Type)
		{
		case DescriptorType::CombinedSampler:
			writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			writeDescriptorSet.pImageInfo = &get_state(&descriptors[i])->m_ResourceInfo.ImageInfo;
			break;
		case DescriptorType::UniformBuffer:
			writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			writeDescriptorSet.pBufferInfo = &get_state(&descriptors[i])->m_ResourceInfo.BufferInfo;
			break;
		default:
			FLUENT_ASSERT_ALWAYS("Unsupported DescriptorType.");
			break;
		}
	}

	uint32_t descriptorWriteCount = static_cast<uint32_t>(writeDescriptorSets.size());
	VkWriteDescriptorSet* descriptorWrites = writeDescriptorSets.data();
	vkUpdateDescriptorSets(this_state->m_Device, descriptorWriteCount, descriptorWrites, 0, nullptr);
}

FLUENT_END_NAMESPACE
