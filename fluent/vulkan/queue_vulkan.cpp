#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

Queue::Queue(const Device* device, const QueueInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(Queue))
{
	this_state->m_FamilyIndex = get_state(device)->m_QueueFamilyIndices[static_cast<uint32_t>(m_Info.Type)];

	vkGetDeviceQueue(get_state(device)->m_Device, this_state->m_FamilyIndex, m_Info.Index, &this_state->m_Queue);

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Queue), VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT, name);
#endif // FLUENT_DEBUG
}

void Queue::Submit(const std::vector<CommandBuffer*> commandBuffers, const std::vector<Semaphore*> semaphoresToWait,
	const std::vector<Semaphore*> semaphoresToSignal, const std::optional<Fence*> fenceToWait)
{
	std::vector<VkCommandBuffer> vkCommandBuffers;
	for (uint32_t i = 0; i < commandBuffers.size(); ++i)
	{
		vkCommandBuffers.push_back(get_state(commandBuffers[i])->m_CommandBuffer);
	}

	std::vector<VkSemaphore> vkSemaphoresToWait;
	for (uint32_t i = 0; i < semaphoresToWait.size(); ++i)
	{
		vkSemaphoresToWait.push_back(get_state(semaphoresToWait[i])->m_Semaphore);
	}

	std::vector<VkSemaphore> vkSemaphoresToSignal;
	for (uint32_t i = 0; i < semaphoresToSignal.size(); ++i)
	{
		vkSemaphoresToSignal.push_back(get_state(semaphoresToSignal[i])->m_Semaphore);
	}

	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT };

	VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
	submitInfo.commandBufferCount = static_cast<uint32_t>(vkCommandBuffers.size());
	submitInfo.pCommandBuffers = vkCommandBuffers.data();
	submitInfo.waitSemaphoreCount = static_cast<uint32_t>(vkSemaphoresToWait.size());
	submitInfo.pWaitSemaphores = vkSemaphoresToWait.data();
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.signalSemaphoreCount = static_cast<uint32_t>(vkSemaphoresToSignal.size());
	submitInfo.pSignalSemaphores = vkSemaphoresToSignal.data();

	VkFence vkFenceToWait = VK_NULL_HANDLE;
	if (fenceToWait.has_value())
	{
		vkFenceToWait = get_state(fenceToWait.value())->m_Fence;
	}

	FLUENT_API_CALL(vkQueueSubmit(this_state->m_Queue, 1, &submitInfo, vkFenceToWait));
}

void Queue::WaitIdle()
{
	vkQueueWaitIdle(this_state->m_Queue);
}

FLUENT_END_NAMESPACE
