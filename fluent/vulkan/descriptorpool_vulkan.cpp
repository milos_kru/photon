#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

DescriptorPool::DescriptorPool(const Device* device, const DescriptorPoolInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(DescriptorPool))
{
	this_state->m_Device = get_state(device)->m_Device;

	std::vector<VkDescriptorPoolSize> descriptroPoolSizes;
	for (const DescriptorPoolSize descriptorSize : m_Info.DescriptorSizes)
	{
		VkDescriptorPoolSize descriptorPoolSize;
		descriptorPoolSize.descriptorCount = descriptorSize.Count;
		descriptorPoolSize.type = GetVkDescriptorTypeFrom(descriptorSize.Type);
		descriptroPoolSizes.push_back(descriptorPoolSize);
	}

	VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
	descriptorPoolCreateInfo.poolSizeCount = static_cast<uint32_t>(descriptroPoolSizes.size());
	descriptorPoolCreateInfo.pPoolSizes = descriptroPoolSizes.data();
	descriptorPoolCreateInfo.maxSets = m_Info.MaxDescriptorSets;

	FLUENT_API_CALL(vkCreateDescriptorPool(this_state->m_Device, &descriptorPoolCreateInfo, nullptr, &this_state->m_DescriptorPool));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_DescriptorPool), VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT, name);
#endif // FLUENT_DEBUG
}

DescriptorPool::~DescriptorPool()
{
	vkDestroyDescriptorPool(this_state->m_Device, this_state->m_DescriptorPool, nullptr);
}

FLUENT_END_NAMESPACE
