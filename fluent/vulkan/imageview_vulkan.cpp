#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

ImageView::ImageView(const Device* device, const ImageViewInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(ImageView))
{
	ImageResource* image = m_Info.Resource;
	this_state->m_Device = get_state(device)->m_Device;
	this_state->m_Format = GetVkFormatFrom(m_Info.Format == Format::UNDEFINED ? image->GetInfo().Format : m_Info.Format);

	VkImageViewCreateInfo imageViewCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
	imageViewCreateInfo.format = this_state->m_Format;

	uint32_t arraySizeMult = 1;
	if (IsFlagSet(image->GetInfo().Flags & ImageFlags::CubeMap))
	{
		arraySizeMult = 6;
		imageViewCreateInfo.viewType = image->GetInfo().ArraySize > 1 ? VK_IMAGE_VIEW_TYPE_CUBE_ARRAY : VK_IMAGE_VIEW_TYPE_CUBE;
	}
	else if (get_state(image)->m_ImageType == VK_IMAGE_TYPE_2D)
	{
		imageViewCreateInfo.viewType = image->GetInfo().ArraySize > 1 ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D;
	}
	else if (get_state(image)->m_ImageType == VK_IMAGE_TYPE_3D)
	{
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
	}
	else
	{
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
	}

	imageViewCreateInfo.subresourceRange.aspectMask = GetVkImageAspectFlagsFrom(this_state->m_Format);
	imageViewCreateInfo.subresourceRange.baseMipLevel = m_Info.Subresource.MipSlice;
	imageViewCreateInfo.subresourceRange.levelCount = m_Info.Subresource.MipSize;
	imageViewCreateInfo.subresourceRange.baseArrayLayer = m_Info.Subresource.ArraySlice * arraySizeMult;
	imageViewCreateInfo.subresourceRange.layerCount = m_Info.Subresource.ArraySize * arraySizeMult;
	imageViewCreateInfo.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
	imageViewCreateInfo.image = get_state(image)->m_Image;

	FLUENT_API_CALL(vkCreateImageView(this_state->m_Device, &imageViewCreateInfo, nullptr, &this_state->m_ImageView));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_ImageView), VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT, name);
#endif // FLUENT_DEBUG
}

ImageView::~ImageView()
{
	vkDestroyImageView(this_state->m_Device, this_state->m_ImageView, nullptr);
}

FLUENT_END_NAMESPACE
