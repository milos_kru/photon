#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

TransferCommandBuffer::TransferCommandBuffer(const Device* device, const CommandPool* commandPool,
	const char* name)
	: CommandBuffer(device, commandPool, new_state(TransferCommandBuffer), name) {}

void TransferCommandBuffer::CopyResource(const BufferResource* srcBuffer, const BufferResource* dstBuffer)
{
	BufferRegion srcBufferRegion = { 0, srcBuffer->GetInfo().Size };
	BufferRegion dstBufferRegion = { 0, dstBuffer->GetInfo().Size };
	CopyResourceRegion(srcBuffer, srcBufferRegion, dstBuffer, dstBufferRegion);
}

void TransferCommandBuffer::CopyResource(const ImageResource* srcImage, const ImageResource* dstImage)
{
	FLUENT_ASSERT(GetVkSampleFlagsFrom(srcImage->GetInfo().MultisampleType) ==
		GetVkSampleFlagsFrom(dstImage->GetInfo().MultisampleType));

	CommitBarriers();

	uint32_t srcMipLevels = srcImage->GetInfo().MipCount;
	uint32_t dstMipLevels = dstImage->GetInfo().MipCount;
	uint32_t mipCount = srcMipLevels < dstMipLevels ? srcMipLevels : dstMipLevels;
	VkImageCopy* imageCopies = static_cast<VkImageCopy*>(alloca(mipCount * sizeof(VkImageCopy)));

	VkImageAspectFlags srcAspectFlags = GetVkImageAspectFlagsFrom(get_state(srcImage)->m_Format);
	VkImageAspectFlags dstAspectFlags = GetVkImageAspectFlagsFrom(get_state(dstImage)->m_Format);
	FLUENT_ASSERT(srcAspectFlags == dstAspectFlags);

	for (uint32_t j = 0; j < mipCount; ++j)
	{
		uint32_t srcHalfWidth = srcImage->GetInfo().Extent.Width >> j;
		uint32_t width = srcHalfWidth > static_cast<uint32_t>(1) ? srcHalfWidth : static_cast<uint32_t>(1);

		uint32_t srcHalfHeight = srcImage->GetInfo().Extent.Height >> j;
		uint32_t height = srcHalfHeight > static_cast<uint32_t>(1) ? srcHalfHeight : static_cast<uint32_t>(1);

		uint32_t srcHalfDepth = srcImage->GetInfo().Extent.Depth >> j;
		uint32_t depth = srcHalfDepth > static_cast<uint32_t>(1) ? srcHalfDepth : static_cast<uint32_t>(1);

		imageCopies[j].extent = { width, height, depth };
		imageCopies[j].srcOffset = { 0, 0, 0 };
		imageCopies[j].dstOffset = { 0, 0, 0 };

		imageCopies[j].srcSubresource.aspectMask = srcAspectFlags;
		imageCopies[j].srcSubresource.baseArrayLayer = 0;
		imageCopies[j].srcSubresource.layerCount = srcImage->GetInfo().ArraySize;
		imageCopies[j].srcSubresource.mipLevel = j;

		imageCopies[j].dstSubresource.aspectMask = dstAspectFlags;
		imageCopies[j].dstSubresource.baseArrayLayer = 0;
		imageCopies[j].dstSubresource.layerCount = dstImage->GetInfo().ArraySize;
		imageCopies[j].dstSubresource.mipLevel = j;
	}

	vkCmdCopyImage(this_state->m_CommandBuffer, get_state(srcImage)->m_Image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		get_state(dstImage)->m_Image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipCount, imageCopies);
}

void TransferCommandBuffer::CopyResourceRegion(const BufferResource* srcBuffer, const BufferRegion& srcRegion,
	const BufferResource* dstBuffer, const BufferRegion& dstRegion)
{
	FLUENT_ASSERT(dstRegion.Size >= srcRegion.Size);

	CommitBarriers();

	VkBufferCopy bufferCopy;
	bufferCopy.srcOffset = srcRegion.Offset;
	bufferCopy.dstOffset = dstRegion.Offset;
	bufferCopy.size = srcRegion.Size;

	vkCmdCopyBuffer(this_state->m_CommandBuffer, get_state(srcBuffer)->m_Buffer, get_state(dstBuffer)->m_Buffer, 1, &bufferCopy);
}

void TransferCommandBuffer::CopyResourceRegion(const BufferResource* srcBuffer, const BufferRegion& srcRegion,
	const ImageResource* dstImage, const ImageRegion& dstRegion)
{
	CommitBarriers();

	VkBufferImageCopy bufferImageCopy;
	bufferImageCopy.bufferOffset = srcRegion.Offset;
	bufferImageCopy.bufferRowLength = 0;
	bufferImageCopy.bufferImageHeight = 0;
	bufferImageCopy.imageOffset = { dstRegion.Offset.X, dstRegion.Offset.Y, dstRegion.Offset.Z };
	bufferImageCopy.imageExtent = { dstRegion.Extent.Width, dstRegion.Extent.Height, dstRegion.Extent.Depth };
	bufferImageCopy.imageSubresource =
	{
		GetVkImageAspectFlagsFrom(get_state(dstImage)->m_Format),
		dstRegion.Subresource.MipSlice, dstRegion.Subresource.ArraySlice, dstRegion.Subresource.ArraySize
	};

	vkCmdCopyBufferToImage(this_state->m_CommandBuffer, get_state(srcBuffer)->m_Buffer, get_state(dstImage)->m_Image,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferImageCopy);
}

void TransferCommandBuffer::CopyResourceRegion(const ImageResource* srcImage, const ImageRegion& srcRegion,
	const BufferResource* dstBuffer, const BufferRegion& dstRegion)
{
	CommitBarriers();

	VkBufferImageCopy bufferImageCopy;
	bufferImageCopy.bufferOffset = dstRegion.Offset;
	bufferImageCopy.bufferRowLength = 0;
	bufferImageCopy.bufferImageHeight = 0;
	bufferImageCopy.imageOffset = { srcRegion.Offset.X, srcRegion.Offset.Y, srcRegion.Offset.Z };
	bufferImageCopy.imageExtent = { srcRegion.Extent.Width, srcRegion.Extent.Height, srcRegion.Extent.Depth };
	bufferImageCopy.imageSubresource =
	{
		GetVkImageAspectFlagsFrom(get_state(srcImage)->m_Format),
		srcRegion.Subresource.MipSlice, srcRegion.Subresource.ArraySlice, srcRegion.Subresource.ArraySize
	};

	vkCmdCopyImageToBuffer(this_state->m_CommandBuffer, get_state(srcImage)->m_Image,
		VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, get_state(dstBuffer)->m_Buffer, 1, &bufferImageCopy);
}

void TransferCommandBuffer::CopyResourceRegion(const ImageResource* srcImage, const ImageRegion& srcRegion,
	const ImageResource* dstImage, const ImageRegion& dstRegion)
{
	FLUENT_ASSERT(dstRegion.Extent.Width == srcRegion.Extent.Width &&
		dstRegion.Extent.Height == srcRegion.Extent.Height &&
		dstRegion.Extent.Depth == srcRegion.Extent.Depth);

	FLUENT_ASSERT(GetVkSampleFlagsFrom(srcImage->GetInfo().MultisampleType) ==
		GetVkSampleFlagsFrom(dstImage->GetInfo().MultisampleType));

	CommitBarriers();

	VkImageCopy imageCopy;
	imageCopy.dstOffset = { dstRegion.Offset.X, dstRegion.Offset.Y, dstRegion.Offset.Z };
	imageCopy.srcOffset = { srcRegion.Offset.X, srcRegion.Offset.Y, srcRegion.Offset.Z };
	imageCopy.extent = { srcRegion.Extent.Width, srcRegion.Extent.Height, srcRegion.Extent.Depth };

	imageCopy.srcSubresource =
	{
		GetVkImageAspectFlagsFrom(get_state(srcImage)->m_Format),
		srcRegion.Subresource.MipSlice, srcRegion.Subresource.ArraySlice, srcRegion.Subresource.ArraySize
	};
	
	imageCopy.dstSubresource =
	{
		GetVkImageAspectFlagsFrom(get_state(dstImage)->m_Format),
		dstRegion.Subresource.MipSlice, dstRegion.Subresource.ArraySlice, dstRegion.Subresource.ArraySize
	};

	vkCmdCopyImage(this_state->m_CommandBuffer, get_state(srcImage)->m_Image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		get_state(dstImage)->m_Image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imageCopy);
}

FLUENT_END_NAMESPACE
