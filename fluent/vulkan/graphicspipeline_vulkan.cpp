#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

GraphicsPipeline::GraphicsPipeline(const Device* device, const RenderPass* renderPass,
	const DescriptorSetLayout* descriptorSetLayout, const GraphicsPipelineDescription* descriptor, const char* name)
	: m_State(new_state(GraphicsPipeline))
{
	this_state->m_Device = get_state(device)->m_Device;
	this_state->m_PipelineLayout = get_state(descriptorSetLayout)->m_PipelineLayout;

	VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
	graphicsPipelineCreateInfo.stageCount = get_state(descriptor)->m_ShaderStageCount;
	graphicsPipelineCreateInfo.pStages = get_state(descriptor)->m_ShaderStageCreateInfo;
	graphicsPipelineCreateInfo.pVertexInputState = &get_state(descriptor)->m_VertexInputStateCreateInfo;
	graphicsPipelineCreateInfo.pInputAssemblyState = &get_state(descriptor)->m_InputAssemblyStateCreateInfo;
	graphicsPipelineCreateInfo.pViewportState = &get_state(descriptor)->m_ViewportStateCreateInfo;
	graphicsPipelineCreateInfo.pRasterizationState = &get_state(descriptor)->m_RasterizationStateCreateInfo;
	graphicsPipelineCreateInfo.pMultisampleState = &get_state(descriptor)->m_MultisampleStateCreateInfo;
	graphicsPipelineCreateInfo.pDepthStencilState = &get_state(descriptor)->m_DepthStencilStateCreateInfo;
	graphicsPipelineCreateInfo.pColorBlendState = &get_state(descriptor)->m_ColorBlendStateCreateInfo;
	graphicsPipelineCreateInfo.pDynamicState = &get_state(descriptor)->m_DynamicStateCreateInfo;
	graphicsPipelineCreateInfo.pTessellationState = &get_state(descriptor)->m_TessellatinStateCreateInfo;
	graphicsPipelineCreateInfo.layout = this_state->m_PipelineLayout;
	graphicsPipelineCreateInfo.renderPass = get_state(renderPass)->m_RenderPass;
	graphicsPipelineCreateInfo.subpass = 0;
	graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	graphicsPipelineCreateInfo.basePipelineIndex = -1;

	FLUENT_API_CALL(vkCreateGraphicsPipelines(this_state->m_Device, VK_NULL_HANDLE, 1,
		&graphicsPipelineCreateInfo, nullptr, &this_state->m_Pipeline));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Pipeline), VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT, name);
#endif // FLUENT_DEBUG
}

GraphicsPipeline::~GraphicsPipeline()
{
	vkDestroyPipeline(this_state->m_Device, this_state->m_Pipeline, nullptr);
}

FLUENT_END_NAMESPACE
