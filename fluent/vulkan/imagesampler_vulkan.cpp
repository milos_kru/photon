#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

ImageSampler::ImageSampler(const Device* device, const ImageSamplerInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(ImageSampler))
{
	this_state->m_Device = get_state(device)->m_Device;

	VkSamplerCreateInfo samplerCreateInfo = { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO };
	samplerCreateInfo.magFilter = GetVkFilterFrom(m_Info.MagFilterType);
	samplerCreateInfo.minFilter = GetVkFilterFrom(m_Info.MinFilterType);
	samplerCreateInfo.mipmapMode = GetVkMipmapModeFrom(m_Info.MipFilterType);
	samplerCreateInfo.addressModeU = GetVkAddressModeFrom(m_Info.AddressModeU);
	samplerCreateInfo.addressModeV = GetVkAddressModeFrom(m_Info.AddressModeV);
	samplerCreateInfo.addressModeW = GetVkAddressModeFrom(m_Info.AddressModeW);
	samplerCreateInfo.mipLodBias = m_Info.MipLODBias;
	samplerCreateInfo.compareEnable = m_Info.CompareOperation != CompareOperation::Never;
	samplerCreateInfo.compareOp = GetVkCompareOperationFrom(m_Info.CompareOperation);
	samplerCreateInfo.minLod = m_Info.MinLOD;
	samplerCreateInfo.maxLod = m_Info.MaxLOD;
	samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
	samplerCreateInfo.borderColor = GetVkBorderColorFrom(m_Info.BorderColor);

	if (m_Info.MipFilterType == FilterType::Anisotropic && m_Info.MaxLevelOfAnisotropy != 0)
	{
		samplerCreateInfo.anisotropyEnable = VK_TRUE;
		samplerCreateInfo.maxAnisotropy = m_Info.MaxLevelOfAnisotropy;
	}
	else
	{
		samplerCreateInfo.anisotropyEnable = VK_FALSE;
		samplerCreateInfo.maxAnisotropy = 0;
	}

	FLUENT_API_CALL(vkCreateSampler(this_state->m_Device, &samplerCreateInfo, nullptr, &this_state->m_Sampler));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Sampler), VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT, name);
#endif // FLUENT_DEBUG
}

ImageSampler::~ImageSampler()
{
	vkDestroySampler(this_state->m_Device, this_state->m_Sampler, nullptr);
}

FLUENT_END_NAMESPACE
