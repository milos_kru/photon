#include "precompiled.h"

FLUENT_BEGIN_NAMESPACE

Framebuffer::Framebuffer(const Device* device, const std::vector<ImageView*> renderTargetViews,
	const std::optional<ImageView*> depthStencilView, const RenderPass* renderPass, const FramebufferInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(Framebuffer))
	, m_RenderTargets(renderTargetViews)
	, m_DepthStencil(depthStencilView)
{
	this_state->m_Device = get_state(device)->m_Device;

	std::vector<VkImageView> imageAttachments;
	for (const ImageView* renderTargetView : m_RenderTargets)
	{
		FLUENT_ASSERT(m_Info.Width == renderTargetView->GetInfo().Resource->GetInfo().Extent.Width);
		FLUENT_ASSERT(m_Info.Height == renderTargetView->GetInfo().Resource->GetInfo().Extent.Height);

		imageAttachments.push_back(get_state(renderTargetView)->m_ImageView);
	}

	if (m_DepthStencil.has_value())
	{
		ImageView* depthStencilViewValue = m_DepthStencil.value();

		FLUENT_ASSERT(m_Info.Width == depthStencilViewValue->GetInfo().Resource->GetInfo().Extent.Width);
		FLUENT_ASSERT(m_Info.Height == depthStencilViewValue->GetInfo().Resource->GetInfo().Extent.Height);

		imageAttachments.push_back(get_state(depthStencilViewValue)->m_ImageView);
	}

	VkFramebufferCreateInfo framebufferCreateInfo = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
	framebufferCreateInfo.pNext = nullptr;
	framebufferCreateInfo.flags = 0;
	framebufferCreateInfo.renderPass = get_state(renderPass)->m_RenderPass;
	framebufferCreateInfo.pAttachments = imageAttachments.data();
	framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(imageAttachments.size());
	framebufferCreateInfo.width = m_Info.Width;
	framebufferCreateInfo.height = m_Info.Height;
	framebufferCreateInfo.layers = 1;

	FLUENT_API_CALL(vkCreateFramebuffer(this_state->m_Device, &framebufferCreateInfo, nullptr, &this_state->m_Framebuffer));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Framebuffer), VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT, name);
#endif // FLUENT_DEBUG
}

Framebuffer::~Framebuffer()
{
	vkDestroyFramebuffer(this_state->m_Device, this_state->m_Framebuffer, nullptr);
}

FLUENT_END_NAMESPACE
