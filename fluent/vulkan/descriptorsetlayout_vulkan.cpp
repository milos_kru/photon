#include "precompiled.h"

#include <spirv_reflect.h>

FLUENT_BEGIN_NAMESPACE

DescriptorSetLayout::DescriptorSetLayout(const Device* device, const std::vector<Shader*> shaders, const char* name)
	: m_State(new_state(DescriptorSetLayout))
{
	this_state->m_Device = get_state(device)->m_Device;

	std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings;
	for (const Shader* shader : shaders) // TODO: Lambda?
	{
		for (VkDescriptorSetLayoutBinding& binding : get_state(shader)->m_DescriptorSetLayoutBindings)
		{
			bool alreadyRegistered = false;
			for (VkDescriptorSetLayoutBinding& registeredBinding : descriptorSetLayoutBindings)
			{
				if (registeredBinding.binding == binding.binding)
				{
					registeredBinding.stageFlags |= binding.stageFlags;
					alreadyRegistered = true;
					break;
				}
			}

			if (!alreadyRegistered)
			{
				descriptorSetLayoutBindings.push_back(binding);
			}
		}
	}

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
	descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(descriptorSetLayoutBindings.size());
	descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

	FLUENT_API_CALL(vkCreateDescriptorSetLayout(this_state->m_Device, &descriptorSetLayoutCreateInfo, nullptr, &this_state->m_DescriptorSetLayout));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_DescriptorSetLayout), VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT, name);
#endif // FLUENT_DEBUG

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &this_state->m_DescriptorSetLayout;

	FLUENT_API_CALL(vkCreatePipelineLayout(this_state->m_Device, &pipelineLayoutCreateInfo, nullptr, &this_state->m_PipelineLayout));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_PipelineLayout), VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT, name);
#endif // FLUENT_DEBUG
}

DescriptorSetLayout::~DescriptorSetLayout()
{
	vkDestroyDescriptorSetLayout(this_state->m_Device, this_state->m_DescriptorSetLayout, nullptr);
	vkDestroyPipelineLayout(this_state->m_Device, this_state->m_PipelineLayout, nullptr);
}

FLUENT_END_NAMESPACE
