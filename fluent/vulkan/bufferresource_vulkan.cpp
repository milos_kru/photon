#include "precompiled.h"

#include "vk_mem_alloc.h"

FLUENT_BEGIN_NAMESPACE

BufferResource::BufferResource(const Device* device, const BufferResourceInfo& info, const char* name)
	: m_Info(info)
	, m_State(new_state(BufferResource))
{
	// TODO: Since we can't use private helper methods, use {} to separate different logic blocks.

	this_state->m_Device = get_state(device)->m_Device;
	this_state->m_Allocator = get_state(device)->m_Allocator;
	this_state->m_HostVisibleData = nullptr;

	VkBufferCreateInfo bufferCreateInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
	bufferCreateInfo.size = m_Info.Size;
	bufferCreateInfo.usage = GetVkBufferUsageFlags(m_Info.UsageFlags);
	bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocationCreateInfo = {};
	allocationCreateInfo.usage = GetVmaMemoryUsageFrom(m_Info.MemoryAccess);

	FLUENT_API_CALL(vmaCreateBuffer(this_state->m_Allocator, &bufferCreateInfo,
		&allocationCreateInfo, &this_state->m_Buffer, &this_state->m_Allocation, nullptr));

#ifdef FLUENT_DEBUG
	SetDebugName(device, uint64_t(this_state->m_Buffer), VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT, name);
#endif // FLUENT_DEBUG
}

BufferResource::~BufferResource()
{
	vmaDestroyBuffer(this_state->m_Allocator, this_state->m_Buffer, this_state->m_Allocation);
}

void* BufferResource::Map() const
{
	FLUENT_ASSERT(this_state->m_HostVisibleData == nullptr);
	
	if (m_Info.MemoryAccess == MemoryAccess::Host)
	{
		FLUENT_API_CALL(vmaMapMemory(this_state->m_Allocator, this_state->m_Allocation, &this_state->m_HostVisibleData));
	}

	return this_state->m_HostVisibleData;
 }

void BufferResource::Unmap() const
{
	if (this_state->m_HostVisibleData)
	{
		vmaUnmapMemory(this_state->m_Allocator, this_state->m_Allocation);
		this_state->m_HostVisibleData = nullptr;
	}
}

FLUENT_END_NAMESPACE
