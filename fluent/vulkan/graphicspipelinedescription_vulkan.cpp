#include "precompiled.h"

#include <Crc32.h>

FLUENT_BEGIN_NAMESPACE

GraphicsPipelineDescription::GraphicsPipelineDescription()
	: m_State(new_state(GraphicsPipelineDescription))
{
	for (uint32_t i = 0; i < static_cast<uint32_t>(ShaderType::Count); ++i)
	{
		this_state->m_ShaderStageCreateInfo[i] = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO };
	}

	this_state->m_VertexInputStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO };

	this_state->m_InputAssemblyStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO };
	this_state->m_InputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	this_state->m_InputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;

	this_state->m_ViewportStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO };
	this_state->m_ViewportStateCreateInfo.viewportCount = 1;
	this_state->m_ViewportStateCreateInfo.scissorCount = 1;

	this_state->m_MultisampleStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO };
	this_state->m_MultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	this_state->m_MultisampleStateCreateInfo.minSampleShading = 1.0f;
	this_state->m_MultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
	
	static const VkDynamicState dynamicStates[] = { VK_DYNAMIC_STATE_VIEWPORT , VK_DYNAMIC_STATE_SCISSOR };
	this_state->m_DynamicStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO };
	this_state->m_DynamicStateCreateInfo.pDynamicStates = dynamicStates;
	this_state->m_DynamicStateCreateInfo.dynamicStateCount = 2;

	VkStencilOpState defaultStencilOpState{};
	defaultStencilOpState.failOp = VK_STENCIL_OP_KEEP;
	defaultStencilOpState.passOp = VK_STENCIL_OP_KEEP;
	defaultStencilOpState.depthFailOp = VK_STENCIL_OP_KEEP;
	defaultStencilOpState.compareOp = VK_COMPARE_OP_ALWAYS;
	defaultStencilOpState.compareMask = 0xFFFFFFFF;
	defaultStencilOpState.writeMask = 0xFFFFFFFF;
	defaultStencilOpState.reference = 0;

	this_state->m_DepthStencilStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO };
	this_state->m_DepthStencilStateCreateInfo.depthTestEnable = true;
	this_state->m_DepthStencilStateCreateInfo.depthWriteEnable = true;
	this_state->m_DepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS;
	this_state->m_DepthStencilStateCreateInfo.depthBoundsTestEnable = false;
	this_state->m_DepthStencilStateCreateInfo.stencilTestEnable = true;
	this_state->m_DepthStencilStateCreateInfo.front = defaultStencilOpState;
	this_state->m_DepthStencilStateCreateInfo.back = defaultStencilOpState;
	this_state->m_DepthStencilStateCreateInfo.minDepthBounds = 0.0f;
	this_state->m_DepthStencilStateCreateInfo.maxDepthBounds = 1.0f;

	this_state->m_RasterizationStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO };
	this_state->m_RasterizationStateCreateInfo.depthClampEnable = false;
	this_state->m_RasterizationStateCreateInfo.rasterizerDiscardEnable = false;
	this_state->m_RasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	this_state->m_RasterizationStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	this_state->m_RasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
	this_state->m_RasterizationStateCreateInfo.depthBiasEnable = false;
	this_state->m_RasterizationStateCreateInfo.depthBiasConstantFactor = 0.0f;
	this_state->m_RasterizationStateCreateInfo.depthBiasClamp = 0.0f;
	this_state->m_RasterizationStateCreateInfo.depthBiasSlopeFactor = 0.0f;
	this_state->m_RasterizationStateCreateInfo.lineWidth = 1.0f;

	this_state->m_ColorBlendStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO };
	this_state->m_ColorBlendStateCreateInfo.logicOpEnable = false;
	this_state->m_ColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_NO_OP;
	this_state->m_ColorBlendStateCreateInfo.attachmentCount = MaxRenderTargetCount;
	this_state->m_ColorBlendStateCreateInfo.pAttachments = this_state->m_ColorBlendAttachmentState;
	this_state->m_ColorBlendStateCreateInfo.blendConstants[0] = 1.0f;
	this_state->m_ColorBlendStateCreateInfo.blendConstants[1] = 1.0f;
	this_state->m_ColorBlendStateCreateInfo.blendConstants[2] = 1.0f;
	this_state->m_ColorBlendStateCreateInfo.blendConstants[3] = 1.0f;

	for (uint32_t i = 0; i < MaxRenderTargetCount; ++i)
	{
		this_state->m_ColorBlendAttachmentState[i].blendEnable = false;
		this_state->m_ColorBlendAttachmentState[i].srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
		this_state->m_ColorBlendAttachmentState[i].dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
		this_state->m_ColorBlendAttachmentState[i].colorBlendOp = VK_BLEND_OP_ADD;
		this_state->m_ColorBlendAttachmentState[i].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		this_state->m_ColorBlendAttachmentState[i].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		this_state->m_ColorBlendAttachmentState[i].alphaBlendOp = VK_BLEND_OP_ADD;
		this_state->m_ColorBlendAttachmentState[i].colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
			VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	}

	this_state->m_TessellatinStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO };
}

void GraphicsPipelineDescription::SetShaders(const std::vector<Shader*> shaders)
{
	for (uint32_t i = 0; i < static_cast<uint32_t>(ShaderType::Count); ++i)
	{
		this_state->m_ShaderStageCreateInfo[i] = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO };
	}

	this_state->m_ShaderStageCount = static_cast<uint32_t>(shaders.size());

	for (Shader* shader : shaders)
	{
		const uint32_t stage = static_cast<uint32_t>(shader->GetInfo().Type);
		this_state->m_ShaderStageCreateInfo[stage].pName = shader->GetInfo().CodeEntry;
		this_state->m_ShaderStageCreateInfo[stage].stage = GetVkShaderStageFrom(shader->GetInfo().Type);
		this_state->m_ShaderStageCreateInfo[stage].module = get_state(shader)->m_ShaderModule;
	}
}

void GraphicsPipelineDescription::SetVertexBinding(const VertexBinding* vertexBinding)
{
	this_state->m_VertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	this_state->m_VertexInputStateCreateInfo.pVertexBindingDescriptions = &get_state(vertexBinding)->m_VertexInputBindingDescription;
	this_state->m_VertexInputStateCreateInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(get_state(vertexBinding)->m_VertexAttributeDescriptions.size());
	this_state->m_VertexInputStateCreateInfo.pVertexAttributeDescriptions = get_state(vertexBinding)->m_VertexAttributeDescriptions.data();
}

void GraphicsPipelineDescription::SetTopology(const Topology topology)
{
	this_state->m_InputAssemblyStateCreateInfo.topology = GetVkPrimitiveTopologyFrom(topology);
}

void GraphicsPipelineDescription::SetMultisampleType(const MultisampleType multiSampleType)
{
	this_state->m_MultisampleStateCreateInfo.rasterizationSamples = GetVkSampleFlagsFrom(multiSampleType);
	this_state->m_MultisampleStateCreateInfo.sampleShadingEnable = multiSampleType == MultisampleType::Sample_1 ? VK_FALSE : VK_TRUE;
}

void GraphicsPipelineDescription::SetDepthWriteEnabled(const bool enabled)
{
	this_state->m_DepthStencilStateCreateInfo.depthWriteEnable = enabled;
}

void GraphicsPipelineDescription::SetDepthTestEnabled(const bool enabled)
{
	this_state->m_DepthStencilStateCreateInfo.depthTestEnable = enabled;
}

void GraphicsPipelineDescription::SetDepthCompare(const CompareOperation compareFunction)
{
	this_state->m_DepthStencilStateCreateInfo.depthCompareOp = GetVkCompareOperationFrom(compareFunction);
}

void GraphicsPipelineDescription::SetDepthBias(const float depthBiasSlope, const float depthBias, const float depthBiasClamp)
{
	this_state->m_RasterizationStateCreateInfo.depthBiasEnable = (depthBiasSlope != 0.0f) || (depthBias != 0.0f);
	this_state->m_RasterizationStateCreateInfo.depthBiasSlopeFactor = depthBiasSlope;
	this_state->m_RasterizationStateCreateInfo.depthBiasConstantFactor = depthBias;
	this_state->m_RasterizationStateCreateInfo.depthBiasClamp = depthBiasClamp;
}

void GraphicsPipelineDescription::SetStencilEnable(const bool enabled)
{
	this_state->m_DepthStencilStateCreateInfo.stencilTestEnable = enabled;
}

void GraphicsPipelineDescription::SetStencilOperation(const StencilOperation stencilFail, const StencilOperation depthFail, const StencilOperation stencilPass)
{
	VkStencilOp stencilFailOp = GetVkStencilOpFrom(stencilFail);
	VkStencilOp depthFailOp = GetVkStencilOpFrom(depthFail);
	VkStencilOp stencilPassOp = GetVkStencilOpFrom(stencilPass);

	this_state->m_DepthStencilStateCreateInfo.front.failOp = stencilFailOp;
	this_state->m_DepthStencilStateCreateInfo.front.depthFailOp = depthFailOp;
	this_state->m_DepthStencilStateCreateInfo.front.passOp = stencilPassOp;

	this_state->m_DepthStencilStateCreateInfo.back.failOp = stencilFailOp;
	this_state->m_DepthStencilStateCreateInfo.back.depthFailOp = depthFailOp;
	this_state->m_DepthStencilStateCreateInfo.back.passOp = stencilPassOp;
}

void GraphicsPipelineDescription::SetStencilFunction(const CompareOperation compareOperation, const uint8_t compareMask)
{
	VkCompareOp stencilCompareOp = GetVkCompareOperationFrom(compareOperation);

	this_state->m_DepthStencilStateCreateInfo.front.compareOp = stencilCompareOp;
	this_state->m_DepthStencilStateCreateInfo.back.compareOp = stencilCompareOp;

	this_state->m_DepthStencilStateCreateInfo.front.compareMask = compareMask;
	this_state->m_DepthStencilStateCreateInfo.back.compareMask = compareMask;
}

void GraphicsPipelineDescription::SetStencilWriteMask(const uint8_t mask)
{
	this_state->m_DepthStencilStateCreateInfo.front.writeMask = mask;
	this_state->m_DepthStencilStateCreateInfo.back.writeMask = mask;
}

void GraphicsPipelineDescription::SetWireFrameEnabled(const bool enable)
{
	this_state->m_RasterizationStateCreateInfo.polygonMode = enable ? VK_POLYGON_MODE_LINE : VK_POLYGON_MODE_FILL;
}

void GraphicsPipelineDescription::SetCullMode(const CullMode mode)
{
	this_state->m_RasterizationStateCreateInfo.cullMode = mode == CullMode::None ? VK_CULL_MODE_NONE :
		mode == CullMode::Front ? VK_CULL_MODE_FRONT_BIT : VK_CULL_MODE_BACK_BIT;
}

void GraphicsPipelineDescription::SetWindingOrder(const WindingOrder winding)
{
	this_state->m_RasterizationStateCreateInfo.frontFace = winding == WindingOrder::Clockwise ? VK_FRONT_FACE_CLOCKWISE : VK_FRONT_FACE_COUNTER_CLOCKWISE;
}

void GraphicsPipelineDescription::SetColorWriteMask(const uint32_t mrtIndex, const ColorWriteMaskFlags mask)
{
	FLUENT_ASSERT(mrtIndex < MaxRenderTargetCount);

	this_state->m_ColorBlendAttachmentState[mrtIndex].colorWriteMask = VkColorComponentFlags(mask);
}

void GraphicsPipelineDescription::SetColorWriteMask(const uint32_t mrtIndex, const uint32_t mrtCount, const ColorWriteMaskFlags masks[])
{
	FLUENT_ASSERT(mrtIndex + mrtCount <= MaxRenderTargetCount);

	for (uint32_t i = 0; i < mrtCount; ++i)
	{
		this_state->m_ColorBlendAttachmentState[mrtIndex].colorWriteMask = VkColorComponentFlags(masks[mrtIndex]);
	}
}

void GraphicsPipelineDescription::SetAttachmentCount(const uint32_t attachmentCount)
{
	FLUENT_ASSERT(attachmentCount <= MaxRenderTargetCount);

	this_state->m_ColorBlendStateCreateInfo.attachmentCount = attachmentCount;
}

void GraphicsPipelineDescription::SetAlphaBlendEnabled(const uint32_t mrtIndex, const bool enabled)
{
	FLUENT_ASSERT(mrtIndex < MaxRenderTargetCount);

	this_state->m_ColorBlendAttachmentState[mrtIndex].blendEnable = enabled;
}

void GraphicsPipelineDescription::SetAlphaBlendFunction(const uint32_t mrtIndex, const BlendValue srcBlend, const BlendValue dstBlend)
{
	FLUENT_ASSERT(mrtIndex < MaxRenderTargetCount);

	this_state->m_ColorBlendAttachmentState[mrtIndex].srcColorBlendFactor = GetVkBlendFactorFrom(srcBlend);
	this_state->m_ColorBlendAttachmentState[mrtIndex].dstColorBlendFactor = GetVkBlendFactorFrom(dstBlend);
}

void GraphicsPipelineDescription::SetAlphaBlendFunction(const uint32_t mrtIndex, const BlendValue srcColorBlend,
	const BlendValue dstColorBlend, const BlendValue srcAlphaBlend, const BlendValue dstAlphablend)
{
	FLUENT_ASSERT(mrtIndex < MaxRenderTargetCount);

	this_state->m_ColorBlendAttachmentState[mrtIndex].srcColorBlendFactor = GetVkBlendFactorFrom(srcColorBlend);
	this_state->m_ColorBlendAttachmentState[mrtIndex].dstColorBlendFactor = GetVkBlendFactorFrom(dstColorBlend);
	this_state->m_ColorBlendAttachmentState[mrtIndex].srcAlphaBlendFactor = GetVkBlendFactorFrom(srcAlphaBlend);
	this_state->m_ColorBlendAttachmentState[mrtIndex].dstAlphaBlendFactor = GetVkBlendFactorFrom(dstAlphablend);
}

void GraphicsPipelineDescription::SetAlphaBlendOperation(const uint32_t mrtIndex, const BlendOperation colorOperation)
{
	FLUENT_ASSERT(mrtIndex < MaxRenderTargetCount);

	this_state->m_ColorBlendAttachmentState[mrtIndex].colorBlendOp = GetVkBlendOpFrom(colorOperation);
}

void GraphicsPipelineDescription::SetAlphaBlendOperation(const uint32_t mrtIndex, const BlendOperation colorOperation, const BlendOperation alphaOperation)
{
	FLUENT_ASSERT(mrtIndex < MaxRenderTargetCount);

	this_state->m_ColorBlendAttachmentState[mrtIndex].colorBlendOp = GetVkBlendOpFrom(colorOperation);
	this_state->m_ColorBlendAttachmentState[mrtIndex].alphaBlendOp = GetVkBlendOpFrom(alphaOperation);
}

uint32_t GraphicsPipelineDescription::GetHash() const
{
	uint32_t hash = 0;

	for (uint32_t i = 0; i < this_state->m_ShaderStageCount; ++i)
	{
		hash = crc32_16bytes(&this_state->m_ShaderStageCreateInfo[i], sizeof(this_state->m_ShaderStageCreateInfo[i]), hash);
		hash = crc32_16bytes(&this_state->m_ShaderStageCreateInfo[i].stage, sizeof(this_state->m_ShaderStageCreateInfo[i].stage), hash);
		hash = crc32_16bytes(&this_state->m_ShaderStageCreateInfo[i].module, sizeof(this_state->m_ShaderStageCreateInfo[i].module), hash);
	}

	hash = crc32_16bytes(&this_state->m_DepthStencilStateCreateInfo, sizeof(this_state->m_DepthStencilStateCreateInfo), hash);
	hash = crc32_16bytes(&this_state->m_RasterizationStateCreateInfo, sizeof(this_state->m_RasterizationStateCreateInfo), hash);
	hash = crc32_16bytes(&this_state->m_ColorBlendStateCreateInfo.logicOpEnable, sizeof(this_state->m_ColorBlendStateCreateInfo.logicOpEnable), hash);
	hash = crc32_16bytes(&this_state->m_ColorBlendStateCreateInfo.logicOp, sizeof(this_state->m_ColorBlendStateCreateInfo.logicOp), hash);
	hash = crc32_16bytes(&this_state->m_ColorBlendStateCreateInfo.attachmentCount, sizeof(this_state->m_ColorBlendStateCreateInfo.attachmentCount), hash);
	hash = crc32_16bytes(&this_state->m_ColorBlendStateCreateInfo.blendConstants, sizeof(this_state->m_ColorBlendStateCreateInfo.blendConstants), hash);

	for (uint32_t i = 0; i < this_state->m_ColorBlendStateCreateInfo.attachmentCount; ++i)
	{
		hash = crc32_16bytes(&this_state->m_ColorBlendAttachmentState[i], sizeof(this_state->m_ColorBlendAttachmentState[i]), hash);
	}

	return hash;
}

FLUENT_END_NAMESPACE
